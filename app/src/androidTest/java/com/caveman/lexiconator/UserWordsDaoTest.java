package com.caveman.lexiconator;

import android.arch.persistence.db.SimpleSQLiteQuery;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.caveman.lexiconator.model.SortColumn;
import com.caveman.lexiconator.model.SortDirection;
import com.caveman.lexiconator.model.SortingOrder;
import com.caveman.lexiconator.model.room.UserWordMetadata;
import com.caveman.lexiconator.model.room.UserWordsDao;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.model.room.UserWordsSqliteSourcesDatabase;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static com.caveman.lexiconator.model.WordsDbContract.WordsMetadataTable.COLUMN_NAME_USER_WORDS_SOURCE_ID;
import static com.caveman.lexiconator.model.WordsDbContract.WordsMetadataTable.TABLE_NAME;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class UserWordsDaoTest {
    private UserWordsSqliteSourcesDatabase db;
    private UserWordsDao userWordsDao;
    private UserWordsSourceMetadata userWordsSourceMetadata1;
    private UserWordsSourceMetadata userWordsSourceMetadata2;
    private UserWordMetadata userWordMetadata1;
    private UserWordMetadata userWordMetadata2;


    @Before
    public void prepare() {
        Context context = InstrumentationRegistry.getTargetContext();;
        db = Room.inMemoryDatabaseBuilder(context, UserWordsSqliteSourcesDatabase.class).build();

        userWordsSourceMetadata1 = new UserWordsSourceMetadata("english", "eng");
        userWordsSourceMetadata2 = new UserWordsSourceMetadata("russian", "ru");
        long userWordsSourceMetadata1Id = db.userWordsSourcesDao().addNewUserWordsSourceMetadata(userWordsSourceMetadata1);
        long userWordsSourceMetadata2Id = db.userWordsSourcesDao().addNewUserWordsSourceMetadata(userWordsSourceMetadata2);
        userWordsSourceMetadata1.setId(userWordsSourceMetadata1Id);
        userWordsSourceMetadata2.setId(userWordsSourceMetadata2Id);

        userWordsDao = db.userWordsDao();

        String word = "cat";
        long addTimestamp = System.currentTimeMillis() / 1000L;
        int priority = 1;
        long userWordsSourceId = userWordsSourceMetadata2.getId();
        userWordMetadata1 = new UserWordMetadata(word, addTimestamp, priority, userWordsSourceId);

        word = "fish";
        addTimestamp = System.currentTimeMillis() / 1000L;
        priority = 4;
        userWordsSourceId = userWordsSourceMetadata2.getId();
        userWordMetadata2 = new UserWordMetadata(word, addTimestamp, priority, userWordsSourceId);
    }

    @After
    public void finish() {
        db.close();
    }

    @Test
    public void addWord_And_CheckIfExistsByWordAndUserWordsSourceId_Test() {
        userWordsDao.addWord(userWordMetadata1);
        boolean exist = userWordsDao.checkIfWordExist(userWordMetadata1.getWord(), userWordMetadata1.getUserWordsSourceId());
        assertTrue(exist);
    }

    @Test
    public void addWord_And_CheckIfExistsByWordId_Test() {
        long wordId = userWordsDao.addWord(userWordMetadata1);
        boolean exist = userWordsDao.checkIfWordExist(wordId);
        assertTrue(exist);
    }

    @Test
    public void addAndUpdateTest() {

        long wordId = userWordsDao.addWord(userWordMetadata1);
        userWordMetadata1.setId(wordId);
        userWordMetadata1.setWord("dog");
        userWordMetadata1.setAddTimestamp(100500);
        userWordMetadata1.setPriority(10);
        userWordMetadata1.setUserWordsSourceId(userWordsSourceMetadata2.getId());

        int rows = userWordsDao.updateWord(userWordMetadata1);
        UserWordMetadata resultUserWordMetadata = userWordsDao.getWordMetadataForWord(
                userWordMetadata1.getUserWordsSourceId(),
                userWordMetadata1.getWord()
        );
        assertThat(rows, Matchers.greaterThan(0));
        assertNotNull(resultUserWordMetadata);
        assertThatWordsMetadataAreEqual(resultUserWordMetadata, userWordMetadata1);
    }

    @Test
    public void addAndDeleteTest() {
        long wordId = userWordsDao.addWord(userWordMetadata1);
        userWordMetadata1.setId(wordId);
        int rows = userWordsDao.deleteWord(userWordMetadata1);
        boolean exist = userWordsDao.checkIfWordExist(wordId);
        assertThat(rows, Matchers.greaterThan(0));
        assertFalse(exist);
    }

    @Test
    public void add_And_GetWordByWordAndUserWordsSourceId_Test() {
        userWordsDao.addWord(userWordMetadata1);
        UserWordMetadata resultUserWordMetadata = userWordsDao.getWordMetadataForWord(
                userWordMetadata1.getUserWordsSourceId(),
                userWordMetadata1.getWord()
        );
        assertThatWordsMetadataAreEqual(resultUserWordMetadata, userWordMetadata1);
    }

    @Test
    public void addAndGetWordByWordIdTest() {
        long wordId = userWordsDao.addWord(userWordMetadata1);
        UserWordMetadata resultUserWordMetadata = userWordsDao.getWordMetadataForWordId(wordId);
        assertThatWordsMetadataAreEqual(resultUserWordMetadata, userWordMetadata1);
    }

    @Test
    public void getWordsForSourceIdTest() {
        addTwoWordsInUserWordsSource2();

        SortingOrder sortingOrder = new SortingOrder(SortColumn.WORD, SortDirection.DESCENDANT);

        String query = String.format("SELECT * FROM %s WHERE %s = %s ORDER BY %s %s",
                                     TABLE_NAME, COLUMN_NAME_USER_WORDS_SOURCE_ID,
                                     userWordsSourceMetadata2.getId(),
                                     sortingOrder.getSortColumn().getSortColumnString(),
                                     sortingOrder.getSortDirection().getSortDirectionString());
        List<UserWordMetadata> resultList = db.userWordsDao().getWordsForSourceId(new SimpleSQLiteQuery(query));
        assertThat(resultList.size(), is(2));
    }

    @Test
    public void getWordsForSourceId_And_CheckIfSortingOrderIsCorrect_Test() {
        addTwoWordsInUserWordsSource2();

        SortingOrder sortingOrder = new SortingOrder(SortColumn.WORD, SortDirection.DESCENDANT);
        List<UserWordMetadata> resultList = db.userWordsDao().getWordsForSourceId(getWordsForSourceIdQueryString(sortingOrder));
        assertThat(resultList.get(0).getWord(), equalTo(userWordMetadata2.getWord()));
        assertThat(resultList.get(1).getWord(), equalTo(userWordMetadata1.getWord()));

        sortingOrder = new SortingOrder(SortColumn.WORD, SortDirection.ASCENDANT);
        resultList = db.userWordsDao().getWordsForSourceId(getWordsForSourceIdQueryString(sortingOrder));
        assertThat(resultList.get(0).getWord(), equalTo(userWordMetadata1.getWord()));
        assertThat(resultList.get(1).getWord(), equalTo(userWordMetadata2.getWord()));
    }

    @Test
    public void getWordsMinPriorityForSourceIdTest() {
        addTwoWordsInUserWordsSource2();

        int minPriority = userWordsDao.getWordsMinPriorityForSourceId(userWordsSourceMetadata2.getId());
        assertThat(minPriority, is(userWordMetadata1.getPriority()));
    }

    @Test
    public void getWordsMaxPriorityForSourceIdTest() {
        addTwoWordsInUserWordsSource2();

        int maxPriority = userWordsDao.getWordsMaxPriorityForSourceId(userWordsSourceMetadata2.getId());
        assertThat(maxPriority, is(userWordMetadata2.getPriority()));
    }

    @Test
    public void getWordsCountForSourceIdTest() {
        addTwoWordsInUserWordsSource2();

        int wordsCount = userWordsDao.getWordsCountForSourceId(userWordsSourceMetadata2.getId());
        assertThat(wordsCount, is(2));
    }

    private void assertThatWordsMetadataAreEqual(UserWordMetadata userWordMetadata1, UserWordMetadata userWordMetadata2) {
        assertThat(userWordMetadata1.getWord(),              equalTo(userWordMetadata2.getWord()));
        assertThat(userWordMetadata1.getAddTimestamp(),      equalTo(userWordMetadata2.getAddTimestamp()));
        assertThat(userWordMetadata1.getPriority(),          equalTo(userWordMetadata2.getPriority()));
        assertThat(userWordMetadata1.getUserWordsSourceId(), equalTo(userWordMetadata2.getUserWordsSourceId()));
    }

    private void addTwoWordsInUserWordsSource2() {
        long wordId = userWordsDao.addWord(userWordMetadata1);
        userWordMetadata1.setId(wordId);

        wordId = userWordsDao.addWord(userWordMetadata2);
        userWordMetadata2.setId(wordId);
    }

    private SimpleSQLiteQuery getWordsForSourceIdQueryString(SortingOrder sortingOrder) {
        String query = String.format("SELECT * FROM %s WHERE %s = %s ORDER BY %s %s",
                                     TABLE_NAME,
                                     COLUMN_NAME_USER_WORDS_SOURCE_ID,
                                     userWordsSourceMetadata2.getId(),
                                     sortingOrder.getSortColumn().getSortColumnString(),
                                     sortingOrder.getSortDirection().getSortDirectionString());
        return new SimpleSQLiteQuery(query);
    }

}

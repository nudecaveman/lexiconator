package com.caveman.lexiconator;

import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;

public class InputIntFilter implements InputFilter {

    private int min;
    private int max;

    public InputIntFilter (int min, int max) {
        if (min > max) {
            throw new IllegalStateException("Min value for filter must be less or equal than max value");
        }
        this.min = min;
        this.max = max;
    }


    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        int value = Integer.parseInt(dest.toString() + source.toString());
        if (isInRange(value)) {
            return null;
        }
        return "";
    }

    private boolean isInRange(int value) {
        return value >= min && value <= max;
    }
}

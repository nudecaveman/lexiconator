package com.caveman.lexiconator.DTO;

import com.caveman.lexiconator.model.DictionarySourceMetadata;

import java.util.List;
import java.util.Map;

public class DictionariesDTO {
    private List<DictionarySourceMetadata> dictionarySourceMetadataList;
    private Map<Long, String> userWordsSourceIdsAndBriefCodesMap;

    public DictionariesDTO(
            List<DictionarySourceMetadata> dictionarySourceMetadataList,
            Map<Long, String> userWordsSourceIdsAndBriefCodesMap
    ) {
        this.dictionarySourceMetadataList = dictionarySourceMetadataList;
        this.userWordsSourceIdsAndBriefCodesMap = userWordsSourceIdsAndBriefCodesMap;
    }

    public List<DictionarySourceMetadata> getDictionarySourceMetadataList() {
        return dictionarySourceMetadataList;
    }

    public void setDictionarySourceMetadataList(List<DictionarySourceMetadata> dictionarySourceMetadataList) {
        this.dictionarySourceMetadataList = dictionarySourceMetadataList;
    }

    public Map<Long, String> getUserWordsSourceIdsAndBriefCodesMap() {
        return userWordsSourceIdsAndBriefCodesMap;
    }

    public void setUserWordsSourceIdsAndBriefCodesMap(Map<Long, String> userWordsSourceIdsAndBriefCodesMap) {
        this.userWordsSourceIdsAndBriefCodesMap = userWordsSourceIdsAndBriefCodesMap;
    }
}
package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.view.MvpView;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public abstract class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    private V mvpView;

    @Override
    public void onAttachView(V mvpView) {
        this.mvpView = mvpView;
        updateView();
    }

    @Override
    public void onDetachView() {
        mvpView = null;
    }

    @Override
    public boolean isViewAttached() {
        return mvpView != null;
    }

    @Override
    public V getMvpView() {
        return mvpView;
    }

    protected <T> void subscribeWithSingleObserver(Single<T> singleObservable, Consumer<T> lambda) {
        singleObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<T>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(T result) {
                        try {
                            lambda.accept(result);
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    protected <T> void subscribeWithMaybeObserver(Maybe<T> maybeObservable, Consumer<T> successLambda, Runnable completeLambda) {
        maybeObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new MaybeObserver<T>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(T result) {
                        try {
                            successLambda.accept(result);
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        throw new RuntimeException(e);
                    }

                    @Override
                    public void onComplete() {
                        completeLambda.run();
                    }
                });
    }

    protected void subscribeWithCompletableObserver(Completable completable, Runnable lambda) {
        completable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onComplete() {
                        lambda.run();
                    }

                    @Override
                    public void onError(Throwable e) {
                        throw new RuntimeException(e);
                    }
                });
    }
}

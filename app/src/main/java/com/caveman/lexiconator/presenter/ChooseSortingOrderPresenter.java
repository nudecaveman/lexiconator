package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.SortingOrder;
import com.caveman.lexiconator.view.ChooseSortingOrderView;

public interface ChooseSortingOrderPresenter extends MvpPresenter<ChooseSortingOrderView> {

    void onChooseSortingOrder(SortingOrder sortingOrder);
}

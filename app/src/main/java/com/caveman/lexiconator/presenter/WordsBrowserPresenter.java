package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.WordsSourceMetadata;
import com.caveman.lexiconator.view.WordsBrowserView;

public interface WordsBrowserPresenter extends MvpChooseWordsSourcePresenter<WordsBrowserView, WordsSourceMetadata>{
}

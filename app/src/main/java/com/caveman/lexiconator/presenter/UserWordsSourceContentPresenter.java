package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.SortingOrder;
import com.caveman.lexiconator.model.room.UserWordMetadata;
import com.caveman.lexiconator.view.UserWordsSourceContentView;

public interface UserWordsSourceContentPresenter extends MvpPresenter<UserWordsSourceContentView>{

    void onWordClick(UserWordMetadata userWordMetadata);

    void onEditWordButtonClick(UserWordMetadata userWordMetadata);

    void onEditWord(String word, long wordId);

    void onDeleteWordButtonClick(UserWordMetadata userWordMetadata);

    void onAddWordButtonClick();

    void onChooseSortingOrderButtonClick();

    void onAddWord(String word);

    void onChooseCurrentSortingOrder(SortingOrder currentSortingOrder);
}

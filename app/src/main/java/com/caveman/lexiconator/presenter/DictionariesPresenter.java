package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.DictionarySourceMetadata;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.view.DictionariesView;

public interface DictionariesPresenter extends MvpPresenter<DictionariesView> {

    void onChooseUserWordsSourceButtonClick(DictionarySourceMetadata dictionarySourceMetadata);

    void onBindDictionaryToUserWordsSource(long dictionarySourceId, UserWordsSourceMetadata userWordsSourceMetadata);

    void onDeleteDictionaryButtonClick(DictionarySourceMetadata dictionarySourceMetadata);

    void onDeleteDictionary(DictionarySourceMetadata dictionarySourceMetadata);

    void onDictionaryStateSwitch(DictionarySourceMetadata dictionarySourceMetadata, boolean isChecked);
}

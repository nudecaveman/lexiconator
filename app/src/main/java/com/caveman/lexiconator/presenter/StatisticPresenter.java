package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.UserWordsSource;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.view.StatisticView;

import java.util.Date;

public interface StatisticPresenter extends MvpChooseWordsSourcePresenter<StatisticView, UserWordsSourceMetadata> {

    void onChooseDateButtonClick(int buttonId);

    void onStartDatePick(int year, int month, int day);

    void onEndDatePick(int year, int month, int day);

    void updateView();
}

package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.ChooseWordsSourceModel;
import com.caveman.lexiconator.model.WordsSourceMetadata;
import com.caveman.lexiconator.view.ChooseWordsSourceDialogView;

public class ChooseWordsSourcePresenterImpl extends BasePresenter<ChooseWordsSourceDialogView>
        implements ChooseWordsSourcePresenter {

    private ChooseWordsSourceModel chooseWordsSourceModel;

    public ChooseWordsSourcePresenterImpl(ChooseWordsSourceModel chooseWordsSourceModel) {
        this.chooseWordsSourceModel = chooseWordsSourceModel;
    }

    @Override
    public void updateView() {
        subscribeWithSingleObserver(chooseWordsSourceModel.getWordsSourcesData(), (wordsSourcesData) -> {
            if (isViewAttached()) getMvpView().showWordsSources(wordsSourcesData); });
    }

    @Override
    public void onWordsSourceClick(WordsSourceMetadata wordsSourceMetadata) {
        if (isViewAttached()) {
            getMvpView().notifyListener(wordsSourceMetadata);
            getMvpView().close();
        }
    }
}

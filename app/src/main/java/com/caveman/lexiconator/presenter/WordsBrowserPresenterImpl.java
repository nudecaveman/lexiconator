package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.DictionarySourceMetadata;
import com.caveman.lexiconator.model.WordsBrowserModel;
import com.caveman.lexiconator.model.WordsSourceMetadata;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.view.WordsBrowserView;

public class WordsBrowserPresenterImpl extends BasePresenter<WordsBrowserView>
        implements WordsBrowserPresenter {

    private WordsBrowserModel wordsBrowserModel;

    public WordsBrowserPresenterImpl(WordsBrowserModel wordsBrowserModel) {
        this.wordsBrowserModel = wordsBrowserModel;
    }

    @Override
    public void onToolbarUpdate() {
        subscribeWithMaybeObserver(
                wordsBrowserModel.getCurrentWordSourceMetadata(),
                (wordsSourceMetadata) -> {
                    if (isViewAttached()) getMvpView().updateWordSourceLabel(wordsSourceMetadata.getBriefCode());
                    },
                () -> {if (isViewAttached())getMvpView().updateWordSourceLabel("?");}
                );
    }

    @Override
    public void onChooseWordsSourceButtonClick() {
        if (isViewAttached()) getMvpView().showChooseWordsSourceDialog();
    }

    @Override
    public void onChooseWordsSource(WordsSourceMetadata wordsSourceMetadata) {
        subscribeWithCompletableObserver(wordsBrowserModel.setCurrentWordsSourceMetadata(wordsSourceMetadata), () -> {
            wordsBrowserModel.setCurrentWordsSourceMetadata(wordsSourceMetadata);
            if (wordsSourceMetadata instanceof UserWordsSourceMetadata) {
                if (isViewAttached()) getMvpView().openUserWordsSourceContent((UserWordsSourceMetadata) wordsSourceMetadata);
            }
            else if (wordsSourceMetadata instanceof DictionarySourceMetadata) {
                if (isViewAttached()) getMvpView().openDictionarySourceContent((DictionarySourceMetadata) wordsSourceMetadata);
            }
        });
    }

    @Override
    public void updateView() {
        if (isViewAttached()) {
            getMvpView()
                    .hideContentScreen()
                    .hideMessageScreen()
                    .showLoadingScreen();

            subscribeWithMaybeObserver(
                    wordsBrowserModel.getCurrentWordSourceMetadata(),
                    (wordsSourceMetadata) -> {
                        if (isViewAttached()) {
                            getMvpView()
                                    .showContentScreen()
                                    .hideLoadingScreen();
                        }
                        if (wordsSourceMetadata instanceof UserWordsSourceMetadata) {
                            if (isViewAttached())
                                getMvpView().openUserWordsSourceContent((UserWordsSourceMetadata) wordsSourceMetadata);
                        } else if (wordsSourceMetadata instanceof DictionarySourceMetadata) {
                            if (isViewAttached())
                                getMvpView().openDictionarySourceContent((DictionarySourceMetadata) wordsSourceMetadata);
                        }
                    },
                    () -> {
                        if (isViewAttached()) {
                            getMvpView().hideLoadingScreen();
                            getMvpView().showNoWordsSourcesMessageScreen();
                        }
                    });
        }
    }
}

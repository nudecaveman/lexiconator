package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.DTO.DictionariesDTO;
import com.caveman.lexiconator.model.DictionariesModel;
import com.caveman.lexiconator.model.DictionarySourceMetadata;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.view.DictionariesView;

public class DictionariesPresenterImpl extends BasePresenter<DictionariesView> implements DictionariesPresenter {

    private DictionariesModel dictionariesModel;

    public DictionariesPresenterImpl(DictionariesModel dictionariesModel) {
        this.dictionariesModel = dictionariesModel;
    }

    @Override
    public void onChooseUserWordsSourceButtonClick(DictionarySourceMetadata dictionarySourceMetadata) {
        if (isViewAttached()) getMvpView().showChooseUserWordsSourceDialog(dictionarySourceMetadata.getId());
    }

    @Override
    public void onBindDictionaryToUserWordsSource(long dictionarySourceId, UserWordsSourceMetadata userWordsSourceMetadata) {
        subscribeWithCompletableObserver(
                dictionariesModel.bindDictionaryToUserWordsSource(dictionarySourceId, userWordsSourceMetadata), () -> {
                    if (isViewAttached()) getMvpView().updateDictionariesData(); });
    }

    @Override
    public void onDeleteDictionaryButtonClick(DictionarySourceMetadata dictionarySourceMetadata) {
        String dictionaryName = dictionarySourceMetadata.getName();
        long filesSize = dictionarySourceMetadata.getDictionaryFilesSize();
        if (isViewAttached()) getMvpView().showDictionaryDeleteConfirmation(
                dictionarySourceMetadata, dictionaryName, filesSize
        );
    }

    @Override
    public void onDeleteDictionary(DictionarySourceMetadata dictionarySourceMetadata) {
        String dictionaryName = dictionarySourceMetadata.getName();
        long filesSize = dictionarySourceMetadata.getDictionaryFilesSize();
        subscribeWithCompletableObserver(dictionariesModel.deleteDictionary(dictionarySourceMetadata), () -> {
            if (isViewAttached()) getMvpView().showDictionaryIsDeleteNotification(dictionaryName, filesSize);
            updateView();
        });
    }

    @Override
    public void onDictionaryStateSwitch(DictionarySourceMetadata dictionarySourceMetadata, boolean isChecked) {
        if (isChecked && dictionarySourceMetadata.getForUserWordsSourceId() < 0) {
            if (isViewAttached()) getMvpView().showNoUserWordsSourceSpecifiedNotification();
        }
        subscribeWithCompletableObserver(
                dictionariesModel.setDictionaryIsEnable(dictionarySourceMetadata, isChecked),
                () -> {});
    }

    @Override
    public void updateView() {
        if (isViewAttached()) {
            getMvpView()
                    .hideContentScreen()
                    .hideMessageScreen()
                    .showLoadingScreen();

            subscribeWithSingleObserver(
                    dictionariesModel.scanDictionariesDirectories()
                            .zipWith(dictionariesModel.getUserWordsSourceIdsAndBriefCodesMap(), DictionariesDTO::new),

                    dictionariesDTO ->  {
                        if (isViewAttached()) {
                            getMvpView().hideLoadingScreen();
                            if (dictionariesDTO.getDictionarySourceMetadataList().size() != 0) {
                                getMvpView()
                                        .showContentScreen()
                                        .showDictionariesAndUserWordsBriefCodes(dictionariesDTO);
                            }
                            else {
                                getMvpView().showNoDictionarySourcesMessageScreen();
                            }
                        }
                    });
        }
    }
}

package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.ClipboardModel;
import com.caveman.lexiconator.model.NetworkHelper;
import com.caveman.lexiconator.model.PreferencesHelper;
import com.caveman.lexiconator.view.ClipboardView;

import java.util.concurrent.TimeUnit;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ClipboardPresenterImpl extends BasePresenter<ClipboardView> implements ClipboardPresenter {

    private PreferencesHelper preferencesHelper;
    private NetworkHelper networkHelper;
    private ClipboardModel clipboardModel;

    public ClipboardPresenterImpl(PreferencesHelper preferencesHelper, NetworkHelper networkHelper, ClipboardModel clipboardModel) {
        this.preferencesHelper = preferencesHelper;
        this.networkHelper = networkHelper;
        this.clipboardModel = clipboardModel;
    }

    @Override
    public void onShowClipboardNotification() {
        boolean clipboardQuickWordsAddIsActive = preferencesHelper.clipboardQuickWordsAddIsActive();
        String userWordsSourceName = null;

        boolean clipboardTranslateIsActive = preferencesHelper.clipboardTranslationIsActive();
        String clipboardTranslateSourceLanguage = null;
        String clipboardTranslateTargetLanguage = null;

        if (isViewAttached()) getMvpView().showClipboardNotification(
                clipboardQuickWordsAddIsActive, userWordsSourceName,
                clipboardTranslateIsActive, clipboardTranslateSourceLanguage, clipboardTranslateTargetLanguage);
    }

    @Override
    public void onTextCapture(String text) {
        if (preferencesHelper.clipboardQuickWordsAddIsActive()) {
            addWordsFromText(text);
        }
        if (preferencesHelper.clipboardTranslationIsActive()) {
            if (preferencesHelper.isMakeInternetOperationsIfWiFiOnly() && networkHelper.isWifiConnected() ||
                (!preferencesHelper.isMakeInternetOperationsIfWiFiOnly()) && networkHelper.isConnected()) {
                getTranslationFor(
                        text,
                        preferencesHelper.getClipboardTranslationSourceLang(),
                        preferencesHelper.getClipboardTranslationTargetLang()
                );
            }
        }
    }

    @Override
    public void onCloseButtonClick() {
        preferencesHelper.setClipboardQuickWordsAddIsActive(false);
        preferencesHelper.setClipboardTranslationIsActive(false);
        if (isViewAttached()) getMvpView().close();
    }

    @Override
    public void updateView() {
    }

    private void addWordsFromText(String text) {
        long userWordsSourceId = preferencesHelper.getClipboardQuickWordsAddUserWordsSourceId();
        subscribeWithMaybeObserver(
                clipboardModel.addWordsFromTextForUserWordsSource(text, userWordsSourceId),
                (userWordsSourceAndAddedWordsCountPair) -> {
                    if (isViewAttached()) getMvpView().showAddedWordsCountToast(
                            userWordsSourceAndAddedWordsCountPair.getValue1(),
                            userWordsSourceAndAddedWordsCountPair.getValue2()
                    );
                },
                () -> {}
                );
    }

    private void getTranslationFor(String text, String sourceLang, String targetLang) {
        clipboardModel.getTranslationFor(text, sourceLang, targetLang)
                .retryWhen(retry -> retry.delay(1, TimeUnit.SECONDS))
                .timeout(5, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) { }

                    @Override
                    public void onSuccess(String wordMeaning) {
                        if (isViewAttached()) getMvpView().showWordDefinition(wordMeaning);
                    }
                    @Override
                    public void onError(Throwable e) {
                        /**
                         * возможно стоит показывать плашку, что нет подключения, но она скорее всего будет мешать пользователю
                         */
                        //throw new RuntimeException(e);
                    }
                });
    }
}

package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.WordsSourceMetadata;
import com.caveman.lexiconator.view.MvpChooseWordsSourceView;

public interface MvpChooseWordsSourcePresenter<V extends MvpChooseWordsSourceView, W extends WordsSourceMetadata>
        extends MvpPresenter<V> {

    void onToolbarUpdate();

    void onChooseWordsSourceButtonClick();

    void onChooseWordsSource(W wordsSourceMetadata);
}

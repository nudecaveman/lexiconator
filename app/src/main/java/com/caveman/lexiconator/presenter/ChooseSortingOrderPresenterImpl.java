package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.CurrentSortingModel;
import com.caveman.lexiconator.model.SortingOrder;
import com.caveman.lexiconator.view.ChooseSortingOrderView;

import io.reactivex.Single;

public class ChooseSortingOrderPresenterImpl extends BasePresenter<ChooseSortingOrderView>
        implements ChooseSortingOrderPresenter {

    private CurrentSortingModel currentSortingModel;

    public ChooseSortingOrderPresenterImpl(CurrentSortingModel currentSortingModel) {
        this.currentSortingModel = currentSortingModel;
    }

    @Override
    public void onChooseSortingOrder(SortingOrder sortingOrder) {
        if (isViewAttached()) getMvpView().notifyListener(sortingOrder);
    }

    @Override
    public void updateView() {
        Single<SortingOrder> sortingOrderObservable = currentSortingModel.getCurrentSortingOrder();
        subscribeWithSingleObserver(sortingOrderObservable, (sortingOrder) -> {
            if (isViewAttached()) getMvpView().updateView(sortingOrder);});
    }
}

package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.ChooseUserWordsSourceModel;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.view.ChooseUserWordsSourceDialogView;

public class ChooseUserWordsSourcePresenterImpl
        extends BasePresenter<ChooseUserWordsSourceDialogView> implements ChooseUserWordsSourcePresenter {

    private ChooseUserWordsSourceModel chooseUserWordsSourceModel;

    public ChooseUserWordsSourcePresenterImpl(ChooseUserWordsSourceModel chooseUserWordsSourceModel) {
        this.chooseUserWordsSourceModel = chooseUserWordsSourceModel;
    }


    @Override
    public void onChooseUserWordsSource(UserWordsSourceMetadata userWordsSourceMetadata) {
        if (isViewAttached()) {
            getMvpView().notifyListenerChooseUserWordsSource(userWordsSourceMetadata);
            getMvpView().close();
        }
    }

    @Override
    public void onNoUserWordsSourcesClick() {
        if (isViewAttached()) {
            getMvpView().notifyListenerRedirectToUserWordsSources();
            getMvpView().close();
        }
    }

    @Override
    public void updateView() {
        subscribeWithSingleObserver(chooseUserWordsSourceModel.getUserWordsSourceMetadataList(), (userWordsSourceMetadataList) -> {
            if (isViewAttached()) getMvpView().showUserWordsSources(userWordsSourceMetadataList);
        });
    }
}

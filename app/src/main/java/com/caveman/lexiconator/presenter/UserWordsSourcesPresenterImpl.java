package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.UserWordsSourcesModel;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.view.UserWordsSourcesView;

import io.reactivex.Completable;

public class UserWordsSourcesPresenterImpl extends BasePresenter<UserWordsSourcesView> implements UserWordsSourcesPresenter {

    private UserWordsSourcesModel userWordsSourcesModel;

    public UserWordsSourcesPresenterImpl(UserWordsSourcesModel userWordsSourcesModel) {
        this.userWordsSourcesModel = userWordsSourcesModel;
    }

    @Override
    public void onAddWordsSourceButtonClick() {
        if (isViewAttached()) getMvpView().showAddWordsSourceDialog();
    }

    @Override
    public void onAddNewUserWordsSource(String userWordsSourceName, String userWordsSourceBriefCode) {
        Completable addNewUserWordsSourceObserver =  userWordsSourcesModel.addNewUserWordsSource(userWordsSourceName, userWordsSourceBriefCode);
        subscribeWithCompletableObserver(addNewUserWordsSourceObserver, this::updateView);
    }

    @Override
    public void onEditWordsSourceButtonClick(UserWordsSourceMetadata userWordsSourceMetadata) {
        if (isViewAttached()) getMvpView().showEditWordsSourceDialog(userWordsSourceMetadata);
    }

    @Override
    public void onEditUserWordsSource(long userWordsSourceId, String wordsSourceName, String wordsSourceBriefCode) {
        Completable editUserWordsSourceObserver = userWordsSourcesModel.editUserWordsSource(userWordsSourceId, wordsSourceName, wordsSourceBriefCode);
        subscribeWithCompletableObserver(editUserWordsSourceObserver, this::updateView);
    }

    @Override
    public void onDeleteWordsSourceButtonClick(UserWordsSourceMetadata userWordsSourceMetadata) {
        if (userWordsSourceMetadata.getWordsCount() > 0) {
            if (isViewAttached()) getMvpView().showConfirmWordsSourceDeleteDialog(userWordsSourceMetadata);
        }
        else {
            onDeleteUserWordsSource(userWordsSourceMetadata);
        }
    }

    @Override
    public void onDeleteUserWordsSource(UserWordsSourceMetadata userWordsSourceMetadata) {
        Completable deleteUserWordsSourceObserver = userWordsSourcesModel.deleteUserWordsSource(userWordsSourceMetadata);
        subscribeWithCompletableObserver(deleteUserWordsSourceObserver, this::updateView);
    }

    @Override
    public void updateView() {
        if (isViewAttached()) {
            getMvpView()
                    .hideContentScreen()
                    .hideMessageScreen()
                    .showLoadingScreen();

            subscribeWithSingleObserver(userWordsSourcesModel.getAllUserWordsSourcesMetadata(), (userWordsSourceMetadataList) -> {
                if (isViewAttached()) getMvpView().hideLoadingScreen();
                if (userWordsSourceMetadataList.size() > 0) {
                    if (isViewAttached())
                        getMvpView()
                                .showContentScreen()
                                .showUserWordsSourcesMetadataList(userWordsSourceMetadataList);
                } else {
                    if (isViewAttached()) {
                        getMvpView().showNoUserWordsSourcesMessageScreen();
                    }
                }
            });
        }
    }
}

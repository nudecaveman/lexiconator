package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.view.UserWordsSourcesView;

public interface UserWordsSourcesPresenter extends MvpPresenter<UserWordsSourcesView> {

    void onAddWordsSourceButtonClick();

    void onAddNewUserWordsSource(String userWordsSourceName, String userWordsSourceBriefCode);

    void onEditWordsSourceButtonClick(UserWordsSourceMetadata userWordsSourceMetadata);

    void onEditUserWordsSource(long userWordsSourceId, String wordsSourceName, String wordsSourceBriefCode);

    void onDeleteWordsSourceButtonClick(UserWordsSourceMetadata userWordsSourceMetadata);

    void onDeleteUserWordsSource(UserWordsSourceMetadata userWordsSourceMetadata);
}

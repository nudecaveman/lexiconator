package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.DictionaryWordMetadata;
import com.caveman.lexiconator.model.SortDirection;
import com.caveman.lexiconator.view.DictionarySourceContentView;

public interface DictionarySourceContentPresenter extends MvpPresenter<DictionarySourceContentView> {

    void onWordClick(DictionaryWordMetadata wordMetadata);

    void onChooseSortDirectionButtonClick();

    void onChooseCurrentSortDirection(SortDirection currentSortDirection);
}

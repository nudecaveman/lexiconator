package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.WordData;
import com.caveman.lexiconator.view.WordTestView;

import java.util.Map;

public interface WordTestPresenter extends MvpPresenter<WordTestView> {

    void onAnswersIdBindingsInit(Map<WordData, Integer> answersIdBindings);

    void onAnswerSelected(int answerId);

    void onAttachView(WordTestView wordTestView);

    void onDetachView();
}

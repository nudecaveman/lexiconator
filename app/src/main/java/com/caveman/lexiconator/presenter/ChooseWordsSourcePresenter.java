package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.WordsSourceMetadata;
import com.caveman.lexiconator.view.ChooseWordsSourceDialogView;

public interface ChooseWordsSourcePresenter extends MvpPresenter<ChooseWordsSourceDialogView> {

    void onWordsSourceClick(WordsSourceMetadata wordsSourceMetadata);
}

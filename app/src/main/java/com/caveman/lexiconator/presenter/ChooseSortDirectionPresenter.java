package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.SortDirection;
import com.caveman.lexiconator.view.ChooseSortDirectionView;

public interface ChooseSortDirectionPresenter extends MvpPresenter<ChooseSortDirectionView> {

    void onChooseSortDirection(SortDirection sortDirection);
}

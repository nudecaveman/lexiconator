package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.Pair;
import com.caveman.lexiconator.model.RandomWordsModel;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.view.RandomWordsView;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RandomWordsPresenterImpl extends BasePresenter<RandomWordsView> implements RandomWordsPresenter {

    private RandomWordsModel randomWordsModel;

    public RandomWordsPresenterImpl(RandomWordsModel randomWordsModel) {
        this.randomWordsModel = randomWordsModel;
    }

    @Override
    public void onWordLike(String word) {
        subscribeWithCompletableObserver(randomWordsModel.addWord(word), () -> {});
    }

    @Override
    public void onToolbarUpdate() {
        subscribeWithMaybeObserver(
                randomWordsModel.getCurrentUserWordsSourceMetadata(),
                (currentUserWordsSourceMetadata) -> {
                    if (isViewAttached()) getMvpView().updateWordSourceLabel(currentUserWordsSourceMetadata.getBriefCode());},
                () -> {if (isViewAttached()) getMvpView().updateWordSourceLabel("?");}
                );
    }

    @Override
    public void onChooseWordsSourceButtonClick() {
        if (isViewAttached()) getMvpView().showChooseWordsSourceDialog();
    }

    @Override
    public void onChooseWordsSource(UserWordsSourceMetadata wordsSourceMetadata) {
        subscribeWithCompletableObserver(randomWordsModel.setCurrentUserWordsSource(wordsSourceMetadata), () -> {
            onToolbarUpdate();
            updateView();
        });
    }

    @Override
    public void updateView() {

        if (isViewAttached()) {
            getMvpView()
                    .hideContentScreen()
                    .hideMessageScreen()
                    .showLoadingScreen();

            subscribeWithMaybeObserver(
                    getUserWordsSourceIdAndRandomWordsObservable(
                            getIsHaveCurrentDictionarySourcesObservable(),
                            getIsHaveCurrentUserWordsSourceObservable()
                    ),
                    (randomWordsPair) -> {
                        if (isViewAttached()) getMvpView().hideLoadingScreen();
                        if (randomWordsPair.getValue2().size() > 0) {
                            if (isViewAttached()) {
                                getMvpView()
                                        .showContentScreen()
                                        .showRandomWords(randomWordsPair.getValue1(), randomWordsPair.getValue2());
                            }
                        } else {
                            if (isViewAttached()) getMvpView().showNoRandomWordsMessageScreen();
                        }
                    },
                    () -> {
                    }
            );
        }
    }

    private Single<Boolean> getIsHaveCurrentDictionarySourcesObservable() {
        return randomWordsModel.isHaveCurrentDictionarySources()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess((isHaveCurrentUserWordsSource) -> {
                    if (!isHaveCurrentUserWordsSource) {
                        if (isViewAttached()) {
                            getMvpView()
                                    .hideLoadingScreen()
                                    .showNoDictionarySourcesMessageScreen();
                        }
                    }
                });
    }

    private Single<Boolean> getIsHaveCurrentUserWordsSourceObservable() {
        return randomWordsModel.isHaveUserWordsSources()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess((isHaveCurrentUserWordsSource) -> {
                    if (!isHaveCurrentUserWordsSource) {
                        if (isViewAttached()) {
                            getMvpView()
                                    .hideLoadingScreen()
                                    .showNoUserWordsSourcesMessageScreen();
                        }
                    }
                });
    }

    private Maybe<Pair<Long, List<String>>> getUserWordsSourceIdAndRandomWordsObservable(
            Single<Boolean> isHaveCurrentDictionarySourcesObservable,
            Single<Boolean> isHaveCurrentUserWordsSourceObservable
    ) {
        return isHaveCurrentDictionarySourcesObservable
                .zipWith(isHaveCurrentUserWordsSourceObservable, (isHaveCurrentDictionarySources, isHaveCurrentUserWordsSource) ->
                        isHaveCurrentDictionarySources && isHaveCurrentUserWordsSource)
                .observeOn(Schedulers.io())
                .filter(haveAllResources -> haveAllResources)
                .flatMap(haveAllResources -> randomWordsModel.getRandomWords().toMaybe())
        ;
    }
}

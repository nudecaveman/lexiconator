package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.CurrentSortingModel;
import com.caveman.lexiconator.model.SortDirection;
import com.caveman.lexiconator.view.ChooseSortDirectionView;

public class ChooseSortDirectionPresenterImpl
        extends BasePresenter<ChooseSortDirectionView> implements ChooseSortDirectionPresenter {

    private CurrentSortingModel currentSortingModel;

    public ChooseSortDirectionPresenterImpl(CurrentSortingModel currentSortingModel) {
        this.currentSortingModel = currentSortingModel;
    }

    @Override
    public void onChooseSortDirection(SortDirection sortDirection) {
        if (isViewAttached()) getMvpView().notifyListener(sortDirection);
    }

    @Override
    public void updateView() {
        subscribeWithSingleObserver(currentSortingModel.getCurrentDictionarySourceSortDirection(), (currentSortDirection) -> {
            if (isViewAttached()) getMvpView().updateView(currentSortDirection);});
    }
}

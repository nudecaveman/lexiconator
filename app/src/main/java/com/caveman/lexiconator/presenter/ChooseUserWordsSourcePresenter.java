package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.view.ChooseUserWordsSourceDialogView;

public interface ChooseUserWordsSourcePresenter  extends MvpPresenter<ChooseUserWordsSourceDialogView> {

    void onChooseUserWordsSource(UserWordsSourceMetadata userWordsSourceMetadata);

    void onNoUserWordsSourcesClick();
}

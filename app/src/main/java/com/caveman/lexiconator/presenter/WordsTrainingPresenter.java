package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.view.WordsTrainingView;

public interface WordsTrainingPresenter extends MvpChooseWordsSourcePresenter<WordsTrainingView, UserWordsSourceMetadata> {

    void onWordsRunOut();

    void onTestPass();

    void onNextTestButtonClick();
}

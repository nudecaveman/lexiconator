package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.view.ClipboardView;

public interface ClipboardPresenter extends MvpPresenter<ClipboardView> {

    void onShowClipboardNotification();

    void onTextCapture(String text);

    void onCloseButtonClick();
}

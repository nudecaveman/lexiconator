package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.view.WordCardView;

public interface WordCardPresenter extends MvpPresenter<WordCardView> {
}

package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.StatisticModel;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.view.StatisticView;

import java.util.Calendar;

public class StatisticPresenterImpl extends BasePresenter<StatisticView> implements StatisticPresenter {

    private StatisticModel statisticModel;

    public StatisticPresenterImpl(StatisticModel statisticModel) {
        this.statisticModel = statisticModel;
    }

    @Override
    public void onChooseDateButtonClick(int buttonId) {
        if (isViewAttached()) getMvpView().showDatePicker(
                buttonId,
                statisticModel.getStartDate(),
                statisticModel.getEndDate());
    }

    @Override
    public void onStartDatePick(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        statisticModel.setStartDate(calendar.getTime());
        updateView();
    }

    @Override
    public void onEndDatePick(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        statisticModel.setEndDate(calendar.getTime());
        updateView();
    }

    @Override
    public void onToolbarUpdate() {
        subscribeWithMaybeObserver(
                statisticModel.getCurrentUserWordsSourceMetadata(),
                (currentUserWordsSourceMetadata) -> {
                    if (isViewAttached()) getMvpView().updateWordSourceLabel(currentUserWordsSourceMetadata.getBriefCode());
                    },
                () -> {if (isViewAttached()) getMvpView().updateWordSourceLabel("?");}
                );
    }

    @Override
    public void onChooseWordsSourceButtonClick() {
        if (isViewAttached()) getMvpView().showChooseWordsSourceDialog();
    }

    @Override
    public void onChooseWordsSource(UserWordsSourceMetadata wordsSourceMetadata) {
        subscribeWithCompletableObserver(statisticModel.setCurrentUserWordsSource(wordsSourceMetadata), () -> {
            onToolbarUpdate();
            updateView();
        });
    }

    @Override
    public void updateView() {
        if (isViewAttached()) {
            getMvpView()
                    .hideContentScreen()
                    .hideMessageScreen()
                    .showLoadingScreen();

            subscribeWithSingleObserver(statisticModel.isHaveUserWordsSources(), (isHasCurrentUserWordsSource) -> {

                if (isHasCurrentUserWordsSource) {
                    if (isViewAttached()) getMvpView().updateDateButtons(statisticModel.getStartDate(), statisticModel.getEndDate());
                    subscribeWithSingleObserver(statisticModel.getAddedWordsInDayStatistic(), (wordsInDayStatistic) -> {
                        if (isViewAttached()) {
                            getMvpView()
                                    .hideLoadingScreen()
                                    .showContentScreen()
                                    .buildGraph(wordsInDayStatistic);
                        }
                    });
                    subscribeWithSingleObserver(statisticModel.getWordsCountStatistic(), (wordsCountStatistic) -> {
                        if (isViewAttached()) getMvpView().showWordsCountStatistic(wordsCountStatistic);
                    });
                } else {
                    if (isViewAttached()) {
                        getMvpView().hideLoadingScreen();
                        getMvpView().showNoUserWordsSourcesMessageScreen();
                    }
                }
            });
        }
    }
}

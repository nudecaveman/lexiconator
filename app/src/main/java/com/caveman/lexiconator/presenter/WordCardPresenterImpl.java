package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.MeaningConfiguration;
import com.caveman.lexiconator.model.MeaningConfigurationBuilder;
import com.caveman.lexiconator.model.PreferencesHelper;
import com.caveman.lexiconator.model.WordCardModel;
import com.caveman.lexiconator.view.WordCardView;

public class WordCardPresenterImpl extends BasePresenter<WordCardView> implements WordCardPresenter {
    private WordCardModel wordCardModel;
    private PreferencesHelper preferencesHelper;
    private MeaningConfigurationBuilder meaningConfigurationBuilder;

    public WordCardPresenterImpl(
            WordCardModel wordCardModel,
            PreferencesHelper preferencesHelper,
            MeaningConfigurationBuilder meaningConfigurationBuilder
    ) {
        this.wordCardModel = wordCardModel;
        this.preferencesHelper = preferencesHelper;
        this.meaningConfigurationBuilder = meaningConfigurationBuilder;
    }

    @Override
    public void updateView() {
        if (isViewAttached()) {
            boolean fromSpecificDictionary = getMvpView().getFromSpecificDictionaryArg();
            long wordsSourceSourceId = getMvpView().getWordSourceIdArg();
            String word = getMvpView().getWordArg();

            boolean processTags = preferencesHelper.isProcessTags();

            MeaningConfiguration meaningConfiguration = getMeaningConfiguration();

            if (fromSpecificDictionary) {
                subscribeWithSingleObserver(
                        wordCardModel.findWordFromDictionary(wordsSourceSourceId, word, meaningConfiguration), (wordDataList) -> {
                    if (isViewAttached()) getMvpView().updateView(wordDataList, processTags);
                });
            }
            else {
                subscribeWithSingleObserver(
                        wordCardModel.findWordForUserWordsSource(wordsSourceSourceId, word, meaningConfiguration), (wordDataList) -> {
                    if (isViewAttached()) getMvpView().updateView(wordDataList, processTags);
                });
            }
        }
    }

    private MeaningConfiguration getMeaningConfiguration() {
        return meaningConfigurationBuilder
                .restrictMeaningsCount(preferencesHelper.isRestrictMeaningsCount())
                .maxMeaningsCount(preferencesHelper.getMaxMeaningsCount())
                .meaningsOnNewLines(preferencesHelper.isMeaningsOnNewLines())
                .processTags(preferencesHelper.isProcessTags())
                .build();
    }
}

package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.DictionarySourceContentModel;
import com.caveman.lexiconator.model.DictionaryWordMetadata;
import com.caveman.lexiconator.model.SortDirection;
import com.caveman.lexiconator.view.DictionarySourceContentView;

import io.reactivex.Completable;

public class DictionarySourceContentPresenterImpl
        extends BasePresenter<DictionarySourceContentView> implements DictionarySourceContentPresenter {

    private DictionarySourceContentModel dictionarySourceContentModel;

    public DictionarySourceContentPresenterImpl(DictionarySourceContentModel dictionaryWordSourceContentModel) {
        this.dictionarySourceContentModel = dictionaryWordSourceContentModel;
    }

    @Override
    public void onAttachView(DictionarySourceContentView view) {
        long dictionaryWordsSourceId = view.getWordSourceId();
        Completable setCurrentUserWordSourceObserver = dictionarySourceContentModel.setCurrentDictionaryWordSource(dictionaryWordsSourceId);
        subscribeWithCompletableObserver(setCurrentUserWordSourceObserver, () -> super.onAttachView(view));
    }

    @Override
    public void onWordClick(DictionaryWordMetadata wordMetadata) {
        if (isViewAttached()) getMvpView().getNavigator().openWordCardActivity(
                true,
                wordMetadata.getDictionarySourceId(),
                wordMetadata.getWord()
        );
    }

    @Override
    public void onChooseSortDirectionButtonClick() {
        if (isViewAttached()) getMvpView().showChooseSortingDialog();
    }

    @Override
    public void onChooseCurrentSortDirection(SortDirection currentSortDirection) {
        subscribeWithCompletableObserver(dictionarySourceContentModel.setCurrentSortDirection(currentSortDirection),
                                         this::updateView);
    }

    @Override
    public void updateView() {
        if (isViewAttached()) {
            getMvpView().clearWords();
            getMvpView().hideMessageScreen();
            getMvpView().showLoadingScreen();
        }
        subscribeWithSingleObserver(dictionarySourceContentModel.getWordsMetadataList(), (wordMetadataList) -> {
            if (isViewAttached()) getMvpView().hideLoadingScreen();
            if (wordMetadataList.size() > 0) {
                if (isViewAttached()) getMvpView().showWords(wordMetadataList);
            }
            else {
                if (isViewAttached()) getMvpView().showNoWordsMessageScreen();
            }
        });

        subscribeWithSingleObserver(dictionarySourceContentModel.getWordsCounter(), (counter) -> {
            if (isViewAttached()) getMvpView().showWordsCounter(counter);
        });
    }
}

package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.MeaningConfiguration;
import com.caveman.lexiconator.model.MeaningConfigurationBuilder;
import com.caveman.lexiconator.model.PreferencesHelper;
import com.caveman.lexiconator.model.WordData;
import com.caveman.lexiconator.model.WordTestData;
import com.caveman.lexiconator.model.WordTestModel;
import com.caveman.lexiconator.view.WordTestView;

import java.util.Map;

import io.reactivex.Completable;

public class WordTestPresenterImpl extends BasePresenter<WordTestView> implements WordTestPresenter {

    private WordTestModel wordTestModel;
    private PreferencesHelper preferencesHelper;
    private MeaningConfigurationBuilder meaningConfigurationBuilder;
    private WordTestData wordTestData;
    private Map<WordData, Integer> answersIdBindings;

    public WordTestPresenterImpl(
            WordTestModel wordTestModel,
            PreferencesHelper preferencesHelper,
            MeaningConfigurationBuilder meaningConfigurationBuilder
    ) {
        this.wordTestModel = wordTestModel;
        this.preferencesHelper = preferencesHelper;
        this.meaningConfigurationBuilder = meaningConfigurationBuilder;
    }

    @Override
    public void onAnswersIdBindingsInit(Map<WordData, Integer> answersIdBindings) {
        this.answersIdBindings = answersIdBindings;
    }

    @Override
    public void onAnswerSelected(int answerId) {
        Integer rightAnswerId = answersIdBindings.get(wordTestData.getQuestionData());

        Completable testResultObservable =
                (answerId == rightAnswerId) ?
                wordTestModel.countWordTestSuccess(wordTestData.getUserWordMetadata()) :
                wordTestModel.countWordTestFail(wordTestData.getUserWordMetadata());

        subscribeWithCompletableObserver(testResultObservable, () -> {
            if (isViewAttached()) {
                getMvpView().showAnswersWithColors(answerId, rightAnswerId);
                getMvpView().disableVariants();
                getMvpView().notifyTestPassListener();
            }
        });
    }

    @Override
    public void updateView() {
        if (isViewAttached()) {
            long wordsSourceSourceId = getMvpView().getWordSourceIdArg();
            String word = getMvpView().getWordArg();

            subscribeWithSingleObserver(
                    wordTestModel.getWordTestData(word, wordsSourceSourceId, getMeaningConfiguration()), (wordTestData) -> {
                        WordTestPresenterImpl.this.wordTestData = wordTestData;
                        if (isViewAttached()) {
                            if (isViewAttached())
                                getMvpView().updateView(wordTestData, preferencesHelper.isProcessTags());
                        }
                    });
        }
    }

    private MeaningConfiguration getMeaningConfiguration() {
        return meaningConfigurationBuilder
                .restrictMeaningsCount(preferencesHelper.isRestrictMeaningsCount())
                .maxMeaningsCount(preferencesHelper.getMaxMeaningsCount())
                .meaningsOnNewLines(preferencesHelper.isMeaningsOnNewLines())
                .processTags(preferencesHelper.isProcessTags())
                .build();
    }
}

package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.view.MvpView;

public interface MvpPresenter<V extends MvpView> {

    void onAttachView(V mvpView);

    void onDetachView();

    boolean isViewAttached();

    V getMvpView();

    void updateView();
}

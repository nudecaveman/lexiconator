package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.SortingOrder;
import com.caveman.lexiconator.model.UserWordsSourceContentModel;
import com.caveman.lexiconator.model.room.UserWordMetadata;
import com.caveman.lexiconator.view.UserWordsSourceContentView;

import io.reactivex.Completable;
import io.reactivex.Single;

public class UserWordsSourceContentPresenterImpl extends BasePresenter<UserWordsSourceContentView>
        implements UserWordsSourceContentPresenter {

    private UserWordsSourceContentModel userWordsSourceContentModel;

    public UserWordsSourceContentPresenterImpl(UserWordsSourceContentModel userWordsSourceContentModel) {
        this.userWordsSourceContentModel = userWordsSourceContentModel;
    }

    @Override
    public void onAttachView(UserWordsSourceContentView view) {
        long userWordsSourceId = view.getWordSourceId();
        Completable setCurrentUserWordSourceObserver = userWordsSourceContentModel.setCurrentWordSourceId(userWordsSourceId);
        subscribeWithCompletableObserver(setCurrentUserWordSourceObserver, () -> super.onAttachView(view));
    }

    @Override
    public void onWordClick(UserWordMetadata userWordMetadata) {
        subscribeWithSingleObserver(userWordsSourceContentModel.isHaveDictionarySources(), (isHaveDictionarySources) -> {
            if (isHaveDictionarySources) {
                if (isViewAttached()) getMvpView().getNavigator().openWordCardActivity(
                        false,
                        getMvpView().getWordSourceId(),
                        userWordMetadata.getWord()
                );
            }
            else {
                if (isViewAttached()) getMvpView().showCannotOpenWordCardToast();
            }
        });
    }

    @Override
    public void onEditWordButtonClick(UserWordMetadata userWordMetadata) {
        if (isViewAttached()) getMvpView().showEditWordDialog(userWordMetadata.getWord(), userWordMetadata.getId());
    }

    @Override
    public void onEditWord(String word, long wordId) {
        subscribeWithCompletableObserver(userWordsSourceContentModel.editWord(word, wordId), this::updateView);
    }

    @Override
    public void onDeleteWordButtonClick(UserWordMetadata userWordMetadata) {
        Completable deleteWordCompletable = userWordsSourceContentModel.deleteWord(userWordMetadata);
        subscribeWithCompletableObserver(deleteWordCompletable, this::updateView);
    }

    @Override
    public void onAddWordButtonClick() {
        if (isViewAttached()) getMvpView().showAddWordDialog();
    }

    @Override
    public void onChooseSortingOrderButtonClick() {
        if (isViewAttached()) getMvpView().showChooseSortingDialog();
    }

    @Override
    public void onAddWord(String word) {
        String input = word.toLowerCase().trim();
        if (!input.isEmpty()) {
            Completable addWordCompletable = userWordsSourceContentModel.addWord(word);
            subscribeWithCompletableObserver(addWordCompletable, this::updateView);
        }
    }

    @Override
    public void onChooseCurrentSortingOrder(SortingOrder currentSortingOrder) {
        Completable setCurrentSortingOrderObserver = userWordsSourceContentModel.setCurrentSortingOrder(currentSortingOrder);
        subscribeWithCompletableObserver(setCurrentSortingOrderObserver, this::updateView);
    }

    @Override
    public void updateView() {
        if (isViewAttached()) {
        getMvpView().clearWords();
        getMvpView().hideMessageScreen();
        getMvpView().showLoadingScreen();
        }
        subscribeWithSingleObserver(userWordsSourceContentModel.getWordsMetadataList(), (wordsMetadataList) -> {
            if (isViewAttached()) getMvpView().hideLoadingScreen();
            if (wordsMetadataList.size() > 0) {
                subscribeWithSingleObserver(userWordsSourceContentModel.getWordsMinMaxPriorityData(), (userWordsMinMaxPriorityData) -> {
                    if (isViewAttached()) getMvpView().showWords(wordsMetadataList, userWordsMinMaxPriorityData);
                });
            }
            else {
                if (isViewAttached()) getMvpView().showNoWordsMessageScreen();
            }
        });

        Single<Integer> counterObservable = userWordsSourceContentModel.getWordsCounter();
        subscribeWithSingleObserver(counterObservable, (wordsCounter) -> {
            if (isViewAttached()) getMvpView().showWordsCounter(wordsCounter);
        });
    }
}

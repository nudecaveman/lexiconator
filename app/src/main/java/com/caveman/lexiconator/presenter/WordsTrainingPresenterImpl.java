package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.WordsTrainingModel;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.view.WordsTrainingView;

public class WordsTrainingPresenterImpl extends BasePresenter<WordsTrainingView> implements WordsTrainingPresenter {

    private WordsTrainingModel wordsTrainingModel;

    public WordsTrainingPresenterImpl(WordsTrainingModel wordsTrainingModel) {
        this.wordsTrainingModel = wordsTrainingModel;
    }

    @Override
    public void onWordsRunOut() {
        addWordsForTest();
    }

    @Override
    public void onTestPass() {
        if (isViewAttached()) getMvpView().showNextButton();
    }

    @Override
    public void onNextTestButtonClick() {
        if (isViewAttached()) getMvpView().showNextTest();
    }

    @Override
    public void onToolbarUpdate() {
        subscribeWithMaybeObserver(
                wordsTrainingModel.getCurrentUserWordsSourceMetadata(),
                (currentUserWordsSourceMetadata) -> {
                    if (isViewAttached()) getMvpView().updateWordSourceLabel(currentUserWordsSourceMetadata.getBriefCode());
                },
                () -> {if (isViewAttached()) getMvpView().updateWordSourceLabel("?");}
        );
    }

    @Override
    public void onChooseWordsSourceButtonClick() {
        if (isViewAttached()) getMvpView().showChooseWordsSourceDialog();
    }

    @Override
    public void onChooseWordsSource(UserWordsSourceMetadata wordsSourceMetadata) {
        subscribeWithCompletableObserver(wordsTrainingModel.setCurrentUserWordsSource(wordsSourceMetadata), () -> {
            if (isViewAttached()) getMvpView().resetTests();
            onToolbarUpdate();
            updateView();
        });
    }

    @Override
    public void updateView() {
        addWordsForTest();
    }

    private void addWordsForTest() {
        if (isViewAttached()) {
            getMvpView()
                    .hideMessageScreen();
            //getMvpView().showLoadingScreen();
            subscribeWithSingleObserver(wordsTrainingModel.isHaveUserWordsSources(), (isHasCurrentUserWordsSource) -> {
                if (isHasCurrentUserWordsSource) {
                    subscribeWithSingleObserver(wordsTrainingModel.isHasCurrentDictionarySources(), (isHaveCurrentDictionarySources) -> {
                        if (isHaveCurrentDictionarySources) {
                            subscribeWithSingleObserver(wordsTrainingModel.getUserWordsList(), (userWordsMetadataList) -> {
                                //getMvpView().hideLoadingScreen();
                                if (userWordsMetadataList.size() > 0) {
                                    if (isViewAttached())
                                        getMvpView().addWordsTests(userWordsMetadataList);
                                } else {
                                    if (isViewAttached())
                                        getMvpView().showNoUserWordsMessageScreen();
                                }
                            });
                        } else {
                            //getMvpView().hideLoadingScreen();
                            if (isViewAttached())
                                getMvpView().showNoDictionarySourcesMessageScreen();
                        }
                    });
                } else {
                    //getMvpView().hideLoadingScreen();
                    if (isViewAttached()) getMvpView().showNoUserWordsSourcesMessageScreen();
                }
            });
        }
    }
}

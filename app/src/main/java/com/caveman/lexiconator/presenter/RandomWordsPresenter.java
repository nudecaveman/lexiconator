package com.caveman.lexiconator.presenter;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.view.MvpChooseWordsSourceView;
import com.caveman.lexiconator.view.RandomWordsView;

public interface RandomWordsPresenter extends MvpChooseWordsSourcePresenter<RandomWordsView, UserWordsSourceMetadata> {

    void onWordLike(String word);
}

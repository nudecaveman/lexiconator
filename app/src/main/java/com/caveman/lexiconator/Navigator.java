package com.caveman.lexiconator;

public interface Navigator {

    void openWordsBrowserActivity();

    void openRandomWordsActivity();

    void openWordsTrainingActivity();

    void openWordCardActivity(boolean fromSpecificDictionary, long wordsSourceId, String word);

    void openUserWordsSourcesActivity();

    void openDictionariesActivity();

    void openStatisticActivity();

    void openSettingsActivity();
}

package com.caveman.lexiconator.dependency_injection;

import com.caveman.lexiconator.LexiconatorApplication;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    ActivityComponent activityComponent(ActivityModule activityModule);

    FragmentComponent fragmentComponent(FragmentModule fragmentModule);

    ServiceComponent serviceComponent(ServiceModule serviceModule);

    void inject(LexiconatorApplication application);
}

package com.caveman.lexiconator.dependency_injection;

import com.caveman.lexiconator.model.ChooseUserWordsSourceModel;
import com.caveman.lexiconator.model.ChooseUserWordsSourceModelImpl;
import com.caveman.lexiconator.model.ChooseWordsSourceModel;
import com.caveman.lexiconator.model.ChooseWordsSourceModelImpl;
import com.caveman.lexiconator.model.CurrentDictionariesHelper;
import com.caveman.lexiconator.model.CurrentSortingModel;
import com.caveman.lexiconator.model.CurrentSortingModelImpl;
import com.caveman.lexiconator.model.CurrentSortingRepository;
import com.caveman.lexiconator.model.DictionarySourceContentModel;
import com.caveman.lexiconator.model.DictionarySourceContentModelImpl;
import com.caveman.lexiconator.model.MeaningConfigurationBuilder;
import com.caveman.lexiconator.model.PreferencesHelper;
import com.caveman.lexiconator.model.UserWordsSourceContentModel;
import com.caveman.lexiconator.model.UserWordsSourceContentModelImpl;
import com.caveman.lexiconator.model.UserWordsSourcesHelper;
import com.caveman.lexiconator.model.WordCardModel;
import com.caveman.lexiconator.model.WordCardModelImpl;
import com.caveman.lexiconator.model.WordTestModel;
import com.caveman.lexiconator.model.WordTestModelImpl;
import com.caveman.lexiconator.presenter.ChooseSortDirectionPresenter;
import com.caveman.lexiconator.presenter.ChooseSortDirectionPresenterImpl;
import com.caveman.lexiconator.presenter.ChooseSortingOrderPresenter;
import com.caveman.lexiconator.presenter.ChooseSortingOrderPresenterImpl;
import com.caveman.lexiconator.presenter.ChooseUserWordsSourcePresenter;
import com.caveman.lexiconator.presenter.ChooseUserWordsSourcePresenterImpl;
import com.caveman.lexiconator.presenter.ChooseWordsSourcePresenter;
import com.caveman.lexiconator.presenter.ChooseWordsSourcePresenterImpl;
import com.caveman.lexiconator.presenter.DictionarySourceContentPresenter;
import com.caveman.lexiconator.presenter.DictionarySourceContentPresenterImpl;
import com.caveman.lexiconator.presenter.UserWordsSourceContentPresenter;
import com.caveman.lexiconator.presenter.UserWordsSourceContentPresenterImpl;
import com.caveman.lexiconator.presenter.WordCardPresenter;
import com.caveman.lexiconator.presenter.WordCardPresenterImpl;
import com.caveman.lexiconator.presenter.WordTestPresenter;
import com.caveman.lexiconator.presenter.WordTestPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class FragmentModule {

    public FragmentModule() {
    }

    @Provides
    @PerFragment
    public UserWordsSourceContentModel providesUserWordsSourceContentModel(
            UserWordsSourcesHelper userWordsSourcesHelper,
            CurrentDictionariesHelper currentDictionariesHelper,
            CurrentSortingRepository currentSortingRepository) {
        return new UserWordsSourceContentModelImpl(userWordsSourcesHelper, currentDictionariesHelper, currentSortingRepository);
    }

    @Provides
    @PerFragment
    public UserWordsSourceContentPresenter providesUserWordsSourceContentPresenter(
            UserWordsSourceContentModel userWordsSourceContentModel) {
        return new UserWordsSourceContentPresenterImpl(userWordsSourceContentModel);
    }

    @Provides
    @PerFragment
    public DictionarySourceContentModel providesDictionarySourceContentModel(
            CurrentDictionariesHelper currentDictionariesHelper,
            CurrentSortingRepository currentSortingRepository) {
        return new DictionarySourceContentModelImpl(currentDictionariesHelper, currentSortingRepository);
    }

    @Provides
    @PerFragment
    public DictionarySourceContentPresenter providesDictionarySourceContentPresenter(
            DictionarySourceContentModel dictionarySourceContentModel) {
        return new DictionarySourceContentPresenterImpl(dictionarySourceContentModel);
    }

    @Provides
    @PerFragment
    public CurrentSortingModel providesCurrentSortingModel(CurrentSortingRepository currentSortingRepository) {
        return new CurrentSortingModelImpl(currentSortingRepository);
    }

    @Provides
    @PerFragment
    public ChooseSortingOrderPresenter providesChooseSortingOrderPresenter(
            CurrentSortingModel currentSortingModel) {
        return new ChooseSortingOrderPresenterImpl(currentSortingModel);
    }

    @Provides
    @PerFragment
    public ChooseSortDirectionPresenter providesChooseSortDirectionPresenter(
            CurrentSortingModel currentSortingModel) {
        return new ChooseSortDirectionPresenterImpl(currentSortingModel);
    }

    @Provides
    @PerFragment
    public ChooseWordsSourceModel providesChooseWordsSourceModel(
            UserWordsSourcesHelper userWordsSourcesHelper, CurrentDictionariesHelper currentDictionariesHelper) {
        return new ChooseWordsSourceModelImpl(userWordsSourcesHelper, currentDictionariesHelper);
    }

    @Provides
    @PerFragment
    public ChooseWordsSourcePresenter providesChooseWordsSourcePresenter(ChooseWordsSourceModel chooseWordsSourceModel) {
        return new ChooseWordsSourcePresenterImpl(chooseWordsSourceModel);
    }

    @Provides
    @PerFragment
    public ChooseUserWordsSourceModel providesChooseUserWordsSourceModel(UserWordsSourcesHelper userWordsSourcesHelper) {
        return new ChooseUserWordsSourceModelImpl(userWordsSourcesHelper);
    }

    @Provides
    @PerFragment
    public ChooseUserWordsSourcePresenter providesChooseUserWordsSourcePresenter(ChooseUserWordsSourceModel chooseWordsSourceModel) {
        return new ChooseUserWordsSourcePresenterImpl(chooseWordsSourceModel);
    }

    @Provides
    public WordCardModel providesWordCardModel(UserWordsSourcesHelper userWordsSourcesHelper, CurrentDictionariesHelper currentDictionariesHelper) {
        return new WordCardModelImpl(userWordsSourcesHelper, currentDictionariesHelper);
    }

    @Provides
    public WordCardPresenter providesWordCardPresenter(
            WordCardModel wordCardModel,
            PreferencesHelper preferencesHelper,
            MeaningConfigurationBuilder meaningConfigurationBuilder
    ) {
        return new WordCardPresenterImpl(wordCardModel, preferencesHelper, meaningConfigurationBuilder);
    }

    @Provides
    public WordTestModel providesWordTestModel(UserWordsSourcesHelper userWordsSourcesHelper, CurrentDictionariesHelper currentDictionariesHelper) {
        return new WordTestModelImpl(userWordsSourcesHelper, currentDictionariesHelper);
    }

    @Provides
    public WordTestPresenter providesWordTestPresenter(
            WordTestModel wordTestModel,
            PreferencesHelper preferencesHelper,
            MeaningConfigurationBuilder meaningConfigurationBuilder
    ) {
        return new WordTestPresenterImpl(wordTestModel, preferencesHelper, meaningConfigurationBuilder);
    }

    @Provides
    public MeaningConfigurationBuilder providesMeaningConfigurationBuilder() {
        return new MeaningConfigurationBuilder();
    }
}

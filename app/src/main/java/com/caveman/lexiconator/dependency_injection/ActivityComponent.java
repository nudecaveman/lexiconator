package com.caveman.lexiconator.dependency_injection;

import com.caveman.lexiconator.view.DictionariesActivity;
import com.caveman.lexiconator.view.RandomWordsActivity;
import com.caveman.lexiconator.view.StatisticActivity;
import com.caveman.lexiconator.view.UserWordsSourcesActivity;
import com.caveman.lexiconator.view.WordsBrowserActivity;
import com.caveman.lexiconator.view.WordsTrainingActivity;

import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(WordsBrowserActivity wordsBrowserActivity);

    void inject(RandomWordsActivity randomWordsActivity);

    void inject(WordsTrainingActivity wordsTrainingActivity);

    void inject(UserWordsSourcesActivity userWordsSourcesActivity);

    void inject(DictionariesActivity dictionariesActivity);

    void inject(StatisticActivity statisticActivity);
}

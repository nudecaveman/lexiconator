package com.caveman.lexiconator.dependency_injection;

import com.caveman.lexiconator.view.ChooseSortDirectionDialogFragment;
import com.caveman.lexiconator.view.ChooseSortingOrderDialogFragment;
import com.caveman.lexiconator.view.ChooseUserWordsSourceDialogFragment;
import com.caveman.lexiconator.view.ChooseWordsSourceDialogFragment;
import com.caveman.lexiconator.view.DictionarySourceContentFragment;
import com.caveman.lexiconator.view.SettingsActivity;
import com.caveman.lexiconator.view.UserWordsSourceContentFragment;
import com.caveman.lexiconator.view.WordCardFragment;
import com.caveman.lexiconator.view.WordTestMeaningAndWordsFragment;

import dagger.Subcomponent;

@PerFragment
@Subcomponent(modules = FragmentModule.class)
public interface FragmentComponent {

    void inject(UserWordsSourceContentFragment userWordsSourceContentFragment);

    void inject(DictionarySourceContentFragment dictionarySourceContentFragment);

    void inject(ChooseSortingOrderDialogFragment chooseSortingOrderDialogFragment);

    void inject(ChooseSortDirectionDialogFragment chooseSortDirectionDialogFragment);

    void inject(ChooseWordsSourceDialogFragment chooseWordsSourceDialogFragment);

    void inject(ChooseUserWordsSourceDialogFragment chooseUserWordsSourceDialogFragment);

    void inject(WordCardFragment wordCardFragment);

    void inject(WordTestMeaningAndWordsFragment wordTestMeaningAndWordsFragment);

    void inject(SettingsActivity.MainSettingsFragment mainSettingsFragment);
}

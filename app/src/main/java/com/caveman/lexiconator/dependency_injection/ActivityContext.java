package com.caveman.lexiconator.dependency_injection;

import javax.inject.Qualifier;

@Qualifier
@interface ActivityContext {}

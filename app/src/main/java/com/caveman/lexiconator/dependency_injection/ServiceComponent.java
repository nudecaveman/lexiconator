package com.caveman.lexiconator.dependency_injection;

import com.caveman.lexiconator.view.ClipboardService;

import dagger.Subcomponent;

@PerService
@Subcomponent(modules = ServiceModule.class)
public interface ServiceComponent {

    void inject(ClipboardService clipboardService);
}

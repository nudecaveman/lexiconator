package com.caveman.lexiconator.dependency_injection;

import android.app.Application;
import android.content.Context;

import com.caveman.lexiconator.model.AndroidPreferencesHelper;
import com.caveman.lexiconator.model.CurrentBrowserWordsSourceRepository;
import com.caveman.lexiconator.model.CurrentBrowserWordsSourceRepositoryImpl;
import com.caveman.lexiconator.model.CurrentDictionariesHelper;
import com.caveman.lexiconator.model.CurrentDictionariesHelperImpl;
import com.caveman.lexiconator.model.CurrentDictionariesRepository;
import com.caveman.lexiconator.model.CurrentDictionariesRepositoryImpl;
import com.caveman.lexiconator.model.CurrentSortingRepository;
import com.caveman.lexiconator.model.CurrentSortingRepositoryImpl;
import com.caveman.lexiconator.model.CurrentUserWordsSourcesRepository;
import com.caveman.lexiconator.model.CurrentUserWordsSourcesRepositoryImpl;
import com.caveman.lexiconator.model.DictionariesDirectoryHelper;
import com.caveman.lexiconator.model.DictionariesDirectoryHelperImpl;
import com.caveman.lexiconator.model.GoogleTranslateModel.WordDataGTHelperImpl;
import com.caveman.lexiconator.model.NetworkHelper;
import com.caveman.lexiconator.model.NetworkHelperImpl;
import com.caveman.lexiconator.model.PreferencesHelper;
import com.caveman.lexiconator.model.UserWordsSourcesHelper;
import com.caveman.lexiconator.model.WordDataHelper;
import com.caveman.lexiconator.model.WordsStatisticHelper;
import com.caveman.lexiconator.model.room.UserWordsSourcesSqliteHelper;
import com.caveman.lexiconator.model.room.UserWordsSqliteSourcesDatabase;
import com.caveman.lexiconator.model.room.WordsSqliteStatisticHelper;

import java.io.File;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static android.content.Context.MODE_PRIVATE;

@Module
public class ApplicationModule {

    private Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @ApplicationContext
    public Context providesApplicationContext() {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    public File getDictionariesDirectory() {
        return application.getDir("Lexiconator", MODE_PRIVATE);
    }

    @Provides
    @Singleton
    public DictionariesDirectoryHelper providesDictionariesDirectoryHelper(
            @ApplicationContext Context context, File defaultDictionariesDirectory, PreferencesHelper preferencesHelper){
        return new DictionariesDirectoryHelperImpl(context, defaultDictionariesDirectory, preferencesHelper);
    }


    @Provides
    @Singleton
    public UserWordsSourcesHelper providesUserWordsSourceHelper(@ApplicationContext Context context) {
        return new UserWordsSourcesSqliteHelper(context);
    }

    @Provides
    @Singleton
    public CurrentDictionariesHelper providesCurrentDictionariesHelper(CurrentDictionariesRepository currentDictionariesRepository) {
        return new CurrentDictionariesHelperImpl(currentDictionariesRepository);
    }

    @Provides
    @Singleton
    public CurrentDictionariesRepository providesCurrentDictionariesRepository(@ApplicationContext Context context) {
        return new CurrentDictionariesRepositoryImpl(context);
    }

    @Provides
    @Singleton
    public CurrentSortingRepository providesCurrentSortingRepository(@ApplicationContext Context context) {
        return new CurrentSortingRepositoryImpl(context);
    }

    @Provides
    @Singleton
    public CurrentBrowserWordsSourceRepository providesCurrentBrowserWordsSourceRepository(@ApplicationContext Context context) {
        return new CurrentBrowserWordsSourceRepositoryImpl(context);
    }

    @Provides
    @Singleton
    public CurrentUserWordsSourcesRepository providesCurrentUserWordsSourcesRepository(@ApplicationContext Context context) {
        return new CurrentUserWordsSourcesRepositoryImpl(context);
    }

    @Provides
    @Singleton
    public PreferencesHelper providesPreferencesHelper(@ApplicationContext Context context) {
        return new AndroidPreferencesHelper(context);
    }

    @Provides
    @Singleton
    public WordsStatisticHelper providesWordsStatisticHelper(@ApplicationContext Context context) {
        return new WordsSqliteStatisticHelper(context);
    }

    @Provides
    @Singleton
    public WordDataHelper providesWordDataHelper() {
        return new WordDataGTHelperImpl();
    }

    @Provides
    @Singleton
    public NetworkHelper providesNetworkHelper(@ApplicationContext Context context) {
        return new NetworkHelperImpl(context);
    }

    @Provides
    @Singleton
    public UserWordsSqliteSourcesDatabase providesUserWordsSqliteSourcesDatabase(@ApplicationContext Context context) {
        return UserWordsSqliteSourcesDatabase.getInstance(context);
    }
}

package com.caveman.lexiconator.dependency_injection;

import com.caveman.lexiconator.model.CurrentBrowserWordsSourceRepository;
import com.caveman.lexiconator.model.CurrentDictionariesHelper;
import com.caveman.lexiconator.model.CurrentUserWordsSourcesRepository;
import com.caveman.lexiconator.model.DictionariesDirectoryHelper;
import com.caveman.lexiconator.model.DictionariesModel;
import com.caveman.lexiconator.model.DictionariesModelImpl;
import com.caveman.lexiconator.model.PreferencesHelper;
import com.caveman.lexiconator.model.RandomWordsModel;
import com.caveman.lexiconator.model.RandomWordsModelImpl;
import com.caveman.lexiconator.model.StatisticModel;
import com.caveman.lexiconator.model.StatisticModelImpl;
import com.caveman.lexiconator.model.UserWordsSourcesHelper;
import com.caveman.lexiconator.model.UserWordsSourcesModel;
import com.caveman.lexiconator.model.UserWordsSourcesModelImpl;
import com.caveman.lexiconator.model.WordsBrowserModel;
import com.caveman.lexiconator.model.WordsBrowserModelImpl;
import com.caveman.lexiconator.model.WordsStatisticHelper;
import com.caveman.lexiconator.model.WordsTrainingModel;
import com.caveman.lexiconator.model.WordsTrainingModelImpl;
import com.caveman.lexiconator.presenter.DictionariesPresenter;
import com.caveman.lexiconator.presenter.DictionariesPresenterImpl;
import com.caveman.lexiconator.presenter.RandomWordsPresenter;
import com.caveman.lexiconator.presenter.RandomWordsPresenterImpl;
import com.caveman.lexiconator.presenter.StatisticPresenter;
import com.caveman.lexiconator.presenter.StatisticPresenterImpl;
import com.caveman.lexiconator.presenter.UserWordsSourcesPresenter;
import com.caveman.lexiconator.presenter.UserWordsSourcesPresenterImpl;
import com.caveman.lexiconator.presenter.WordsBrowserPresenter;
import com.caveman.lexiconator.presenter.WordsBrowserPresenterImpl;
import com.caveman.lexiconator.presenter.WordsTrainingPresenter;
import com.caveman.lexiconator.presenter.WordsTrainingPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    @Provides
    @PerActivity
    public WordsBrowserModel providesWordBrowserModel(
            UserWordsSourcesHelper userWordsSourcesHelper,
            CurrentDictionariesHelper currentDictionariesHelper,
            CurrentBrowserWordsSourceRepository currentBrowserWordsSourceRepository) {
        return new WordsBrowserModelImpl(userWordsSourcesHelper, currentDictionariesHelper, currentBrowserWordsSourceRepository);
    }

    @Provides
    @PerActivity
    public WordsBrowserPresenter providesWordsBrowserPresenter(
            WordsBrowserModel wordsBrowserModel) {
        return new WordsBrowserPresenterImpl(wordsBrowserModel);
    }

    @Provides
    @PerActivity
    public RandomWordsModel providesRandomWordsModel(
            UserWordsSourcesHelper userWordsSourcesHelper,
            CurrentUserWordsSourcesRepository currentUserWordsSourcesRepository,
            CurrentDictionariesHelper currentDictionariesHelper) {
        return new RandomWordsModelImpl(userWordsSourcesHelper, currentUserWordsSourcesRepository, currentDictionariesHelper);
    }

    @Provides
    @PerActivity
    public RandomWordsPresenter providesRandomWordsPresenter(
            RandomWordsModel randomWordsModel) {
        return new RandomWordsPresenterImpl(randomWordsModel);
    }

    @Provides
    @PerActivity
    public WordsTrainingModel providesWordsTrainingModel(
            UserWordsSourcesHelper userWordsSourcesHelper,
            CurrentUserWordsSourcesRepository currentUserWordsSourcesRepository,
            CurrentDictionariesHelper currentDictionariesHelper,
            PreferencesHelper preferencesHelper) {

        return new WordsTrainingModelImpl(
                userWordsSourcesHelper,
                currentUserWordsSourcesRepository,
                currentDictionariesHelper, preferencesHelper);
    }

    @Provides
    @PerActivity
    public WordsTrainingPresenter providesWordsTrainingPresenter(
            WordsTrainingModel wordsTrainingModel) {
        return new WordsTrainingPresenterImpl(wordsTrainingModel);
    }

    @Provides
    @PerActivity
    public UserWordsSourcesModel providesUserWordsSourcesModel(UserWordsSourcesHelper userWordsSourcesHelper) {
        return new UserWordsSourcesModelImpl(userWordsSourcesHelper);
    }

    @Provides
    @PerActivity
    public UserWordsSourcesPresenter providesUserWordsSourcesPresenter(
            UserWordsSourcesModel userWordsSourcesModel) {
        return new UserWordsSourcesPresenterImpl(userWordsSourcesModel);
    }

    @Provides
    @PerActivity
    public DictionariesModel providesDictionariesModel(
            DictionariesDirectoryHelper dictionariesDirectoryHelper,
            UserWordsSourcesHelper userWordsSourcesHelper,
            CurrentDictionariesHelper currentDictionariesHelper
    ) {
        return new DictionariesModelImpl(dictionariesDirectoryHelper, userWordsSourcesHelper, currentDictionariesHelper);
    }

    @Provides
    @PerActivity
    public DictionariesPresenter providesDictionariesPresenter(
            DictionariesModel dictionariesModel) {
        return new DictionariesPresenterImpl(dictionariesModel);
    }

    @Provides
    @PerActivity
    public StatisticModel providesStatisticModel(
            WordsStatisticHelper wordsStatisticHelper,
            UserWordsSourcesHelper userWordsSourcesHelper,
            CurrentUserWordsSourcesRepository currentUserWordsSourcesRepository
    ) {
        return new StatisticModelImpl(wordsStatisticHelper, userWordsSourcesHelper, currentUserWordsSourcesRepository);
    }

    @Provides
    @PerActivity
    public StatisticPresenter providesStatisticPresenter(
            StatisticModel statisticModel) {
        return new StatisticPresenterImpl(statisticModel);
    }
}

package com.caveman.lexiconator.dependency_injection;

import com.caveman.lexiconator.model.ClipboardModel;
import com.caveman.lexiconator.model.ClipboardModelImpl;
import com.caveman.lexiconator.model.NetworkHelper;
import com.caveman.lexiconator.model.PreferencesHelper;
import com.caveman.lexiconator.model.UserWordsSourcesHelper;
import com.caveman.lexiconator.model.WordDataHelper;
import com.caveman.lexiconator.presenter.ClipboardPresenter;
import com.caveman.lexiconator.presenter.ClipboardPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceModule {

    @Provides
    @PerService
    public ClipboardModel providesClipboardModel(UserWordsSourcesHelper userWordsSourcesHelper, WordDataHelper wordDataHelper) {
        return new ClipboardModelImpl(userWordsSourcesHelper, wordDataHelper);
    }

    @Provides
    @PerService
    public ClipboardPresenter providesClipboardPresenter(PreferencesHelper preferencesHelper, NetworkHelper networkHelper,
            ClipboardModel clipboardModel) {
        return new ClipboardPresenterImpl(preferencesHelper, networkHelper, clipboardModel);
    }
}

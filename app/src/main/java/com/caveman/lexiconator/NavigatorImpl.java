package com.caveman.lexiconator;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.caveman.lexiconator.view.DictionariesActivity;
import com.caveman.lexiconator.view.RandomWordsActivity;
import com.caveman.lexiconator.view.SettingsActivity;
import com.caveman.lexiconator.view.StatisticActivity;
import com.caveman.lexiconator.view.UserWordsSourcesActivity;
import com.caveman.lexiconator.view.WordCardActivity;
import com.caveman.lexiconator.view.WordsBrowserActivity;
import com.caveman.lexiconator.view.WordsTrainingActivity;

import static com.caveman.lexiconator.view.WordCardFragment.ARG_FROM_SPECIFIC_DICTIONARY;
import static com.caveman.lexiconator.view.WordCardFragment.ARG_WORD;
import static com.caveman.lexiconator.view.WordCardFragment.ARG_WORDS_SOURCE_ID;

public class NavigatorImpl implements Navigator {

    private Context context;

    public NavigatorImpl(Context context) {
         this.context = context;
    }

    @Override
    public void openWordsBrowserActivity() {
        startIfNotInstanceOf(WordsBrowserActivity.class);
    }

    @Override
    public void openRandomWordsActivity() {
        startIfNotInstanceOf(RandomWordsActivity.class);
    }

    @Override
    public void openWordsTrainingActivity() {
        startIfNotInstanceOf(WordsTrainingActivity.class);
    }

    @Override
    public void openWordCardActivity(boolean fromSpecificDictionary, long wordsSourceId, String word) {
        Intent intent = new Intent(context, WordCardActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(ARG_FROM_SPECIFIC_DICTIONARY, fromSpecificDictionary);
        bundle.putLong(ARG_WORDS_SOURCE_ID, wordsSourceId);
        bundle.putString(ARG_WORD, word);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    public void openUserWordsSourcesActivity() {
        startIfNotInstanceOf(UserWordsSourcesActivity.class);
    }

    @Override
    public void openDictionariesActivity() {
        startIfNotInstanceOf(DictionariesActivity.class);
    }

    @Override
    public void openStatisticActivity() {
        startIfNotInstanceOf(StatisticActivity.class);
    }

    @Override
    public void openSettingsActivity() {
        startIfNotInstanceOf(SettingsActivity.class);
    }

    private boolean isInstanceOf(Class<? extends AppCompatActivity> activityClass) {
        return activityClass.isInstance(context);
    }

    private void startIfNotInstanceOf(Class<? extends AppCompatActivity> activityClass) {
        if (!isInstanceOf(activityClass)) {
            Intent intent = new Intent(context, activityClass);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context.startActivity(intent);
        }
    }
}

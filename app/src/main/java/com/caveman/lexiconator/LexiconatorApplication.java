package com.caveman.lexiconator;

import android.content.Intent;
import android.support.multidex.MultiDexApplication;
import android.support.v7.preference.PreferenceManager;

import com.caveman.lexiconator.dependency_injection.ActivityComponent;
import com.caveman.lexiconator.dependency_injection.ActivityModule;
import com.caveman.lexiconator.dependency_injection.ApplicationComponent;
import com.caveman.lexiconator.dependency_injection.ApplicationModule;
import com.caveman.lexiconator.dependency_injection.DaggerApplicationComponent;
import com.caveman.lexiconator.dependency_injection.FragmentComponent;
import com.caveman.lexiconator.dependency_injection.FragmentModule;
import com.caveman.lexiconator.dependency_injection.ServiceComponent;
import com.caveman.lexiconator.dependency_injection.ServiceModule;
import com.caveman.lexiconator.model.DictionariesDirectoryHelper;
import com.caveman.lexiconator.model.PreferencesHelper;
import com.caveman.lexiconator.view.ClipboardService;

import javax.inject.Inject;

public class LexiconatorApplication extends MultiDexApplication {

    private static LexiconatorApplication instance;
    private ApplicationComponent applicationComponent;
    /**
     * С таким подходом мало отличий от глобальных синглтонов, где сабкомпоненты висят в памяти до перезапуска JVM
     * и ненужные зависимости не могут быть собраны мусорщиком (даже если требовались всего один раз на пару секунд),
     * хоть и остается возможность, например, расшаривать один экземпляр модели на несколько экземляров
     * одной и той же активити.
     * Зануливание на OnDestroy()тоже не решает проблему. Да, пока один экземпляр активити находится
     * в бэкграунде бэкстака, последующие запущенные экземляры той же активити получат те же зависимости.
     * Но стоит только одному из верхних экземляров этой активити
     * (а с текущей реализацией - вообще любому экземляру любой активити) уничтожиться,
     * как последующие новые экземляры получат уже совсем новый экземпляр компонента(и новый экземпляр модели, например,
     * впрочем, если иметь один экземпляр активити в бэстаке - вряд ли будет проблемой).
     * Было бы неплохо иметь что-то вроде пула, который будет выдавать один и тот же компонент,
     * пока число ссылок на него != 0. И очищать его, когда этим компонентом больше никто не пользуется.
     */
    //Todo сделать что-то вроде пула?
    private ActivityComponent activityComponent;
    private FragmentComponent fragmentComponent;
    private ServiceComponent serviceComponent;
    @Inject PreferencesHelper preferencesHelper;
    @Inject DictionariesDirectoryHelper dictionariesDirectoryHelper;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);

        if (preferencesHelper.clipboardQuickWordsAddIsActive() || preferencesHelper.clipboardTranslationIsActive()) {
            ifCanStartClipboardService();
        }
        else {
            ifCanStopClipboardService();
        }

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        dictionariesDirectoryHelper.createDefaultDictionariesIfFirstRun();
    }

    public static LexiconatorApplication getInstance() {
        return instance;
    }

    public void ifCanStartClipboardService() {
        startService(new Intent(this, ClipboardService.class));
    }

    public void ifCanStopClipboardService() {
        if (!(preferencesHelper.clipboardQuickWordsAddIsActive() || preferencesHelper.clipboardTranslationIsActive())) {
            stopService(new Intent(this, ClipboardService.class));
        }
    }

    public ActivityComponent getActivityComponent() {

        if (activityComponent == null) {
            activityComponent = applicationComponent
                    .activityComponent(new ActivityModule());
        }
        return activityComponent;
    }

    public void clearActivityComponent() {
        activityComponent = null;
    }

    public FragmentComponent getFragmentComponent() {

        if (fragmentComponent == null) {
            fragmentComponent = applicationComponent
                    .fragmentComponent(new FragmentModule());
        }
        return fragmentComponent;

    }

    public void clearFragmentComponent() {
        fragmentComponent = null;
    }

    public ServiceComponent getServiceComponent() {

        if (serviceComponent == null) {
            serviceComponent = applicationComponent
                    .serviceComponent(new ServiceModule());
        }
        return serviceComponent;

    }

    public void clearServiceComponent() {
        serviceComponent = null;
    }
}

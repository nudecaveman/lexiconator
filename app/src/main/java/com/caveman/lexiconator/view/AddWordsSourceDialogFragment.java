package com.caveman.lexiconator.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.EditText;

import com.caveman.lexiconator.R;

public class AddWordsSourceDialogFragment extends DialogFragment {

    interface AddWordsSourceFragmentListener {
        void onAddWordsSource(String wordsSourceName, String wordsSourceBriefCode);
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_add_words_source_title)
                .setView(R.layout.dialog_add_or_edit_words_source)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        EditText editNameText = getDialog().findViewById(R.id.user_words_source_name);
                        EditText editBriefCodeText = getDialog().findViewById(R.id.user_words_source_brief_code);
                        Activity activity = getActivity();
                        if(activity instanceof AddWordsSourceFragmentListener) {
                            ((AddWordsSourceFragmentListener) activity).onAddWordsSource(
                                    editNameText.getText().toString(), editBriefCodeText.getText().toString());
                        }
                    }
                })
                .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        return builder.create();
    }
}
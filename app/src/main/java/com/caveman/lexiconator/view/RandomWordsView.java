package com.caveman.lexiconator.view;

import java.util.List;

public interface RandomWordsView extends MvpChooseWordsSourceView<RandomWordsView>{

    void showRandomWords(long userWordsSourceId, List<String> randomWords);

    void showNoRandomWordsMessageScreen();
}

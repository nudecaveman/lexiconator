package com.caveman.lexiconator.view;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.caveman.lexiconator.R;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.presenter.ChooseUserWordsSourcePresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ChooseUserWordsSourceDialogFragment
        extends BaseFragment<ChooseUserWordsSourceDialogView, ChooseUserWordsSourcePresenter>
        implements ChooseUserWordsSourceDialogView {

    public static final String ARG_WITH_RESET_USER_WORDS_SOURCE = "ARG_WITH_RESET_USER_WORDS_SOURCE";
    public static final String ARG_ITEM_ID = "ARG_ITEM_ID";
    @BindView(R.id.user_words_source_choose_view) ListView userWordsSourceChooseListView;
    private Unbinder unbinder;

    interface ChooseUserWordsSourceListener {

        void onChooseUserWordsSource(long itemId, UserWordsSourceMetadata userWordsSourceMetadata);

        void onRedirectToUserWordsSources();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentComponent().inject(this);
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_choose_user_words_source, null);
        unbinder = ButterKnife.bind(this, view);

        getPresenter().onAttachView(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_choose_user_words_source_title)
                .setView(view);

        return builder.create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getPresenter().onDetachView();
        unbinder.unbind();
    }

    @Override
    public void showUserWordsSources(List<UserWordsSourceMetadata> userWordsSourceMetadataList) {
        List<CharSequence> items = new ArrayList<>();

        /**
         * Sometimes (in the DictionariesActivity, where we need to binds Dictionaries to
         * specific UserWordsSource, and clean that binding)
         * we need to choose nothing from userWordsSourcesList
         */
        boolean withResetUserWordsSource = false;
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(ARG_WITH_RESET_USER_WORDS_SOURCE)) {
            withResetUserWordsSource = bundle.getBoolean(ARG_WITH_RESET_USER_WORDS_SOURCE, false);
        }

        if (userWordsSourceMetadataList.size() > 0) {
            addUserWordsSourceListItems(items, userWordsSourceMetadataList, withResetUserWordsSource);
        }
        else {
            addOnlyCreateNewUserWordSourceRedirectItem(items);
        }
    }

    @Override
    public void notifyListenerChooseUserWordsSource(UserWordsSourceMetadata userWordsSourceMetadata) {
        long itemId = -1;
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(ARG_ITEM_ID)) {
            itemId = bundle.getLong(ARG_ITEM_ID, -1);
        }

        Activity activity = getActivity();
        if (activity instanceof ChooseUserWordsSourceListener) {
            ((ChooseUserWordsSourceListener) activity).onChooseUserWordsSource(itemId, userWordsSourceMetadata);
        }
        else {
            Fragment fragment = getParentFragment();
            if(fragment instanceof ChooseUserWordsSourceListener) {
                ((ChooseUserWordsSourceListener) fragment).onChooseUserWordsSource(itemId, userWordsSourceMetadata);
            }
        }
    }

    @Override
    public void notifyListenerRedirectToUserWordsSources() {
        Activity activity = getActivity();
        if (activity instanceof ChooseUserWordsSourceListener) {
            ((ChooseUserWordsSourceListener) activity).onRedirectToUserWordsSources();
        }
        else {
            Fragment fragment = getParentFragment();
            if(fragment instanceof ChooseUserWordsSourceListener) {
                ((ChooseUserWordsSourceListener) fragment).onRedirectToUserWordsSources();
            }
        }
    }

    @Override
    public void close() {
        dismiss();
    }

    private void addUserWordsSourceListItems(
            List<CharSequence> items,
            List<UserWordsSourceMetadata> userWordsSourceMetadataList,
            boolean withResetUserWordsSource
    ) {
        if (withResetUserWordsSource) items.add(0, getString(R.string.nothing));
        for (UserWordsSourceMetadata userWordsSourceMetadata : userWordsSourceMetadataList) {
            items.add(userWordsSourceMetadata.getName());
        }
        ArrayAdapter<CharSequence> userWordsSourceMetadataAdapter = new ArrayAdapter<>(
                getContext(), R.layout.item_list_words_source, R.id.words_source_name, items);
        userWordsSourceChooseListView.setAdapter(userWordsSourceMetadataAdapter);

        userWordsSourceChooseListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserWordsSourceMetadata userWordsSourceMetadata = null;
                if (withResetUserWordsSource) {
                    if (position != 0) {
                        userWordsSourceMetadata = userWordsSourceMetadataList.get(position - 1);
                    }
                }
                else {
                    userWordsSourceMetadata = userWordsSourceMetadataList.get(position);
                }
                getPresenter().onChooseUserWordsSource(userWordsSourceMetadata);
            }
        });
    }

    private void addOnlyCreateNewUserWordSourceRedirectItem(List<CharSequence> items) {
        CharSequence createNewUserWordsSourceClickableString = getClickableString(
                getString(R.string.label_no_user_words_sources_add), new ClickableSpan() {
                    @Override
                    public void onClick(@NonNull View widget) {
                    }
                });
        items.clear();

        items.add(createNewUserWordsSourceClickableString);
        ArrayAdapter<CharSequence> userWordsSourceMetadataAdapter = new ArrayAdapter<>(
                getContext(), R.layout.item_list_words_source, R.id.words_source_name, items);
        userWordsSourceChooseListView.setAdapter(userWordsSourceMetadataAdapter);
        userWordsSourceChooseListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getPresenter().onNoUserWordsSourcesClick();
            }
        });
    }

    public static ChooseUserWordsSourceDialogFragment createNewForDictionariesBinding(boolean withResetUserWordsSource, long itemId) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ARG_WITH_RESET_USER_WORDS_SOURCE, withResetUserWordsSource);
        bundle.putLong(ARG_ITEM_ID, itemId);
        ChooseUserWordsSourceDialogFragment chooseUserWordsSourceDialogFragment = new ChooseUserWordsSourceDialogFragment();
        chooseUserWordsSourceDialogFragment.setArguments(bundle);
        return chooseUserWordsSourceDialogFragment;
    }
}

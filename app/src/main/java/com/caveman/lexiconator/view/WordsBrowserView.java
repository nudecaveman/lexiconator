package com.caveman.lexiconator.view;

import com.caveman.lexiconator.model.DictionarySourceMetadata;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

public interface WordsBrowserView extends MvpChooseWordsSourceView<WordsBrowserView> {

    void openUserWordsSourceContent(UserWordsSourceMetadata userWordsSourceMetadata);

    void openDictionarySourceContent(DictionarySourceMetadata dictionarySourceMetadata);

    void showNoWordsSourcesMessageScreen();
}

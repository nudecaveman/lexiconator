package com.caveman.lexiconator.view;

public interface MvpChooseWordsSourceView<V extends MvpChooseWordsSourceView> extends MvpNavigatorView<V> {

    void showChooseWordsSourceDialog();

    void updateWordSourceLabel(String wordSourceLabelText);

    void showNoUserWordsSourcesMessageScreen();

    void showNoDictionarySourcesMessageScreen();
}

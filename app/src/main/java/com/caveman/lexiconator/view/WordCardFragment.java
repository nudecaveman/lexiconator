package com.caveman.lexiconator.view;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.caveman.lexiconator.R;
import com.caveman.lexiconator.model.WordData;
import com.caveman.lexiconator.presenter.WordCardPresenter;

import org.apache.commons.text.WordUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class WordCardFragment extends BaseFragment<WordCardView, WordCardPresenter> implements WordCardView {

    public static final String ARG_FROM_SPECIFIC_DICTIONARY = "ARG_FROM_SPECIFIC_DICTIONARY";
    public static final String ARG_WORDS_SOURCE_ID = "ARG_WORDS_SOURCE_ID";
    public static final String ARG_WORD = "ARG_WORD";

    @BindView(R.id.card_view) View cardContentView;
    @BindView(R.id.word_image) ImageView wordImage;
    @BindView(R.id.word_title) TextView word_title;
    @BindView(R.id.word_pronunciation) TextView word_pronunciation;
    @BindView(R.id.word_definition) TextView word_definition;
    @BindView(R.id.word_example) TextView word_example;
    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.content_card, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getPresenter().onAttachView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().onDetachView();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void updateView(List<WordData> wordDataList, boolean isHtml) {
        if (wordDataList.size() != 0) {
            WordData wordData = wordDataList.get(0);
            CharSequence meaning = wordData.getMeaning();
            if (isHtml) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    meaning = Html.fromHtml(wordData.getMeaning(), Html.FROM_HTML_MODE_COMPACT);
                } else {
                    meaning = Html.fromHtml(wordData.getMeaning());
                }
            }
            word_title.setText(WordUtils.capitalize(wordData.getWord()));
            word_definition.setText(meaning, TextView.BufferType.SPANNABLE);
        }
    }

    @Override
    public boolean getFromSpecificDictionaryArg() {
        boolean fromSpecificDictionaryArg = false;
        Bundle bundle = getArguments();
        if(bundle != null) {
            fromSpecificDictionaryArg = bundle.getBoolean(ARG_FROM_SPECIFIC_DICTIONARY);
        }
        return fromSpecificDictionaryArg;
    }

    @Override
    public long getWordSourceIdArg() {
        long dictionarySourceIdArg = -1;
        Bundle bundle = getArguments();
        if(bundle != null) {
            dictionarySourceIdArg = bundle.getLong(ARG_WORDS_SOURCE_ID);
        }
        return dictionarySourceIdArg;
    }

    @Override
    public String getWordArg() {
        String wordArg = null;
        Bundle bundle = getArguments();
        if(bundle != null) {
            wordArg = bundle.getString(ARG_WORD);
        }
        return wordArg;
    }

    public static WordCardFragment newInstanceForSpecificDictionary(long dictionarySourceId, String word) {
        return newInstanceForSpecificDictionary(true, dictionarySourceId, word);
    }

    public static WordCardFragment newInstanceForUserWordsSource(long userWordsSourceId, String word) {
        return newInstanceForSpecificDictionary(false, userWordsSourceId, word);
    }

    private static WordCardFragment newInstanceForSpecificDictionary(boolean fromSpecificDictionary,
            long wordsSourceId, String word) {
        WordCardFragment fragment = new WordCardFragment();

        Bundle args = new Bundle();
        args.putBoolean(ARG_FROM_SPECIFIC_DICTIONARY, fromSpecificDictionary);
        args.putLong(ARG_WORDS_SOURCE_ID, wordsSourceId);
        args.putString(ARG_WORD, word);
        fragment.setArguments(args);

        return fragment;
    }
}
package com.caveman.lexiconator.view;

import com.caveman.lexiconator.Navigator;

public interface WordsSourceContentView extends MvpView {

    long getWordSourceId();

    void showWordsCounter(int count);

    void showChooseSortingDialog();

    Navigator getNavigator();

    void showLoadingScreen();

    void hideLoadingScreen();

    void showNoWordsMessageScreen();

    void hideMessageScreen();

    void clearWords();
}

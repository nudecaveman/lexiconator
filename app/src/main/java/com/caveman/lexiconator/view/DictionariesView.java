package com.caveman.lexiconator.view;

import com.caveman.lexiconator.DTO.DictionariesDTO;
import com.caveman.lexiconator.model.DictionarySourceMetadata;

public interface DictionariesView extends MvpNavigatorView<DictionariesView> {

   void showDictionariesAndUserWordsBriefCodes(DictionariesDTO dictionariesDTO);

   void showChooseUserWordsSourceDialog(long dictionarySourceId);

   void updateDictionariesData();

   void showNoUserWordsSourceSpecifiedNotification();

   void showDictionaryDeleteConfirmation(DictionarySourceMetadata dictionarySourceMetadata, String dictionaryName, long filesSize);

   void showDictionaryIsDeleteNotification(String dictionaryName, long filesSize);

   void showNoDictionarySourcesMessageScreen();
}

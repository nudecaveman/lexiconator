package com.caveman.lexiconator.view;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v14.preference.SwitchPreference;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceScreen;
import android.text.InputFilter;
import android.widget.Toast;

import com.caveman.lexiconator.InputIntFilter;
import com.caveman.lexiconator.LexiconatorApplication;
import com.caveman.lexiconator.Navigator;
import com.caveman.lexiconator.NavigatorImpl;
import com.caveman.lexiconator.R;
import com.caveman.lexiconator.model.UserWordsSourcesHelper;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.takisoft.fix.support.v7.preference.EditTextPreference;

import net.rdrei.android.dirchooser.DirectoryChooserConfig;
import net.rdrei.android.dirchooser.DirectoryChooserFragment;

import java.io.File;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

//С android.support.v7.preference.EditTextPreference нельзя вызвать .getEditText();

public class SettingsActivity extends AppCompatActivity
        implements PreferenceFragmentCompat.OnPreferenceStartScreenCallback,
        DirectoryChooserFragment.OnFragmentInteractionListener {

    public static final String TAG_SETTINGS_FRAGMENT = "TAG_SETTINGS_FRAGMENT";
    private DirectoryChooserFragment directoryChooserFragmentDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new MainSettingsFragment(), TAG_SETTINGS_FRAGMENT)
                .commit();

        final DirectoryChooserConfig config = DirectoryChooserConfig.builder()
                .newDirectoryName(getString(R.string.app_name))
                .allowNewDirectoryNameModification(true)
                .allowReadOnlyDirectory(false)
                .build();

        directoryChooserFragmentDialog = DirectoryChooserFragment.newInstance(config);
    }

    @Override
    public boolean onPreferenceStartScreen(PreferenceFragmentCompat preferenceFragmentCompat, PreferenceScreen preferenceScreen) {
        CardDisplaySettingsFragment fragment = new CardDisplaySettingsFragment();
        Bundle args = new Bundle();
        args.putString(PreferenceFragmentCompat.ARG_PREFERENCE_ROOT, preferenceScreen.getKey());
        fragment.setArguments(args);

        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, fragment, TAG_SETTINGS_FRAGMENT)
                .addToBackStack(null)
                .commit();
        return true;
    }

    @Override
    public void onSelectDirectory(@NonNull String path) {
        Fragment fragment = getSupportFragmentManager().
                findFragmentByTag(TAG_SETTINGS_FRAGMENT);
        if (fragment instanceof MainSettingsFragment) {
            ((MainSettingsFragment) fragment).onSelectDirectory(path);
        }
        directoryChooserFragmentDialog.dismiss();
    }

    @Override
    public void onCancelChooser() {
        directoryChooserFragmentDialog.dismiss();
    }

    //Todo Вызывать диалоги и сабфрагменты без участия активити

    /**
     * Кошмар, но не могу найти способ показывать в PreferenceFragment и support фрагменты, и обычные
     * Если фрагмент настроек support - из него не получить обычный фрагмент менеджер для того же
     * диалога выбора директории. А если не саппорт - из него не получить support фрагмент менеджер
     * для своих собственных support диалогов. Как targetFragment для диалога выбора директории
     * этот support PreferenceFragment тоже не установить. А если сделать PreferenceFragment обычным -
     * его не сделать как target для своих саппортных диалогов. Возможно стоит форкнуть библиотеку
     * выбора директории и переделать под support. Но пока нет времени, поэтому костыль
     */
    public void showChooseDictionariesDirectoryDialog() {
        directoryChooserFragmentDialog.show(getFragmentManager(), "ChooseDictionariesDirectoryDialog");
    }

    public static class MainSettingsFragment extends PreferenceFragmentCompat
            implements SharedPreferences.OnSharedPreferenceChangeListener,
            ChooseUserWordsSourceDialogFragment.ChooseUserWordsSourceListener,
            DirectoryChooserFragment.OnFragmentInteractionListener {

        public static final int ACTION_CHOOSE_DICTIONARIES_CUSTOM_DIRECTORY_ASK_PERMISSIONS = 2000;
        public static final int ACTION_CHOOSE_DICTIONARIES_CUSTOM_DIRECTORY = 3000;
        public static final int ACTION_OVERLAY_PERMISSION_ADD_WORDS_REQ_CODE = 4000;
        public static final int ACTION_OVERLAY_PERMISSION_TRANSLATE_REQ_CODE = 5000;
        private Preference dictionariesCustomDirectoryPreference;
        private EditTextPreference wordDelayPreference;
        private SwitchPreference switchQuickWordsAddPreference;
        private Preference userWordsSourceForWordsAddPreference;
        private SwitchPreference switchTranslatePreference;
        private ListPreference clipboardTranslationLanguageSourcePreference;
        private ListPreference clipboardTranslationLanguageTargetPreference;
        private Navigator navigator;
        @Inject
        UserWordsSourcesHelper userWordsSourcesHelper;

        @Override
        public void onCreatePreferences(Bundle bundle, String rootKey) {
            setPreferencesFromResource(R.xml.preferences, rootKey);

            dictionariesCustomDirectoryPreference = this.findPreference(getString(R.string.KEY_PREF_DICTIONARIES_CUSTOM_DIRECTORY));
            dictionariesCustomDirectoryPreference.setOnPreferenceClickListener(preference -> {
                showDirectoryChooser();
                return false;
            });

            wordDelayPreference = (EditTextPreference) this.findPreference(getString(R.string.KEY_PREF_WORDS_TRAINING_DELAY));
            wordDelayPreference.getEditText().setFilters(new InputFilter[]{new InputIntFilter(1, 1000)});

            switchQuickWordsAddPreference = (SwitchPreference) getPreferenceScreen().findPreference(
                    getString(R.string.KEY_PREF_CLIPBOARD_SERVICE_QUICK_WORDS_ADD_IS_ACTIVE));

            userWordsSourceForWordsAddPreference = getPreferenceScreen().findPreference(
                    getString(R.string.KEY_PREF_CLIPBOARD_SERVICE_QUICK_WORDS_ADD_USER_WORDS_SOURCE_ID));
            userWordsSourceForWordsAddPreference.setOnPreferenceClickListener(preference -> {
                showUserWordsSourceChooser();
                return false;
            });

            switchTranslatePreference = (SwitchPreference) getPreferenceScreen().findPreference(
                    getString(R.string.KEY_PREF_CLIPBOARD_SERVICE_TRANSLATE_IS_ACTIVE));

            clipboardTranslationLanguageSourcePreference = (ListPreference) getPreferenceScreen().findPreference(
                    getString(R.string.KEY_PREF_CLIPBOARD_TRANSLATION_LANGUAGE_SOURCE));

            clipboardTranslationLanguageTargetPreference = (ListPreference) getPreferenceScreen().findPreference(
                    getString(R.string.KEY_PREF_CLIPBOARD_TRANSLATION_LANGUAGE_TARGET));

            navigator = new NavigatorImpl(getContext());

            LexiconatorApplication.getInstance().getFragmentComponent().inject(this);
        }

        @Override
        public void onResume() {
            super.onResume();
            checkIfDictionariesCustomDirectoryExists();
            updateDictionariesCustomDirectorySummary();
            updateWordDelaySummary();
            updateWordsAddUserWordsSourceSummary();
            updateClipboardTranslationLanguageSourceSummary();
            updateClipboardTranslationLanguageTargetSummary();
            getPreferenceScreen().getSharedPreferences()
                    .registerOnSharedPreferenceChangeListener(this);

        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceScreen().getSharedPreferences()
                    .unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            LexiconatorApplication.getInstance().clearFragmentComponent();
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            String wordDelayKey = getString(R.string.KEY_PREF_WORDS_TRAINING_DELAY);
            //Todo переделать, чтобы был один код и на листенер, и на инициализацию службы при старте
            //Todo выключение службы в уведомлении должно тображаться на текущем экарне настроек

            if (key.equals(switchQuickWordsAddPreference.getKey())) {
                boolean clipboardWordsAddIsActive = sharedPreferences.getBoolean(switchQuickWordsAddPreference.getKey(), false);
                if (clipboardWordsAddIsActive) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                        (!Settings.canDrawOverlays(getActivity().getApplicationContext()))) {
                        //Что, если разрешение будет отменено, когда не будут открыты настройки? Проверять и сбрасывать ключ?
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                                   Uri.parse("package:" + getActivity().getPackageName()));
                        startActivityForResult(intent, ACTION_OVERLAY_PERMISSION_ADD_WORDS_REQ_CODE);
                    } else {
                        startClipboardService();
                    }
                } else {
                    ifCanStopClipboardService();
                }
            }
            if (key.equals(switchTranslatePreference.getKey())) {
                Boolean clipboardTranslateIsActive = sharedPreferences.getBoolean(switchTranslatePreference.getKey(), false);
                if (clipboardTranslateIsActive) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                        (!Settings.canDrawOverlays(getActivity().getApplicationContext()))) {
                        //Что, если разрешение будет отменено, когда не будут открыты настройки? Проверять и сбрасывать ключ?
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                                   Uri.parse("package:" + getActivity().getPackageName()));
                        startActivityForResult(intent, ACTION_OVERLAY_PERMISSION_TRANSLATE_REQ_CODE);
                    } else {
                        startClipboardService();
                    }
                } else {
                    ifCanStopClipboardService();
                }
            } else if (key.equals(wordDelayKey)) {
                updateWordDelaySummary();
            } else if (key.equals((clipboardTranslationLanguageSourcePreference.getKey()))) {
                updateClipboardTranslationLanguageSourceSummary();
            } else if (key.equals((clipboardTranslationLanguageTargetPreference.getKey()))) {
                updateClipboardTranslationLanguageTargetSummary();
            }
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
            switch (requestCode) {
                case ACTION_CHOOSE_DICTIONARIES_CUSTOM_DIRECTORY_ASK_PERMISSIONS: {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        startDirectoryChooser();
                    } else {
                        //Denied
                    }
                    return;
                }
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            switch (requestCode) {
                case ACTION_OVERLAY_PERMISSION_ADD_WORDS_REQ_CODE:
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (Settings.canDrawOverlays(getActivity().getApplicationContext())) {
                            startClipboardService();
                        } else {
                            switchQuickWordsAddPreference.setChecked(false);
                        }
                    }
                    break;
                case ACTION_OVERLAY_PERMISSION_TRANSLATE_REQ_CODE:
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (Settings.canDrawOverlays(getActivity().getApplicationContext())) {
                            startClipboardService();
                        } else {
                            switchTranslatePreference.setChecked(false);
                        }
                    }
                    break;
            }
        }

        private void showUserWordsSourceChooser() {
            FragmentManager fragmentManager = getChildFragmentManager();
            ChooseUserWordsSourceDialogFragment chooseUserWordsSourceDialogFragment = new ChooseUserWordsSourceDialogFragment();
            chooseUserWordsSourceDialogFragment.show(fragmentManager, "ChooseUserWordsSourceDialog");
        }

        @Override
        public void onChooseUserWordsSource(long itemId, UserWordsSourceMetadata userWordsSourceMetadata) {
            saveWordsAddUserWordsSourceId(userWordsSourceMetadata.getId());
        }

        @Override
        public void onRedirectToUserWordsSources() {
            navigator.openUserWordsSourcesActivity();
        }

        private void showDirectoryChooser() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.System.canWrite(getActivity().getApplication())) {
                    requestPermissions(
                            new String[]{
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE
                            },
                            ACTION_CHOOSE_DICTIONARIES_CUSTOM_DIRECTORY_ASK_PERMISSIONS);
                } else {
                    startDirectoryChooser();
                }
            } else {
                startDirectoryChooser();
            }
        }

        private void startDirectoryChooser() {
            Activity activity = getActivity();
            if (activity instanceof SettingsActivity) {
                ((SettingsActivity) activity).showChooseDictionariesDirectoryDialog();
            }
        }

        @Override
        public void onSelectDirectory(@NonNull String path) {
            saveDictionariesCustomDirectoryPath(path);
        }

        @Override
        public void onCancelChooser() {
        }

        private void startClipboardService() {
            long userWordsSourceId = getPreferenceManager().getSharedPreferences()
                    .getLong(getString(R.string.KEY_PREF_CLIPBOARD_SERVICE_QUICK_WORDS_ADD_USER_WORDS_SOURCE_ID), -1);

            subscribeForUserWordsSourceMetadata(
                    userWordsSourceId,
                    (userWordsSourceMetadata) -> {
                    },
                    () -> {
                        Toast toast = Toast.makeText(
                                getContext(),
                                getString(R.string.label_enable_quick_add_service_not_specify_user_words_source),
                                Toast.LENGTH_LONG);
                        toast.show();
                    }
            );
            LexiconatorApplication.getInstance().ifCanStartClipboardService();
        }

        private void ifCanStopClipboardService() {
            LexiconatorApplication.getInstance().ifCanStopClipboardService();
        }

        private void updateDictionariesCustomDirectorySummary() {
            String customDirectoriesDirectoryPath = getPreferenceManager().getSharedPreferences()
                    .getString(getString(R.string.KEY_PREF_DICTIONARIES_CUSTOM_DIRECTORY), null);
            customDirectoriesDirectoryPath = customDirectoriesDirectoryPath != null ? customDirectoriesDirectoryPath : "...";
            dictionariesCustomDirectoryPreference.setSummary(
                    getString(R.string.pref_dictionaries_custom_directory_summ) + ":\n" +
                    customDirectoriesDirectoryPath);
        }

        private void updateWordDelaySummary() {
            wordDelayPreference.setSummary(getString(R.string.pref_words_training_delay_summ) + ": " + wordDelayPreference.getText());
        }

        private void updateWordsAddUserWordsSourceSummary() {
            long userWordsSourceId = getPreferenceManager().getSharedPreferences()
                    .getLong(getString(R.string.KEY_PREF_CLIPBOARD_SERVICE_QUICK_WORDS_ADD_USER_WORDS_SOURCE_ID), -1);

            String summaryString = getString(R.string.pref_clipboard_service_add_user_words_source_summ) + ": ";

            subscribeForUserWordsSourceMetadata(
                    userWordsSourceId,
                    (userWordsSourceMetadata) -> {
                        userWordsSourceForWordsAddPreference.setSummary(summaryString + userWordsSourceMetadata.getName());
                    },
                    () -> userWordsSourceForWordsAddPreference.setSummary(summaryString + "...")
            );
        }

        private void updateClipboardTranslationLanguageSourceSummary() {
            clipboardTranslationLanguageSourcePreference.setSummary(clipboardTranslationLanguageSourcePreference.getValue());
        }

        private void updateClipboardTranslationLanguageTargetSummary() {
            clipboardTranslationLanguageTargetPreference.setSummary(clipboardTranslationLanguageTargetPreference.getValue());
        }

        private void saveDictionariesCustomDirectoryPath(String path) {
            SharedPreferences.Editor editor = getPreferenceManager().getSharedPreferences().edit();
            editor.putString(
                    getString(R.string.KEY_PREF_DICTIONARIES_CUSTOM_DIRECTORY),
                    path);
            editor.commit();
            updateDictionariesCustomDirectorySummary();
        }

        private void saveWordsAddUserWordsSourceId(long userWordsSourceId) {
            SharedPreferences.Editor editor = getPreferenceManager().getSharedPreferences().edit();
            editor.putLong(
                    getString(R.string.KEY_PREF_CLIPBOARD_SERVICE_QUICK_WORDS_ADD_USER_WORDS_SOURCE_ID),
                    userWordsSourceId);
            editor.commit();
            updateWordsAddUserWordsSourceSummary();
        }

        private void checkIfDictionariesCustomDirectoryExists() {
            String customDirectoriesDirectoryPath = getPreferenceManager().getSharedPreferences()
                    .getString(getString(R.string.KEY_PREF_DICTIONARIES_CUSTOM_DIRECTORY), null);
            if (customDirectoriesDirectoryPath != null) {
                File dictionariesCustomDirectory = new File(customDirectoriesDirectoryPath);
                if (!dictionariesCustomDirectory.exists()) {
                    saveDictionariesCustomDirectoryPath(null);
                }
            }
        }

        private void subscribeForUserWordsSourceMetadata(
                long userWordsSourceId,
                Consumer<UserWordsSourceMetadata> successLambda,
                Runnable completeLambda
        ) {
            Maybe<UserWordsSourceMetadata> userWordsSourceMetadataObservable = Maybe.defer(() -> {
                boolean exist = userWordsSourcesHelper.checkIfUserWordsSourceExists(userWordsSourceId);
                return exist ? Maybe.just(userWordsSourcesHelper.getUserWordsSourceMetadataById(userWordsSourceId)) : Maybe.empty();
            });

            userWordsSourceMetadataObservable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new MaybeObserver<UserWordsSourceMetadata>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onSuccess(UserWordsSourceMetadata userWordsSourceMetadata) {
                            try {
                                successLambda.accept(userWordsSourceMetadata);
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            throw new RuntimeException(e);
                        }

                        @Override
                        public void onComplete() {
                            completeLambda.run();
                        }
                    });
        }
    }

    public static class CardDisplaySettingsFragment extends PreferenceFragmentCompat
            implements SharedPreferences.OnSharedPreferenceChangeListener {

        private CheckBoxPreference restrictMeaningsCountPreference;
        private EditTextPreference maxMeaningsCountPreference;
        private CheckBoxPreference meaningsNewLinesPreference;

        @Override
        public void onCreatePreferences(Bundle bundle, String rootKey) {
            setPreferencesFromResource(R.xml.preferences, rootKey);

            restrictMeaningsCountPreference = (CheckBoxPreference) this.findPreference(getString(R.string.KEY_PREF_CARD_DISPLAY_OPTIONS_RESTRICT_MEANINGS_COUNT));
            restrictMeaningsCountPreference.setSingleLineTitle(false);

            maxMeaningsCountPreference = (EditTextPreference) this.findPreference(getString(R.string.KEY_PREF_CARD_DISPLAY_OPTIONS_MEANINGS_MAX_COUNT));
            //Todo такое чувство, что фильтр в этом кастомном едиттекстреференсе не работает
            maxMeaningsCountPreference.getEditText().setFilters(new InputFilter[]{new InputIntFilter(1, 1000)});

            meaningsNewLinesPreference = (CheckBoxPreference) this.findPreference(getString(R.string.KEY_PREF_CARD_DISPLAY_OPTIONS_MEANINGS_ON_NEW_LINES));
            meaningsNewLinesPreference.setSingleLineTitle(false);
        }

        @Override
        public void onResume() {
            super.onResume();
            updateWMaxMeaningCountPreferenceSummary();
            getPreferenceScreen().getSharedPreferences()
                    .registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceScreen().getSharedPreferences()
                    .unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key.equals(getString(R.string.KEY_PREF_CARD_DISPLAY_OPTIONS_MEANINGS_MAX_COUNT))) {
                updateWMaxMeaningCountPreferenceSummary();
            }

        }

        private void updateWMaxMeaningCountPreferenceSummary() {
            maxMeaningsCountPreference.setSummary(
                    String.format(getString(R.string.pref_card_display_options_max_meanings_count_summ),
                                  Integer.valueOf(maxMeaningsCountPreference.getText())
                    )
            );
        }
    }
}

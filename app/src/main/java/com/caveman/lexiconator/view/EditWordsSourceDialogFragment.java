package com.caveman.lexiconator.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.caveman.lexiconator.R;

public class EditWordsSourceDialogFragment extends DialogFragment {

    public static final String ARG_WORDS_SOURCE_ID = "ARG_WORDS_SOURCE_ID";
    public static final String ARG_WORDS_SOURCE_NAME = "ARG_WORDS_SOURCE_NAME";
    public static final String ARG_WORDS_SOURCE_BRIEF_CODE = "ARG_WORDS_SOURCE_BRIEF_CODE";

    interface EditWordsSourceFragmentListener {
        void onEditWordsSource(long userWordsSourceId, String wordsSourceName, String wordsSourceBriefCode);
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        long userWordsSourceIdArg = -1;
        String userWordsSourceNameArg = "";
        String userWordsSourceBriefCodeArg = "";
        Bundle bundle = getArguments();
        if(bundle != null) {
            userWordsSourceIdArg = bundle.getLong(ARG_WORDS_SOURCE_ID, -1);
            userWordsSourceNameArg = bundle.getString(ARG_WORDS_SOURCE_NAME, "");
            userWordsSourceBriefCodeArg = bundle.getString(ARG_WORDS_SOURCE_BRIEF_CODE, "");
        }

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_add_or_edit_words_source, null);
        EditText editNameText = view.findViewById(R.id.user_words_source_name);
        editNameText.setText(userWordsSourceNameArg);
        EditText editBriefCodeText = view.findViewById(R.id.user_words_source_brief_code);
        editBriefCodeText.setText(userWordsSourceBriefCodeArg);


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        long finalUserWordsSourceIdArg = userWordsSourceIdArg;
        builder.setMessage(R.string.dialog_edit_words_source_title)
                .setView(view)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Activity activity = getActivity();
                        if(activity instanceof EditWordsSourceFragmentListener) {
                            ((EditWordsSourceFragmentListener) activity).onEditWordsSource(
                                    finalUserWordsSourceIdArg,
                                    editNameText.getText().toString(), editBriefCodeText.getText().toString());
                        }
                    }
                })
                .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        return builder.create();
    }

    public static EditWordsSourceDialogFragment newInstance(long wordsSourceId,
            String wordsSourceName, String wordsSourceBriefCode) {

        EditWordsSourceDialogFragment editWordsSourceDialogFragment = new EditWordsSourceDialogFragment();

        Bundle args = new Bundle();
        args.putLong(ARG_WORDS_SOURCE_ID, wordsSourceId);
        args.putString(ARG_WORDS_SOURCE_NAME, wordsSourceName);
        args.putString(ARG_WORDS_SOURCE_BRIEF_CODE, wordsSourceBriefCode);
        editWordsSourceDialogFragment.setArguments(args);

        return editWordsSourceDialogFragment;
    }
}

package com.caveman.lexiconator.view;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ClickableSpan;

import com.caveman.lexiconator.LexiconatorApplication;
import com.caveman.lexiconator.dependency_injection.FragmentComponent;
import com.caveman.lexiconator.presenter.MvpPresenter;

import javax.inject.Inject;

public class BaseFragment<V extends MvpView, P extends MvpPresenter<V>> extends DialogFragment implements MvpView {

    private P presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected P getPresenter() {
        return presenter;
    }

    @Inject
    protected void setPresenter(P presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LexiconatorApplication.getInstance().clearFragmentComponent();
    }

    protected FragmentComponent getFragmentComponent() {
        return LexiconatorApplication.getInstance().getFragmentComponent();
    }

    protected CharSequence getClickableString(String text, ClickableSpan linkClickableSpan) {
        String linkStartTag = "<link>";
        String linkEndTag = "</link>";
        if (text.contains(linkStartTag) && text.contains(linkEndTag)) {
            String clickableString = text.substring(
                    text.indexOf(linkStartTag) + linkStartTag.length(),
                    text.indexOf(linkEndTag)
            ).trim();

            text = text.replace(linkStartTag, "").replace(linkEndTag, "");
            SpannableString textResult = new SpannableString(text);

            textResult.setSpan(
                    linkClickableSpan,
                    text.indexOf(clickableString), text.indexOf(clickableString) + clickableString.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            return textResult;
        }
        else {
            return text;
        }
    }
}

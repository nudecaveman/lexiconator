package com.caveman.lexiconator.view;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.caveman.lexiconator.R;
import com.caveman.lexiconator.presenter.StatisticPresenter;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

import static com.caveman.lexiconator.view.DatePickerDialogFragment.ARG_INITIAL_DATE;
import static com.caveman.lexiconator.view.DatePickerDialogFragment.ARG_MAX_DATE;
import static com.caveman.lexiconator.view.DatePickerDialogFragment.ARG_MIN_DATE;
import static com.caveman.lexiconator.view.DatePickerDialogFragment.ARG_VIEW_ID;
import static com.caveman.lexiconator.view.DatePickerDialogFragment.TAG_DATE_PICKER_DIALOG;

public class StatisticActivity extends BaseChooseUserWordsSourceActivity<StatisticView, StatisticPresenter>
        implements DatePickerDialogFragment.OnDateSelectedListener, StatisticView {

    @BindView(R.id.screen_statistic) View statisticScreen;
    @BindView(R.id.buttonStartDate) Button startDateButton;
    @BindView(R.id.buttonEndDate) Button endDateButton;
    @BindView(R.id.statistic_graph) LineChart statisticGraph;
    @BindView(R.id.statistic_count_view) TextView addedWordsCount;
    private SparseArray<Date> dateSparseArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.activity_title_statistic);

        XAxis xAxis = statisticGraph.getXAxis();
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime((dateSparseArray.get((int) value)));
                return String.valueOf(calendar.get(Calendar.DATE));
            }
        });
        xAxis.setGranularity(1f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis lYAxis = statisticGraph.getAxisLeft();
        lYAxis.setGranularity(1f);
        lYAxis.setAxisMinimum(0f);

        YAxis rYAxis = statisticGraph.getAxisRight();
        rYAxis.setEnabled(false);

        statisticGraph.setDrawBorders(true);
        statisticGraph.setNoDataText(getString(R.string.statistic_graph_is_empty));
        statisticGraph.getLegend().setEnabled(false);
        statisticGraph.setHighlightPerDragEnabled(false);
        statisticGraph.setHighlightPerTapEnabled(false);

        Description description = new Description();
        description.setText("");
        statisticGraph.setDescription(description);

        dateSparseArray = new SparseArray<>();

        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_statistic;
    }

    @Override
    protected StatisticView getThisMvpView() {
        return this;
    }

    @Override
    public void onDateSelected(int dateViewId, int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        switch (dateViewId) {
            case (R.id.buttonStartDate):
                getPresenter().onStartDatePick(year, month, day);
                break;
            case (R.id.buttonEndDate):
                getPresenter().onEndDatePick(year, month, day);
                break;
        }
    }

    @OnClick({R.id.buttonStartDate, R.id.buttonEndDate})
    public void onChooseDateButtonClick(Button button) {
        getPresenter().onChooseDateButtonClick(button.getId());
    }

    @Override
    public void showDatePicker(int buttonId, Date startDate, Date endDate) {
        DatePickerDialogFragment dateFragment = new DatePickerDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_VIEW_ID, buttonId);

        Calendar calendar = Calendar.getInstance();

        switch (buttonId) {

            case (R.id.buttonStartDate):
                calendar.setTime(startDate);
                bundle.putLong(ARG_INITIAL_DATE, calendar.getTimeInMillis());

                calendar.setTime(endDate);
                calendar.add(Calendar.DAY_OF_MONTH, - 1);
                bundle.putLong(ARG_MAX_DATE, calendar.getTimeInMillis());
                break;

            case (R.id.buttonEndDate):
                calendar.setTime(endDate);
                bundle.putLong(ARG_INITIAL_DATE, calendar.getTimeInMillis());

                calendar.setTime(startDate);
                calendar.add(Calendar.DAY_OF_MONTH, + 1);
                bundle.putLong(ARG_MIN_DATE, calendar.getTimeInMillis());
                break;

            default:
                return;
        }

        dateFragment.setArguments(bundle);
        dateFragment.show(getFragmentManager(), TAG_DATE_PICKER_DIALOG);
    }

    @Override
    public void buildGraph(Map<Date, Integer> addedWordsInDayStatistic) {
        statisticGraph.getXAxis().mEntries = new float[0];
        statisticGraph.getAxisLeft().mEntries = new float[0];
        statisticGraph.getAxisRight().mEntries = new float[0];
        dateSparseArray.clear();

        List<Entry> entryList = new ArrayList<>();

        int key = 0;
        for (Map.Entry<Date, Integer> statisticEntry : addedWordsInDayStatistic.entrySet()) {
            Date date = statisticEntry.getKey();
            int count = statisticEntry.getValue();

            entryList.add(new Entry(key, count));
            dateSparseArray.append(key, date);
            key++;
        }

        LineDataSet dataSet = new LineDataSet(entryList, "");
        dataSet.setDrawValues(false);
        dataSet.setDrawCircles(false);

        LineData lineData = new LineData(dataSet);
        statisticGraph.setData(lineData);
        statisticGraph.invalidate();
    }

    @Override
    public void showWordsCountStatistic(int wordsCount) {
        addedWordsCount.setText(String.valueOf(wordsCount));
    }

    @Override
    public void updateDateButtons(Date startDate, Date endDate) {
        Calendar calendar = Calendar.getInstance();
        DateFormat formatter = SimpleDateFormat.getDateInstance();

        calendar.setTime(startDate);
        startDateButton.setText(formatter.format(calendar.getTime()));

        calendar.setTime(endDate);
        endDateButton.setText(formatter.format(calendar.getTime()));
    }

    @Override
    public void showNoUserWordsSourcesMessageScreen() {
        statisticScreen.setVisibility(View.INVISIBLE);
        super.showNoUserWordsSourcesMessageScreen();
    }
}
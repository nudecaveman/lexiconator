package com.caveman.lexiconator.view;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.caveman.lexiconator.R;

import static com.caveman.lexiconator.view.WordCardFragment.ARG_FROM_SPECIFIC_DICTIONARY;
import static com.caveman.lexiconator.view.WordCardFragment.ARG_WORD;
import static com.caveman.lexiconator.view.WordCardFragment.ARG_WORDS_SOURCE_ID;

public class WordCardActivity extends BaseActivity implements MvpNavigatorView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.activity_title_word_card);

        Bundle bundle = getIntent().getExtras();

        if(bundle != null) {
            boolean fromSpecificDictionary = bundle.getBoolean(ARG_FROM_SPECIFIC_DICTIONARY, false);
            long wordsSourceId = bundle.getLong(ARG_WORDS_SOURCE_ID, -1);
            String word = bundle.getString(ARG_WORD, null);

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);


            WordCardFragment wordCardFragment;
            if (fromSpecificDictionary) {
                wordCardFragment = WordCardFragment.newInstanceForSpecificDictionary(wordsSourceId, word);
            }
            else {
                wordCardFragment = WordCardFragment.newInstanceForUserWordsSource(wordsSourceId, word);
            }

            fragmentTransaction.replace(R.id.word_card_container, wordCardFragment);
            fragmentTransaction.commit();

            setPresenter(null);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_card;
    }

    @Override
    protected MvpNavigatorView getThisMvpView() {
        return this;
    }
}

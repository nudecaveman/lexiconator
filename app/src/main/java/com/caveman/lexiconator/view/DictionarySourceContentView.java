package com.caveman.lexiconator.view;

import com.caveman.lexiconator.model.DictionaryWordMetadata;

import java.util.List;

public interface DictionarySourceContentView extends WordsSourceContentView {

    void showWords(List<DictionaryWordMetadata> wordMetadataList);
}

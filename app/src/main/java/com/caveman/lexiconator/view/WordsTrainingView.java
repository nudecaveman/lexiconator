package com.caveman.lexiconator.view;

import com.caveman.lexiconator.model.room.UserWordMetadata;

import java.util.List;

public interface WordsTrainingView extends MvpChooseWordsSourceView<WordsTrainingView> {

    void showWordsTests(List<UserWordMetadata> wordsMetadataList);

    void addWordsTests(List<UserWordMetadata> wordsMetadataList);

    void onTestPass();

    void showNextButton();

    void showNextTest();

    void showNoUserWordsMessageScreen();

    void resetTests();
}

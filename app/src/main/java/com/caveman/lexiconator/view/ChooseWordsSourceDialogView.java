package com.caveman.lexiconator.view;

import com.caveman.lexiconator.model.WordsSourceMetadata;
import com.caveman.lexiconator.model.WordsSourcesData;

public interface ChooseWordsSourceDialogView extends MvpView {

    void showWordsSources(WordsSourcesData wordsSourcesData);

    void notifyListener(WordsSourceMetadata wordsSourceMetadata);

    void close();
}

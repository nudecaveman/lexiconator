package com.caveman.lexiconator.view;

import com.caveman.lexiconator.model.SortDirection;
import com.caveman.lexiconator.model.SortingOrder;

public interface ChooseSortDirectionView extends MvpView {

    void updateView(SortDirection sortDirection);

    void notifyListener(SortDirection sortDirection);
}

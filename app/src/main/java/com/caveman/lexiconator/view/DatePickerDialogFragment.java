package com.caveman.lexiconator.view;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;

public class DatePickerDialogFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    public static final String TAG_DATE_PICKER_DIALOG = "DatePickerDialog";
    public static final String ARG_VIEW_ID = "ARG_VIEW_ID";
    public static final String ARG_INITIAL_DATE = "ARG_INITIAL_DATE";
    public static final String ARG_MIN_DATE = "ARG_MIN_DATE";
    public static final String ARG_MAX_DATE = "ARG_MAX_DATE";
    private DatePicker datePicker;
    private OnDateSelectedListener onDateSelectedListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
        datePicker = datePickerDialog.getDatePicker();

        Bundle bundle = getArguments();
        if (bundle.containsKey(ARG_MIN_DATE)) {
            datePicker.setMinDate(bundle.getLong(ARG_MIN_DATE));
        }
        if (bundle.containsKey(ARG_MAX_DATE)) {
            datePicker.setMaxDate(bundle.getLong(ARG_MAX_DATE));
        }
        if (bundle.containsKey(ARG_INITIAL_DATE)) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(bundle.getLong(ARG_INITIAL_DATE));
            datePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        }

        return datePickerDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        int dateViewId =  getArguments().getInt(ARG_VIEW_ID);
        onDateSelectedListener.onDateSelected(dateViewId, year, month, day);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onDateSelectedListener = (DatePickerDialogFragment.OnDateSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnDateSelectedListener");
        }
    }

    public interface OnDateSelectedListener {
        void onDateSelected(int dateViewId, int year, int month, int day);
    }
}
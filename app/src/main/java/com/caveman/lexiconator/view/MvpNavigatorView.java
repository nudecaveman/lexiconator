package com.caveman.lexiconator.view;

import com.caveman.lexiconator.Navigator;

public interface MvpNavigatorView<V extends MvpNavigatorView> extends MvpView {

    Navigator getNavigator();

    V showContentScreen();

    V hideContentScreen();

    V showLoadingScreen();

    V hideLoadingScreen();

    V showMessageScreen(CharSequence message);

    V hideMessageScreen();
}

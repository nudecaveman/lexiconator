package com.caveman.lexiconator.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.caveman.lexiconator.R;
import com.caveman.lexiconator.presenter.RandomWordsPresenter;
import com.oguzbabaoglu.cardpager.CardPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class RandomWordsActivity extends BaseChooseUserWordsSourceActivity<RandomWordsView, RandomWordsPresenter>
        implements RandomWordsView {

    @BindView(R.id.random_words_view) CardPager randomWordsView;
    private RandomWordsPagerAdapter randomWordsPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.activity_title_random_words);

        randomWordsView.setVisibility(View.INVISIBLE);

        randomWordsPagerAdapter = new RandomWordsPagerAdapter(getSupportFragmentManager());
        randomWordsView.setAdapter(randomWordsPagerAdapter);
        randomWordsView.setOnCardChangeListener(new CardPager.SimpleOnCardChangeListener() {
            @Override
            public void onCardDismissed(int position, boolean right) {
                if (right) {
                    String word = randomWordsPagerAdapter.getWords().get(position);
                    getPresenter().onWordLike(word);
                }
            }
        });

        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_random_words;
    }

    @Override
    protected RandomWordsActivity getThisMvpView() {
        return this;
    }

    @Override
    public void showRandomWords(long userWordsSourceId, List<String> randomWords) {
        randomWordsPagerAdapter = new RandomWordsPagerAdapter(getSupportFragmentManager());
        randomWordsView.setAdapter(randomWordsPagerAdapter);
        randomWordsPagerAdapter.setWords(userWordsSourceId, randomWords);

        Animation fadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        fadeIn.setDuration(500);
        fadeIn.setFillAfter(true);
        randomWordsView.startAnimation(fadeIn);
    }

    @Override
    public void showNoRandomWordsMessageScreen() {
        showMessageScreen(getString(R.string.label_no_random_words));
    }

    private class RandomWordsPagerAdapter extends FragmentStatePagerAdapter {
        private List<String> randomWords;
        private long userWordsSourceId;

        public RandomWordsPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
            randomWords = new ArrayList<>();
            userWordsSourceId = -1;
        }

        @Override
        public Fragment getItem(int position) {
            String word = randomWords.get(position);
            return WordCardFragment.newInstanceForUserWordsSource(userWordsSourceId, word);
        }

        @Override
        public int getCount() {
            //TODO Последнее слово если - его не лайкнуть
            return randomWords.size();
        }

        public List<String> getWords() {
            return randomWords;
        }

        public void setWords(long userWordsSourceId, List<String> randomWords) {
            this.randomWords = randomWords;
            this.userWordsSourceId = userWordsSourceId;
            notifyDataSetChanged();
        }
    }
}


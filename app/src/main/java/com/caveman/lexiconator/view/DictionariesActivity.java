package com.caveman.lexiconator.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.caveman.lexiconator.DTO.DictionariesDTO;
import com.caveman.lexiconator.R;
import com.caveman.lexiconator.model.DictionarySourceMetadata;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.presenter.DictionariesPresenter;

import butterknife.BindView;

public class DictionariesActivity extends BaseActivity<DictionariesView, DictionariesPresenter>
        implements DictionariesView, DictionaryItemListener, ChooseUserWordsSourceDialogFragment.ChooseUserWordsSourceListener {

    @BindView(R.id.dictionaries_view) RecyclerView dictionariesRecyclerView;
    private DictionariesAdapter dictionariesAdapter;
    private DictionaryActionModeCallback dictionaryActionModeCallback;
    private ActionMode dictionaryActionMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.activity_title_dictionaries);

        dictionariesRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        dictionariesRecyclerView.setLayoutManager(layoutManager);
        registerForContextMenu(dictionariesRecyclerView);

        dictionariesAdapter = new DictionariesAdapter(this);
        dictionariesRecyclerView.setAdapter(dictionariesAdapter);

        dictionaryActionModeCallback = new DictionaryActionModeCallback();

        getActivityComponent().inject(this);
    }


    @Override
    protected int getLayout() {
        return R.layout.activity_dictionaries;
    }

    @Override
    protected DictionariesView getThisMvpView() {
        return this;
    }

    @Override
    public void onDictionaryItemLongClick(View view, DictionarySourceMetadata dictionarySourceMetadata) {
        dictionaryActionModeCallback.setItem(view, dictionarySourceMetadata);
        dictionaryActionMode = startSupportActionMode(dictionaryActionModeCallback);
    }

    @Override
    public void onDictionaryStateSwitch(View view, DictionarySourceMetadata dictionarySourceMetadata, boolean isChecked) {
        getPresenter().onDictionaryStateSwitch(dictionarySourceMetadata, isChecked);
    }

    @Override
    public void onChooseUserWordsSourceButtonClick(DictionarySourceMetadata dictionarySourceMetadata) {
        getPresenter().onChooseUserWordsSourceButtonClick(dictionarySourceMetadata);
    }

    @Override
    public void showDictionariesAndUserWordsBriefCodes(DictionariesDTO dictionariesDTO) {
        dictionariesAdapter.setDictionariesAndBriefCodes(dictionariesDTO);
    }

    @Override
    public void showChooseUserWordsSourceDialog(long dictionarySourceId) {
        ChooseUserWordsSourceDialogFragment chooseUserWordsSourceDialogFragment =
                ChooseUserWordsSourceDialogFragment.createNewForDictionariesBinding(true, dictionarySourceId);
        chooseUserWordsSourceDialogFragment.show(getSupportFragmentManager(), "ChooseUserWordsSourceDialog");
    }

    @Override
    public void onChooseUserWordsSource(long itemId, UserWordsSourceMetadata userWordsSourceMetadata) {
        getPresenter().onBindDictionaryToUserWordsSource(itemId, userWordsSourceMetadata);
    }

    @Override
    public void onRedirectToUserWordsSources() {
        getNavigator().openUserWordsSourcesActivity();
    }

    @Override
    public void updateDictionariesData() {
        dictionariesAdapter.updateDictionariesData();
    }

    @Override
    public void showNoUserWordsSourceSpecifiedNotification() {
        Toast toast = Toast.makeText(
                getApplicationContext(),
                getString(R.string.label_enable_dictionary_source_user_words_source_not_specify),
                Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void showDictionaryDeleteConfirmation(DictionarySourceMetadata dictionarySourceMetadata, String dictionaryName, long filesSizeInBytes) {
        long fileSizeInKb = filesSizeInBytes / 1024;
        float fileSizeInMb = fileSizeInKb / 1024f;
        String message = String.format(getString(R.string.dialog_really_delete_dictionary_source_label), dictionaryName, fileSizeInMb);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_really_delete_dictionary_source_title)
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        getPresenter().onDeleteDictionary(dictionarySourceMetadata);
                    }})
                .setNegativeButton(R.string.dialog_button_cancel, null);
        alertDialogBuilder.show();
    }

    @Override
    public void showDictionaryIsDeleteNotification(String dictionaryName, long filesSizeInBytes) {
        long fileSizeInKb = filesSizeInBytes / 1024;
        float fileSizeInMb = fileSizeInKb / 1024f;
        
        Toast toast = Toast.makeText(
                getApplicationContext(),
                String.format(getString(R.string.label_dictionary_is_deleted), dictionaryName,  fileSizeInMb),
                Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void showNoDictionarySourcesMessageScreen() {
        showMessageScreen(getString(R.string.label_no_dictionaries));
    }

    class DictionaryActionModeCallback implements ActionMode.Callback {
        private View view;
        private DictionarySourceMetadata currentDictionarySourceMetadata;

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.action_mode_delete_menu, menu);
            view.setSelected(true);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_delete:
                    getPresenter().onDeleteDictionaryButtonClick(currentDictionarySourceMetadata);
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            view.setSelected(false);
            dictionaryActionMode = null;
            view = null;
            currentDictionarySourceMetadata = null;
        }

        public void setItem(View view, DictionarySourceMetadata dictionarySourceMetadata) {
            this.view = view;
            this.currentDictionarySourceMetadata = dictionarySourceMetadata;
        }
    }
}

package com.caveman.lexiconator.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.caveman.lexiconator.DTO.DictionariesDTO;
import com.caveman.lexiconator.R;
import com.caveman.lexiconator.model.DictionarySourceMetadata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class DictionariesAdapter extends RecyclerView.Adapter<DictionariesAdapter.DictionaryViewHolder>{

    private List<DictionarySourceMetadata> dictionarySourceMetadataList;
    private Map<Long, String> userWordsSourcesIdAndBriefCodes;
    private DictionaryItemListener dictionaryItemListener;
    private Set<DictionaryViewHolder> dictionaryViewHolders;

    public DictionariesAdapter(DictionaryItemListener dictionaryItemListener) {
        this.dictionarySourceMetadataList = new ArrayList<>();
        this.userWordsSourcesIdAndBriefCodes = new HashMap<>();
        this.dictionaryItemListener = dictionaryItemListener;
        this.dictionaryViewHolders = new HashSet<>();
    }

    @Override
    @NonNull
    public DictionaryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View dictionaryItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dictionary_res, parent, false);
        DictionaryViewHolder dictionaryViewHolder = new DictionariesAdapter.DictionaryViewHolder(dictionaryItemView, dictionaryItemListener);
        dictionaryViewHolders.add(dictionaryViewHolder);
        return dictionaryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DictionaryViewHolder holder, int position) {
        DictionarySourceMetadata dictionarySourceMetadata = dictionarySourceMetadataList.get(position);
        holder.bind(dictionarySourceMetadata, userWordsSourcesIdAndBriefCodes);
    }

    @Override
    public int getItemCount() {
        return dictionarySourceMetadataList.size();
    }

    public void setDictionariesAndBriefCodes(DictionariesDTO dictionariesDTO) {
        this.dictionarySourceMetadataList = dictionariesDTO.getDictionarySourceMetadataList();
        this.userWordsSourcesIdAndBriefCodes = dictionariesDTO.getUserWordsSourceIdsAndBriefCodesMap();
        notifyDataSetChanged();
    }

    public void updateDictionariesData() {
        for (DictionaryViewHolder dictionaryViewHolder : dictionaryViewHolders) {
            dictionaryViewHolder.updateData();
        }
    }

    public static class DictionaryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.dictionary_name) TextView dictionaryView;
        @BindView(R.id.words_count) TextView wordsCountView;
        @BindView(R.id.button_user_words_source_brief_code) Button userWordsSourceButton;
        @BindView(R.id.state_switch) SwitchCompat stateSwitch;
        private DictionaryItemListener dictionaryItemListener;
        private Map<Long, String> userWordsSourcesIdAndBriefCodes;
        private DictionarySourceMetadata dictionarySourceMetadata;

        public DictionaryViewHolder(View dictionaryItemView, DictionaryItemListener dictionaryItemLongClickListener) {
            super(dictionaryItemView);
            this.dictionaryItemListener = dictionaryItemLongClickListener;
            ButterKnife.bind(this, itemView);
        }

        @OnCheckedChanged(R.id.state_switch)
        public void onCheckedChanged(CompoundButton view, boolean isChecked) {
            dictionaryItemListener.onDictionaryStateSwitch(
                    view, dictionarySourceMetadata, isChecked
            );
        }

        @OnLongClick()
        public boolean onLongClick(View view) {
            dictionaryItemListener.onDictionaryItemLongClick(view, dictionarySourceMetadata);
            return true;
        }

        @OnClick(R.id.button_user_words_source_brief_code)
        public void onClick (Button button) {
            dictionaryItemListener.onChooseUserWordsSourceButtonClick(dictionarySourceMetadata);
        }

        public void bind(DictionarySourceMetadata dictionarySourceMetadata, Map<Long, String> userWordsSourcesIdAndBriefCodes) {
            this.userWordsSourcesIdAndBriefCodes = userWordsSourcesIdAndBriefCodes;
            this.dictionarySourceMetadata = dictionarySourceMetadata;
            updateData();
        }

        public void updateData(){
            long userWordsSourceId = dictionarySourceMetadata.getForUserWordsSourceId();

            String userWordsSourceBriefCode = (userWordsSourcesIdAndBriefCodes.containsKey(userWordsSourceId)) ?
                    userWordsSourcesIdAndBriefCodes.get(userWordsSourceId) : "?";

            dictionaryView.setText(dictionarySourceMetadata.getName());
            wordsCountView.setText(Integer.toString(dictionarySourceMetadata.getWordsCount()));
            userWordsSourceButton.setText(userWordsSourceBriefCode);
            stateSwitch.setChecked(dictionarySourceMetadata.isEnabled());

        }
    }
}

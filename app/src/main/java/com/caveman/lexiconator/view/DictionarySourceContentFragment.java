package com.caveman.lexiconator.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.caveman.lexiconator.Navigator;
import com.caveman.lexiconator.R;
import com.caveman.lexiconator.model.DictionaryWordMetadata;
import com.caveman.lexiconator.model.SortDirection;
import com.caveman.lexiconator.presenter.DictionarySourceContentPresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DictionarySourceContentFragment
        extends BaseFragment<DictionarySourceContentView, DictionarySourceContentPresenter>
        implements DictionarySourceContentView, ChooseSortDirectionDialogFragment.ChooseSortDirectionListener,
        ItemClickViewListener<DictionaryWordMetadata>  {

    public static final String ARG_DICTIONARY_SOURCE_ID = "ARG_DICTIONARY_SOURCE_ID";

    @BindView(R.id.words_view) RecyclerView wordsRecyclerView;
    private DictionarySourcesRecyclerViewAdapter wordsAdapter;
    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        getFragmentComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(
                R.layout.fragment_words_source, container, false);
        unbinder = ButterKnife.bind(this, view);

        getPresenter().onAttachView(this);

        wordsRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        wordsRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        wordsRecyclerView.setLayoutManager(layoutManager);
        registerForContextMenu(wordsRecyclerView);

        wordsAdapter = new DictionarySourcesRecyclerViewAdapter(this);
        wordsRecyclerView.setAdapter(wordsAdapter);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getPresenter().onDetachView();
        unbinder.unbind();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.app_bar_menu_dictionary_source, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                getPresenter().onChooseSortDirectionButtonClick();
                return true;
        }
        return false;
    }

    @Override
    public void onItemClick(View view, DictionaryWordMetadata dictionaryWordMetadata) {
        getPresenter().onWordClick(dictionaryWordMetadata);
    }

    @Override
    public void onItemLongClick(View view, DictionaryWordMetadata value) {

    }

    @Override
    public long getWordSourceId() {
        long userWordsSourceIdArg = -1;
        Bundle bundle = getArguments();
        if(bundle != null) {
            userWordsSourceIdArg = bundle.getLong(ARG_DICTIONARY_SOURCE_ID, -1);
        }
        return userWordsSourceIdArg;
    }

    @Override
    public void showWordsCounter(int count) {
        Activity activity = getActivity();
        if (activity != null) {
            activity.setTitle(getString(R.string.words_counter) + " " + String.valueOf(count));
        }
    }

    @Override
    public void showChooseSortingDialog() {
        FragmentManager fragmentManager = getChildFragmentManager();
        ChooseSortDirectionDialogFragment sortingDialogFragment = new ChooseSortDirectionDialogFragment();
        sortingDialogFragment.show(fragmentManager, "ChooseSortDirectionDialog");
    }

    @Override
    public Navigator getNavigator() {
        Activity activity = getActivity();
        if (activity instanceof MvpNavigatorView) {
            return ((MvpNavigatorView) activity).getNavigator();
        }
        return null;
    }

    @Override
    public void showLoadingScreen() {
        Activity activity = getActivity();
        if (activity instanceof MvpNavigatorView) {
            ((MvpNavigatorView) activity).showLoadingScreen();
        }
    }

    @Override
    public void hideLoadingScreen() {
        Activity activity = getActivity();
        if (activity instanceof MvpNavigatorView) {
            ((MvpNavigatorView) activity).hideLoadingScreen();
        }
    }

    @Override
    public void showNoWordsMessageScreen() {
        Activity activity = getActivity();
        if (activity instanceof MvpNavigatorView) {
            ((MvpNavigatorView) activity).showMessageScreen(activity.getString(R.string.label_no_words));
        }
    }

    @Override
    public void hideMessageScreen() {
        Activity activity = getActivity();
        if (activity instanceof MvpNavigatorView) {
            ((MvpNavigatorView) activity).hideMessageScreen();
        }
    }

    @Override
    public void showWords(List<DictionaryWordMetadata> wordsMetadataList) {
        wordsAdapter.setDictionaryWordsSourcesMetadataList(wordsMetadataList);
    }

    @Override
    public void clearWords() {
        wordsAdapter.setDictionaryWordsSourcesMetadataList(new ArrayList<>());
    }

    @Override
    public void onChooseSortDirection(SortDirection sortDirection) {
        getPresenter().onChooseCurrentSortDirection(sortDirection);
    }

    public static DictionarySourceContentFragment createNew(long dictionarySourceId) {
        Bundle bundle = new Bundle();
        bundle.putLong(ARG_DICTIONARY_SOURCE_ID, dictionarySourceId);
        DictionarySourceContentFragment dictionarySourceContentFragment = new DictionarySourceContentFragment();
        dictionarySourceContentFragment.setArguments(bundle);
        return dictionarySourceContentFragment;
    }
}

package com.caveman.lexiconator.view;

import com.caveman.lexiconator.model.WordTestData;

public interface WordTestView extends MvpView {

    void updateView(WordTestData wordTestData, boolean isHtml);

    void showAnswersWithColors(int currentAnswerId, int correctAnswerId);

    void notifyTestPassListener();

    void disableVariants();

    long getWordSourceIdArg();

    String getWordArg();
}

package com.caveman.lexiconator.view;

import android.support.annotation.NonNull;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.caveman.lexiconator.R;
import com.caveman.lexiconator.model.WordsSourceMetadata;
import com.caveman.lexiconator.presenter.MvpChooseWordsSourcePresenter;

import butterknife.BindView;

public abstract class BaseChooseWordsSourceActivity<
        V extends MvpChooseWordsSourceView,
        P extends MvpChooseWordsSourcePresenter<V, W>,
        W extends WordsSourceMetadata
        >
        extends BaseActivity<V, P> implements MvpChooseWordsSourceView<V> {

    @BindView(R.id.screen_message) View messageScreen;
    @BindView(R.id.screen_message_text_view) TextView screenMessageTextView;

    @Override
    public void onResume() {
        //We need to dynamically update a toolbar (specially the UserWordsSource button's text)
        invalidateOptionsMenu();
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_bar_menu_choose_words_source, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getPresenter().onToolbarUpdate();
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_choose_words_source:
                getPresenter().onChooseWordsSourceButtonClick();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void updateWordSourceLabel(String wordSourceLabelText) {
        MenuItem wordsSourceChooseItem = getToolbar().getMenu().findItem(R.id.action_choose_words_source);
        wordsSourceChooseItem.setTitle(wordSourceLabelText);
    }

    @Override
    public void showNoUserWordsSourcesMessageScreen() {
        showMessageScreen(getNoUserWordsSourcesClickableString());
    }

    @Override
    public void showNoDictionarySourcesMessageScreen() {
        showMessageScreen(getNoDictionarySourcesClickableString());
    }

    protected CharSequence getClickableString(String text, ClickableSpan linkClickableSpan) {
        String linkStartTag = "<link>";
        String linkEndTag = "</link>";
        if (text.contains(linkStartTag) && text.contains(linkEndTag)) {
            String clickableString = text.substring(
                    text.indexOf(linkStartTag) + linkStartTag.length(),
                    text.indexOf(linkEndTag)
            ).trim();

            text = text.replace(linkStartTag, "").replace(linkEndTag, "");
            SpannableString textResult = new SpannableString(text);

            textResult.setSpan(
                    linkClickableSpan,
                    text.indexOf(clickableString), text.indexOf(clickableString) + clickableString.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            return textResult;
        }
        else {
            return text;
        }
    }

    protected CharSequence getNoUserWordsSourcesClickableString() {
        return getClickableString(getString(R.string.label_no_user_words_sources_add), new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                hideMessageScreen();
                getNavigator().openUserWordsSourcesActivity();
            }
        });
    }

    protected CharSequence getNoDictionarySourcesClickableString() {
        return getClickableString(getString(R.string.label_no_dictionary_words_sources_add), new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                hideMessageScreen();
                getNavigator().openDictionariesActivity();
            }
        });
    }
}


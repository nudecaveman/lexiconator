package com.caveman.lexiconator.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.caveman.lexiconator.R;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.presenter.UserWordsSourcesPresenter;

import java.util.List;

import butterknife.BindView;

public class UserWordsSourcesActivity extends BaseActivity<UserWordsSourcesView, UserWordsSourcesPresenter>
        implements UserWordsSourcesView,
        AddWordsSourceDialogFragment.AddWordsSourceFragmentListener, EditWordsSourceDialogFragment.EditWordsSourceFragmentListener,
        ItemClickViewListener<UserWordsSourceMetadata> {

    @BindView(R.id.user_words_sources_view) RecyclerView recyclerView;
    private UserWordsSourcesRecyclerViewAdapter userWordsSourcesAdapter;
    private UserWordsSourcesActionModeCallback userWordsSourcesActionModeCallback;
    private ActionMode userWordsSourcesActionMode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.activity_title_user_words_sources);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        registerForContextMenu(recyclerView);

        userWordsSourcesAdapter = new UserWordsSourcesRecyclerViewAdapter(this);
        recyclerView.setAdapter(userWordsSourcesAdapter);

        userWordsSourcesActionModeCallback = new UserWordsSourcesActionModeCallback();

        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_user_words_sources;
    }

    @Override
    protected UserWordsSourcesView getThisMvpView() {
        return this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_bar_menu_user_words_sources, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.action_add:
                getPresenter().onAddWordsSourceButtonClick();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void showUserWordsSourcesMetadataList(List<UserWordsSourceMetadata> userWordsSourceMetadataList) {
        userWordsSourcesAdapter.setUserWordsSourcesMetadataList(userWordsSourceMetadataList);
    }

    @Override
    public void showAddWordsSourceDialog() {
        AddWordsSourceDialogFragment addWordsSourceDialogFragment = new AddWordsSourceDialogFragment();
        addWordsSourceDialogFragment.show(getSupportFragmentManager(), "AddWordsSourceDialog");
    }

    @Override
    public void showEditWordsSourceDialog(UserWordsSourceMetadata userWordsSourceMetadata) {
        EditWordsSourceDialogFragment editWordsSourceDialogFragment = EditWordsSourceDialogFragment.newInstance(
                userWordsSourceMetadata.getId(), userWordsSourceMetadata.getName(), userWordsSourceMetadata.getBriefCode()
        );
        editWordsSourceDialogFragment.show(getSupportFragmentManager(), "EditWordsSourceDialog");
    }

    @Override
    public void showConfirmWordsSourceDeleteDialog(UserWordsSourceMetadata userWordsSourceMetadata) {
        String messageString = String.format(getString(R.string.dialog_really_delete_user_words_source_label),
                                             userWordsSourceMetadata.getWordsCount());
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_really_delete_user_words_source_title)
                .setMessage(messageString)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        getPresenter().onDeleteUserWordsSource(userWordsSourceMetadata);
                    }})
                .setNegativeButton(R.string.dialog_button_cancel, null);
        alertDialogBuilder.show();
    }

    @Override
    public void showNoUserWordsSourcesMessageScreen() {
        showMessageScreen(getString(R.string.label_no_user_words_sources));
    }

    @Override
    public void onItemClick(View view, UserWordsSourceMetadata value) {

    }

    @Override
    public void onItemLongClick(View view, UserWordsSourceMetadata userWordsSourceMetadata) {
        if (userWordsSourcesActionMode == null) {
            userWordsSourcesActionModeCallback.setItem(view, userWordsSourceMetadata);
            userWordsSourcesActionMode = startSupportActionMode(userWordsSourcesActionModeCallback);
        }
    }

    @Override
    public void onAddWordsSource(String wordsSourceName, String wordsSourceBriefCode) {
        getPresenter().onAddNewUserWordsSource(wordsSourceName, wordsSourceBriefCode);
    }

    @Override
    public void onEditWordsSource(long userWordsSourceId, String wordsSourceName, String wordsSourceBriefCode) {
        getPresenter().onEditUserWordsSource(userWordsSourceId, wordsSourceName, wordsSourceBriefCode);
    }

    class UserWordsSourcesActionModeCallback implements ActionMode.Callback {
        private View view;
        private UserWordsSourceMetadata userWordsSourceMetadata;

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.action_mode_edit_delete_menu, menu);
            view.setSelected(true);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_edit:
                    getPresenter().onEditWordsSourceButtonClick(userWordsSourceMetadata);
                    //Todo Запустить активити создания записи в кастомной словаре
                    mode.finish();
                    return true;
                case R.id.action_delete:
                    getPresenter().onDeleteWordsSourceButtonClick(userWordsSourceMetadata);
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            view.setSelected(false);
            userWordsSourcesActionMode = null;
            view = null;
            userWordsSourceMetadata = null;
        }

        public void setItem(View view, UserWordsSourceMetadata userWordsSourceMetadata) {
            this.view = view;
            this.userWordsSourceMetadata = userWordsSourceMetadata;
        }
    }
}

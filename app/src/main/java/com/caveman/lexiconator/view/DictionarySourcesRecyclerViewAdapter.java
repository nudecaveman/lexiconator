package com.caveman.lexiconator.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.caveman.lexiconator.R;
import com.caveman.lexiconator.model.DictionaryWordMetadata;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class DictionarySourcesRecyclerViewAdapter extends RecyclerView.Adapter<DictionarySourcesRecyclerViewAdapter.DictionaryWordsSourceViewHolder>{

    private List<DictionaryWordMetadata> wordsMetadataList;
    private ItemClickViewListener<DictionaryWordMetadata> itemClickViewListener;

    public DictionarySourcesRecyclerViewAdapter(ItemClickViewListener<DictionaryWordMetadata> itemClickViewListener) {
        this.itemClickViewListener = itemClickViewListener;
    }

    @NonNull
    @Override
    public DictionaryWordsSourceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View wordItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_word, parent, false);
        return new DictionaryWordsSourceViewHolder(wordItemView, itemClickViewListener);
    }

    @Override
    public void onBindViewHolder(@NonNull DictionaryWordsSourceViewHolder holder, int position) {
        DictionaryWordMetadata dictionaryWordMetadata = wordsMetadataList.get(position);
        holder.bind(dictionaryWordMetadata);
    }

    @Override
    public int getItemCount() {
        return wordsMetadataList != null ? wordsMetadataList.size() : 0;
    }

    public void setDictionaryWordsSourcesMetadataList(List<DictionaryWordMetadata> wordMetadataList) {
        this.wordsMetadataList = wordMetadataList;
        notifyDataSetChanged();
    }

    public static class DictionaryWordsSourceViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.word) TextView wordView;
        private ItemClickViewListener<DictionaryWordMetadata> itemClickViewListener;
        private DictionaryWordMetadata wordMetadata;

        public DictionaryWordsSourceViewHolder(View wordItemView, ItemClickViewListener<DictionaryWordMetadata> itemClickViewListener) {
            super(wordItemView);
            ButterKnife.bind(this, itemView);
            this.itemClickViewListener = itemClickViewListener;
        }

        @OnLongClick()
        public boolean onLongClick(View view) {
            itemClickViewListener.onItemLongClick(view, wordMetadata);
            return true;
        }

        @OnClick()
        public void onClick (View view) {
            itemClickViewListener.onItemClick(view, wordMetadata);
        }

        public void bind(DictionaryWordMetadata wordMetadata) {
            wordView.setText(wordMetadata.getWord());
            this.wordMetadata = wordMetadata;
        }
    }
}

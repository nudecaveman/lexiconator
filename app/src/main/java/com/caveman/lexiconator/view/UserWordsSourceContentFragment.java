package com.caveman.lexiconator.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.caveman.lexiconator.Navigator;
import com.caveman.lexiconator.R;
import com.caveman.lexiconator.model.SortingOrder;
import com.caveman.lexiconator.model.UserWordsMinMaxPriorityData;
import com.caveman.lexiconator.model.room.UserWordMetadata;
import com.caveman.lexiconator.presenter.UserWordsSourceContentPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class UserWordsSourceContentFragment
        extends BaseFragment<UserWordsSourceContentView, UserWordsSourceContentPresenter>
        implements UserWordsSourceContentView, AddWordDialogFragment.AddWordDialogFragmentListener,
        EditWordDialogFragment.EditWordDialogFragmentListener, ChooseSortingOrderDialogFragment.ChooseSortingOrderListener,
        ItemClickViewListener<UserWordMetadata> {

    public static final String ARG_USER_WORDS_SOURCE_ID = "ARG_WORDS_SOURCE_ID";

    @BindView(R.id.words_view) RecyclerView userWordsRecyclerView;
    private UserWordsRecyclerViewAdapter userWordsAdapter;
    private UserWordsActionModeCallback wordsActionModeCallback;
    private ActionMode wordsActionMode;
    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        getFragmentComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(
                R.layout.fragment_words_source, container, false);
        unbinder = ButterKnife.bind(this, view);

        getPresenter().onAttachView(this);

        userWordsRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        userWordsRecyclerView.setLayoutManager(layoutManager);
        registerForContextMenu(userWordsRecyclerView);

        userWordsAdapter = new UserWordsRecyclerViewAdapter(this);
        userWordsRecyclerView.setAdapter(userWordsAdapter);

        wordsActionModeCallback = new UserWordsActionModeCallback();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getPresenter().onDetachView();
        unbinder.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.app_bar_menu_user_words_source, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                getPresenter().onAddWordButtonClick();
                return true;
            case R.id.action_sort:
                getPresenter().onChooseSortingOrderButtonClick();
                return true;
        }
        return false;
    }

    @Override
    public void onItemClick(View view, UserWordMetadata userWordMetadata) {
        getPresenter().onWordClick(userWordMetadata);
    }

    @Override
    public void onItemLongClick(View view, UserWordMetadata userWordMetadata) {
        Activity activity = getActivity();
        if (activity instanceof AppCompatActivity && wordsActionMode == null) {
            wordsActionModeCallback.setItem(view, userWordMetadata);
            wordsActionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(wordsActionModeCallback);
        }
    }

    @Override
    public void onChooseSortingOrder(SortingOrder sortingOrder) {
        getPresenter().onChooseCurrentSortingOrder(sortingOrder);
    }

    @Override
    public long getWordSourceId() {
        long userWordsSourceIdArg = -1;
        Bundle bundle = getArguments();
        if(bundle != null) {
            userWordsSourceIdArg = bundle.getLong(ARG_USER_WORDS_SOURCE_ID);
        }
        return userWordsSourceIdArg;
    }

    @Override
    public void showWordsCounter(int count) {
        Activity activity = getActivity();
        if (activity != null) {
            activity.setTitle(getString(R.string.words_counter) + " " + String.valueOf(count));
        }
    }

    @Override
    public void showCannotOpenWordCardToast() {
        Toast toast = Toast.makeText(
                getContext(),
                getString(R.string.label_cannot_open_word_card), Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void showWords(List<UserWordMetadata> userWordsSourceData, UserWordsMinMaxPriorityData userWordsMinMaxPriorityData) {
        userWordsAdapter.setWords(userWordsSourceData, userWordsMinMaxPriorityData);
    }

    @Override
    public void showAddWordDialog() {
        FragmentManager fragmentManager = getChildFragmentManager();
        AddWordDialogFragment addWordDialogFragment = new AddWordDialogFragment();
        addWordDialogFragment.show(fragmentManager, "AddWordDialog");
    }

    @Override
    public void onAddWord(String word) {
        getPresenter().onAddWord(word);
    }

    @Override
    public void showEditWordDialog(String word, long wordId) {
        FragmentManager fragmentManager = getChildFragmentManager();
        EditWordDialogFragment editWordDialogFragment = EditWordDialogFragment.newInstance(word, wordId);
        editWordDialogFragment.show(fragmentManager, "EditWordDialog");
    }

    @Override
    public void onEditWord(String word, long wordId) {
        getPresenter().onEditWord(word, wordId);
    }

    @Override
    public void showChooseSortingDialog() {
        FragmentManager fragmentManager = getChildFragmentManager();
        ChooseSortingOrderDialogFragment sortingDialogFragment = new ChooseSortingOrderDialogFragment();
        sortingDialogFragment.show(fragmentManager, "ChooseSortingDialog");
    }

    @Override
    public Navigator getNavigator() {
        Activity activity = getActivity();
        if (activity instanceof MvpNavigatorView) {
            return ((MvpNavigatorView) activity).getNavigator();
        }
        return null;
    }

    @Override
    public void showLoadingScreen() {
        Activity activity = getActivity();
        if (activity instanceof MvpNavigatorView) {
            ((MvpNavigatorView) activity).showLoadingScreen();
        }
    }

    @Override
    public void hideLoadingScreen() {
        Activity activity = getActivity();
        if (activity instanceof MvpNavigatorView) {
            ((MvpNavigatorView) activity).hideLoadingScreen();
        }
    }

    @Override
    public void showNoWordsMessageScreen() {
        Activity activity = getActivity();
        if (activity instanceof MvpNavigatorView) {
            ((MvpNavigatorView) activity).showMessageScreen(activity.getString(R.string.label_no_words));
        }
    }

    @Override
    public void hideMessageScreen() {
        Activity activity = getActivity();
        if (activity instanceof MvpNavigatorView) {
            ((MvpNavigatorView) activity).hideMessageScreen();
        }
    }

    @Override
    public void clearWords() {
        userWordsAdapter.clearWords();
    }

    public static UserWordsSourceContentFragment createNew(long userWordsSourceId) {
        Bundle bundle = new Bundle();
        bundle.putLong(ARG_USER_WORDS_SOURCE_ID, userWordsSourceId);
        UserWordsSourceContentFragment userWordsSourceContentFragment = new UserWordsSourceContentFragment();
        userWordsSourceContentFragment.setArguments(bundle);
        return userWordsSourceContentFragment;
    }

    class UserWordsActionModeCallback implements ActionMode.Callback {
        private View view;
        private UserWordMetadata currentUserWordMetadata;

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.action_mode_edit_delete_menu, menu);
            view.setSelected(true);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_edit:
                    getPresenter().onEditWordButtonClick(currentUserWordMetadata);
                    mode.finish();
                    return true;
                case R.id.action_delete:
                    getPresenter().onDeleteWordButtonClick(currentUserWordMetadata);
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            view.setSelected(false);
            wordsActionMode = null;
            view = null;
            currentUserWordMetadata = null;
        }

        public void setItem(View view, UserWordMetadata userWordMetadata) {
            this.view = view;
            this.currentUserWordMetadata = userWordMetadata;
        }
    }
}

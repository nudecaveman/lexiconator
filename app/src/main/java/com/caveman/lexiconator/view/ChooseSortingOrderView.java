package com.caveman.lexiconator.view;

import com.caveman.lexiconator.model.SortingOrder;

public interface ChooseSortingOrderView extends MvpView {

    void updateView(SortingOrder sortingOrder);

    void notifyListener(SortingOrder sortingOrder);
}

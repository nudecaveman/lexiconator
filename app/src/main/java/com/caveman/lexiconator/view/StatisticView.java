package com.caveman.lexiconator.view;

import java.util.Date;
import java.util.Map;

public interface StatisticView extends MvpChooseWordsSourceView<StatisticView> {

    void showDatePicker(int buttonId, Date startDate, Date endDate);

    void buildGraph(Map<Date, Integer> addedWordsInDayStatistic);

    void showWordsCountStatistic(int wordsCount);

    void updateDateButtons(Date startDate, Date endDate);
}

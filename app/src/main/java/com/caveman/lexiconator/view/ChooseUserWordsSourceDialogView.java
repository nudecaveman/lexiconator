package com.caveman.lexiconator.view;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.util.List;

public interface ChooseUserWordsSourceDialogView extends MvpView {

    void showUserWordsSources(List<UserWordsSourceMetadata> userWordsSourceMetadataList);

    void notifyListenerChooseUserWordsSource(UserWordsSourceMetadata userWordsSourceMetadata);

    void notifyListenerRedirectToUserWordsSources();

    void close();
}

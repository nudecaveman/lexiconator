package com.caveman.lexiconator.view;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import com.caveman.lexiconator.R;
import com.caveman.lexiconator.model.DictionarySourceMetadata;
import com.caveman.lexiconator.model.WordsSourceMetadata;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.presenter.WordsBrowserPresenter;

import butterknife.BindView;

public class WordsBrowserActivity
        extends BaseChooseWordsSourceActivity<WordsBrowserView, WordsBrowserPresenter, WordsSourceMetadata>
        implements WordsBrowserView, ChooseWordsSourceDialogFragment.ChooseWordsSourceListener {

    @BindView(R.id.words_browser_fragment_container) View wordsBrowserFragmentContainer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setDefaultTitle();

        getActivityComponent().inject(this);
    }

    @Override
    public void showChooseWordsSourceDialog() {
        ChooseWordsSourceDialogFragment chooseWordsSourceDialogFragment = new ChooseWordsSourceDialogFragment();
        chooseWordsSourceDialogFragment.show(getSupportFragmentManager(), "ChooseWordsSourceDialog");
    }

    @Override
    public void updateWordSourceLabel(String wordSourceLabelText) {
        MenuItem wordsSourceChooseItem = getToolbar().getMenu().findItem(R.id.action_choose_words_source);
        wordsSourceChooseItem.setTitle(wordSourceLabelText);
    }

    @Override
    public void onChooseWordsSource(WordsSourceMetadata wordsSourceMetadata) {
        getPresenter().onChooseWordsSource(wordsSourceMetadata);
    }

    @Override
    public void openUserWordsSourceContent(UserWordsSourceMetadata userWordsSourceMetadata) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        UserWordsSourceContentFragment userWordsSourceContentFragment = UserWordsSourceContentFragment
                .createNew(userWordsSourceMetadata.getId());

        fragmentTransaction.replace(R.id.words_browser_fragment_container, userWordsSourceContentFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void openDictionarySourceContent(DictionarySourceMetadata dictionarySourceMetadata) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        DictionarySourceContentFragment dictionarySourceContentFragment = DictionarySourceContentFragment
                .createNew(dictionarySourceMetadata.getId());

        fragmentTransaction.replace(R.id.words_browser_fragment_container, dictionarySourceContentFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void showNoWordsSourcesMessageScreen() {
        setDefaultTitle();
        CharSequence noUserWordsSourcesClickableString = getNoUserWordsSourcesClickableString();
        CharSequence noDictionarySourcesClickableString = getNoDictionarySourcesClickableString();
        CharSequence message = TextUtils.concat(noUserWordsSourcesClickableString, "\n\n\n", noDictionarySourcesClickableString);
        showMessageScreen(message);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_words_browser;
    }

    @Override
    protected WordsBrowserView getThisMvpView() {
        return this;
    }

    private void setDefaultTitle() {
        setTitle(R.string.activity_title_words_browser);
    }
}

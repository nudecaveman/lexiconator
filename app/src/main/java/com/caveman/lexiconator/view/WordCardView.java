package com.caveman.lexiconator.view;

import com.caveman.lexiconator.model.WordData;

import java.util.List;

public interface WordCardView extends MvpView {

    void updateView(List<WordData> wordDataList, boolean isHtml);

    boolean getFromSpecificDictionaryArg();

    long getWordSourceIdArg();

    String getWordArg();
}

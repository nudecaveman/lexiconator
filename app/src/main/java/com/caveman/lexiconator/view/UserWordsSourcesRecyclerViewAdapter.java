package com.caveman.lexiconator.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.caveman.lexiconator.R;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class UserWordsSourcesRecyclerViewAdapter extends RecyclerView.Adapter<UserWordsSourcesRecyclerViewAdapter.UserWordsSourceViewHolder>{

    private List<UserWordsSourceMetadata> userWordsSourceMetadataList;
    private ItemClickViewListener<UserWordsSourceMetadata> itemClickViewListener;

    public UserWordsSourcesRecyclerViewAdapter(ItemClickViewListener<UserWordsSourceMetadata> itemClickViewListener) {
        this.itemClickViewListener = itemClickViewListener;
    }

    @NonNull
    @Override
    public UserWordsSourceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View wordItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_words_source, parent, false);
        return new UserWordsSourceViewHolder(wordItemView, itemClickViewListener);
    }

    @Override
    public void onBindViewHolder(@NonNull UserWordsSourceViewHolder holder, int position) {
        UserWordsSourceMetadata userWordsSourceMetadata = userWordsSourceMetadataList.get(position);
        holder.bind(userWordsSourceMetadata);
    }

    @Override
    public int getItemCount() {
        return userWordsSourceMetadataList != null ? userWordsSourceMetadataList.size() : 0;
    }

    public void setUserWordsSourcesMetadataList(List<UserWordsSourceMetadata> userWordsSourceMetadataList) {
        this.userWordsSourceMetadataList = userWordsSourceMetadataList;
        notifyDataSetChanged();
    }

    public static class UserWordsSourceViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.user_words_source_name) TextView nameView;
        @BindView(R.id.user_words_source_brief_code) TextView briefCodeView;
        @BindView(R.id.user_words_source_words_count) TextView wordsCountView;
        private ItemClickViewListener<UserWordsSourceMetadata> itemClickViewListener;
        private UserWordsSourceMetadata userWordsSourceMetadata;

        public UserWordsSourceViewHolder(View wordItemView, ItemClickViewListener<UserWordsSourceMetadata> itemClickViewListener) {
            super(wordItemView);
            ButterKnife.bind(this, itemView);
            this.itemClickViewListener = itemClickViewListener;
        }

        @OnLongClick()
        public boolean onLongClick(View view) {
            itemClickViewListener.onItemLongClick(view, userWordsSourceMetadata);
            return true;
        }

        @OnClick()
        public void onClick (View view) {
            itemClickViewListener.onItemClick(view, userWordsSourceMetadata);
        }

        public void bind(UserWordsSourceMetadata userWordsSourceMetadata) {
            nameView.setText(userWordsSourceMetadata.getName());
            briefCodeView.setText(userWordsSourceMetadata.getBriefCode());
            wordsCountView.setText(String.valueOf(userWordsSourceMetadata.getWordsCount()));
            this.userWordsSourceMetadata = userWordsSourceMetadata;
        }
    }
}

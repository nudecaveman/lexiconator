package com.caveman.lexiconator.view;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.caveman.lexiconator.R;

public class EditWordDialogFragment extends DialogFragment {

    public static final String ARG_WORD = "ARG_WORD";
    public static final String ARG_WORD_ID = "ARG_WORD_ID";

    interface EditWordDialogFragmentListener {
        void onEditWord(String word, long wordId);
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String wordArg = "";
        long wordIdArg = -1;
        Bundle bundle = getArguments();
        if(bundle != null) {
            wordArg = bundle.getString(ARG_WORD, "");
            wordIdArg = bundle.getLong(ARG_WORD_ID, -1);
        }

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_edit_word, null);
        EditText editWordText = view.findViewById(R.id.word);
        editWordText.setText(wordArg);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        long finalWordIdArg = wordIdArg;
        builder.setMessage(R.string.dialog_edit_word_title)
                .setView(view)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Fragment fragment = getParentFragment();
                        if(fragment instanceof EditWordDialogFragmentListener) {
                            ((EditWordDialogFragmentListener) fragment).onEditWord(
                                    editWordText.getText().toString(),
                                    finalWordIdArg
                            );
                        }
                    }
                })
                .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        return builder.create();
    }

    public static EditWordDialogFragment newInstance(String word, long wordId) {

        EditWordDialogFragment editWordDialogFragment = new EditWordDialogFragment();

        Bundle args = new Bundle();
        args.putString(ARG_WORD, word);
        args.putLong(ARG_WORD_ID, wordId);
        editWordDialogFragment.setArguments(args);

        return editWordDialogFragment;
    }
}
package com.caveman.lexiconator.view;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;

import com.caveman.lexiconator.R;

public class AddWordDialogFragment extends DialogFragment {

    interface AddWordDialogFragmentListener {
        void onAddWord(String word);
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_add_word_title)
                .setView(R.layout.dialog_add_word)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        EditText editText = getDialog().findViewById(R.id.add_word_dialog_text);

                        Fragment fragment = getParentFragment();
                        if(fragment instanceof AddWordDialogFragmentListener) {
                            ((AddWordDialogFragmentListener) fragment).onAddWord(editText.getText().toString());
                        }
                    }
                })
                .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        return builder.create();
    }
}

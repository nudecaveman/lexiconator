package com.caveman.lexiconator.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;

import com.caveman.lexiconator.R;
import com.caveman.lexiconator.SwipeDisableViewPager;
import com.caveman.lexiconator.model.room.UserWordMetadata;
import com.caveman.lexiconator.presenter.WordsTrainingPresenter;
import com.eftimoff.viewpagertransformers.CubeOutTransformer;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class WordsTrainingActivity
        extends BaseChooseUserWordsSourceActivity<WordsTrainingView, WordsTrainingPresenter>
        implements WordsTrainingView {

    @BindView(R.id.fab_next) FloatingActionButton nextButton;
    @BindView(R.id.tests_view_pager) SwipeDisableViewPager testsViewPager;
    private WordsTrainingPagerAdapter wordsTrainingPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.activity_title_words_training);

        testsViewPager.setPagingEnabled(false);
        nextButton.setVisibility(View.INVISIBLE);

        testsViewPager.setPageTransformer(true, new CubeOutTransformer());

        getActivityComponent().inject(this);

        resetTests();
    }

    @Override
    public void showWordsTests(List<UserWordMetadata> wordsMetadataList) {
        wordsTrainingPagerAdapter.setWords(wordsMetadataList);
    }

    @Override
    public void addWordsTests(List<UserWordMetadata> wordsMetadataList) {
        wordsTrainingPagerAdapter.addWords(wordsMetadataList);
    }

    @Override
    public void onTestPass() {
        getPresenter().onTestPass();
    }

    @Override
    public void showNextButton() {
        nextButton.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.fab_next)
    public void onClick(FloatingActionButton button) {
        getPresenter().onNextTestButtonClick();
    }

    @Override
    public void showNextTest() {
        nextButton.setVisibility(View.INVISIBLE);
        testsViewPager.setCurrentItem(testsViewPager.getCurrentItem() + 1, true);
    }

    @Override
    public void showNoUserWordsMessageScreen() {
        showMessageScreen(getString(R.string.label_no_user_words_for_training));
    }

    @Override
    public void resetTests() {
        wordsTrainingPagerAdapter = new WordsTrainingPagerAdapter(getSupportFragmentManager());
        testsViewPager.setAdapter(wordsTrainingPagerAdapter);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_words_training;
    }

    @Override
    protected WordsTrainingView getThisMvpView() {
        return this;
    }

    private class WordsTrainingPagerAdapter extends FragmentStatePagerAdapter {
        private List<UserWordMetadata> wordsMetadataList;

        public WordsTrainingPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
            wordsMetadataList = new ArrayList<>();
        }

        @Override
        public Fragment getItem(int position) {
            if (position == wordsMetadataList.size() - 1) {
                getPresenter().onWordsRunOut();
            }
            UserWordMetadata wordMetadata = wordsMetadataList.get(position);
            return WordTestMeaningAndWordsFragment.createNew(wordMetadata.getWord(), wordMetadata.getUserWordsSourceId());
        }

        @Override
        public int getCount() {
            return wordsMetadataList.size();
        }

        public void setWords(List<UserWordMetadata> wordsMetadataList) {
            this.wordsMetadataList = wordsMetadataList;
            notifyDataSetChanged();
        }

        public void addWords(List<UserWordMetadata> wordsMetadataList) {
            this.wordsMetadataList.addAll(wordsMetadataList);
            notifyDataSetChanged();
        }
    }
}

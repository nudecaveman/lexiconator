package com.caveman.lexiconator.view;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.caveman.lexiconator.R;
import com.caveman.lexiconator.model.WordsSourceMetadata;
import com.caveman.lexiconator.model.WordsSourcesData;
import com.caveman.lexiconator.presenter.ChooseWordsSourcePresenter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ChooseWordsSourceDialogFragment
        extends BaseFragment<ChooseWordsSourceDialogView, ChooseWordsSourcePresenter>
        implements ChooseWordsSourceDialogView {

    @BindView(R.id.words_source_choose_view) ExpandableListView wordsSourceChooseExpandedListView;
    private Unbinder unbinder;
    private List<CharSequence> userWordsSourcesChildGroup;
    private List<CharSequence> dictionarySourcesChildGroup;

    interface ChooseWordsSourceListener {
        void onChooseWordsSource(WordsSourceMetadata wordsSourceMetadata);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentComponent().inject(this);
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_choose_words_source, null);
        unbinder = ButterKnife.bind(this, view);

        getPresenter().onAttachView(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_choose_words_source_title)
                .setView(view);

        return builder.create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        
        getPresenter().onDetachView();
        unbinder.unbind();
    }

    @Override
    public void showWordsSources(WordsSourcesData wordsSourcesData) {

        //Todo провести рефакторинг класа и убрать вермишель и сделать все более явным и наглядным

        List<List<CharSequence>> childData = new ArrayList<>();

        userWordsSourcesChildGroup = getExpandedListChildDataItemFor(wordsSourcesData.getUserWordsSourceMetadataList());
        if (userWordsSourcesChildGroup.size() == 0 && (getActivity() instanceof MvpNavigatorView)) {
            CharSequence createNewUserWordsSourceClickableString = getClickableString(
                    getString(R.string.label_no_user_words_sources_add), new ClickableSpan() {
                        @Override
                        public void onClick(@NonNull View widget) { }});
            userWordsSourcesChildGroup.add(createNewUserWordsSourceClickableString);
        }
        childData.add(userWordsSourcesChildGroup);

        dictionarySourcesChildGroup = getExpandedListChildDataItemFor(wordsSourcesData.getDictionarySourceMetadataList());
        if (dictionarySourcesChildGroup.size() == 0 && (getActivity() instanceof MvpNavigatorView)) {
            CharSequence addNewDictionarySourceClickableString = getClickableString(
                    getString(R.string.label_no_dictionary_words_sources_add), new ClickableSpan() {
                        @Override
                        public void onClick(@NonNull View widget) { }});

            dictionarySourcesChildGroup.add(addNewDictionarySourceClickableString);
        }
        childData.add(dictionarySourcesChildGroup);

        WordsSourcesExpandableListAdapter wordsSourcesExpandableListAdapter = new WordsSourcesExpandableListAdapter(
                getContext(), getGroupsData(), childData);

        wordsSourceChooseExpandedListView.setAdapter(wordsSourcesExpandableListAdapter);
        wordsSourceChooseExpandedListView.setOnChildClickListener(getOnChildClickListener(wordsSourcesData));
        wordsSourceChooseExpandedListView.expandGroup(0);
        wordsSourceChooseExpandedListView.expandGroup(1);

    }

    @Override
    public void notifyListener(WordsSourceMetadata wordSource) {
        Activity activity = getActivity();
        if (activity instanceof ChooseWordsSourceListener) {
            ((ChooseWordsSourceListener) activity).onChooseWordsSource(wordSource);
        }
    }

    @Override
    public void close() {
        dismiss();
    }

    private List<String> getGroupsData() {
        String[] groups = new String[] {
                getString(R.string.dialog_choose_words_source_label_user_words_source),
                getString(R.string.dialog_choose_words_source_label_dictionary_source),
        };

        return Arrays.asList(groups);
    }

    private List<CharSequence> getExpandedListChildDataItemFor(List<? extends WordsSourceMetadata> wordsSourcesMetadataList) {
        List<CharSequence> childDataItem = new ArrayList<>();
        for (WordsSourceMetadata sourceMetadata : wordsSourcesMetadataList) {
            childDataItem.add(sourceMetadata.getName());
        }
        return childDataItem;
    }

    private ExpandableListView.OnChildClickListener getOnChildClickListener(WordsSourcesData wordsSourcesData) {
        return new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view,
                    int groupPosition, int childPosition, long l) {
                CharSequence wordsSourceItem;
                if (groupPosition == 0) {
                    wordsSourceItem = userWordsSourcesChildGroup.get(childPosition);
                    if (wordsSourceItem instanceof SpannableString && (getActivity() instanceof MvpNavigatorView)) {
                        MvpNavigatorView navigatorView = (MvpNavigatorView) getActivity();
                        navigatorView.getNavigator().openUserWordsSourcesActivity();
                        return false;
                    }
                }
                else {
                    wordsSourceItem = dictionarySourcesChildGroup.get(childPosition);
                    if (wordsSourceItem instanceof SpannableString && (getActivity() instanceof MvpNavigatorView)) {
                        MvpNavigatorView navigatorView = (MvpNavigatorView) getActivity();
                        navigatorView.getNavigator().openDictionariesActivity();
                        return false;
                    }
                }

                List<? extends WordsSourceMetadata> wordsSourceMetadataList;
                if (groupPosition == 0) {
                    wordsSourceMetadataList = wordsSourcesData.getUserWordsSourceMetadataList();
                } else {
                    wordsSourceMetadataList = wordsSourcesData.getDictionarySourceMetadataList();
                }
                WordsSourceMetadata wordsSourceMetadata = wordsSourceMetadataList.get(childPosition);
                getPresenter().onWordsSourceClick(wordsSourceMetadata);
                return false;
            }
        };
    }

    public static class WordsSourcesExpandableListAdapter extends BaseExpandableListAdapter {

        private Context context;
        private List<String> groupData;
        private List<List<CharSequence>> childData;

        public WordsSourcesExpandableListAdapter(Context context, List<String> groupData,
                List<List<CharSequence>> childData) {
            this.context = context;
            this.groupData = groupData;
            this.childData = childData;
        }

        @Override
        public int getGroupCount() {
            return groupData.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return childData.get(groupPosition).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return childData.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return childData.get(groupPosition).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(android.R.layout.simple_expandable_list_item_1, null);
            }
            TextView textView = convertView.findViewById(android.R.id.text1);
            textView.setText(groupData.get(groupPosition));
            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.item_list_words_source, null);
            }
            TextView textView = convertView.findViewById(R.id.words_source_name);
            textView.setText(childData.get(groupPosition).get(childPosition));
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }
}

package com.caveman.lexiconator.view;

import android.view.View;

import com.caveman.lexiconator.model.DictionarySourceMetadata;

public interface DictionaryItemListener {

    void onDictionaryItemLongClick(View view, DictionarySourceMetadata dictionarySourceMetadata);

    void onChooseUserWordsSourceButtonClick(DictionarySourceMetadata dictionarySourceMetadata);

    void onDictionaryStateSwitch(View view, DictionarySourceMetadata dictionarySourceMetadata, boolean isChecked);
}

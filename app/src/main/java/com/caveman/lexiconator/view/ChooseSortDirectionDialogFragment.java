package com.caveman.lexiconator.view;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.caveman.lexiconator.R;
import com.caveman.lexiconator.model.SortDirection;
import com.caveman.lexiconator.presenter.ChooseSortDirectionPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ChooseSortDirectionDialogFragment
        extends BaseFragment<ChooseSortDirectionView, ChooseSortDirectionPresenter>
        implements ChooseSortDirectionView {

    @BindView(R.id.sort_group_direction) RadioGroup directionGroup;
    @BindView(R.id.sort_asc) RadioButton sortColumnRadioButton;
    @BindView(R.id.sort_desc) RadioButton sortDirectionRadioButton;
    private Unbinder unbinder;

    interface ChooseSortDirectionListener {
        void onChooseSortDirection(SortDirection sortDirection);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentComponent().inject(this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_sort_direction, null);
        unbinder = ButterKnife.bind(this, view);

        getPresenter().onAttachView(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_title_sorting)
                .setView(view)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        getPresenter().onChooseSortDirection(getSortDirection());
                    }
                })
                .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getPresenter().onDetachView();
        unbinder.unbind();
    }

    @Override
    public void updateView(SortDirection currentSortDirection) {
        initCurrentSorting(currentSortDirection);
    }

    @Override
    public void notifyListener(SortDirection sortDirection) {
        Fragment fragment = getParentFragment();
        if(fragment instanceof ChooseSortDirectionListener) {
            ((ChooseSortDirectionListener) fragment).onChooseSortDirection(sortDirection);
        }
    }


    private void initCurrentSorting(SortDirection currentSortDirection) {

        switch (currentSortDirection) {
            case ASCENDANT:
                sortColumnRadioButton.toggle();
                break;
            case DESCENDANT:
                sortDirectionRadioButton.toggle();
                break;
            default:
                sortColumnRadioButton.toggle();
                break;
        }
    }

    private SortDirection getSortDirection() {
        if (directionGroup.getCheckedRadioButtonId() == R.id.sort_asc) {
            return SortDirection.ASCENDANT;
        }
        else {
            return SortDirection.DESCENDANT;
        }
    }
}

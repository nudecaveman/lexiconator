package com.caveman.lexiconator.view;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.util.List;

public interface UserWordsSourcesView extends MvpNavigatorView<UserWordsSourcesView> {

    void showUserWordsSourcesMetadataList(List<UserWordsSourceMetadata> userWordsSourceMetadataList);

    void showAddWordsSourceDialog();

    void showEditWordsSourceDialog(UserWordsSourceMetadata userWordsSourceMetadata);

    void showConfirmWordsSourceDeleteDialog(UserWordsSourceMetadata userWordsSourceMetadata);

    void showNoUserWordsSourcesMessageScreen();
}

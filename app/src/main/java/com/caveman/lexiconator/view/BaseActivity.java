package com.caveman.lexiconator.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.caveman.lexiconator.LexiconatorApplication;
import com.caveman.lexiconator.Navigator;
import com.caveman.lexiconator.NavigatorImpl;
import com.caveman.lexiconator.R;
import com.caveman.lexiconator.dependency_injection.ActivityComponent;
import com.caveman.lexiconator.presenter.MvpPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseActivity<V extends MvpNavigatorView, P extends MvpPresenter<V>>
        extends AppCompatActivity implements MvpNavigatorView<V> {

    private DrawerLayout drawer;
    private Toolbar toolbar;
    private FrameLayout contentScreen;
    @BindView(R.id.screen_loading) View loadingScreen;
    @BindView(R.id.screen_message) View messageScreen;
    @BindView(R.id.screen_message_text_view) TextView screenMessageTextView;
    private Navigator navigator;
    private P presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        ButterKnife.bind(this);

        navigator = new NavigatorImpl(this);
    }

    @Override
    public void setContentView(int layoutResID) {
        LayoutInflater layoutInflater = getLayoutInflater();
        drawer = (DrawerLayout) layoutInflater.inflate(R.layout.activity_base, null);
        contentScreen = drawer.findViewById(R.id.content_container);
        layoutInflater.inflate(layoutResID, contentScreen, true);
        super.setContentView(drawer);

        //Todo переделать лайоут тулбара под кастомный
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_button_menu);

        final NavigationView navigationView = drawer.findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                drawer.closeDrawers();
                //drawer.closeDrawer(GravityCompat.START);
                switch (item.getItemId()) {
                    case R.id.nav_menu_words:
                        navigator.openWordsBrowserActivity();
                        return true;
                    case R.id.nav_menu_random_words:
                        navigator.openRandomWordsActivity();
                        return true;
                    case R.id.nav_menu_words_training:
                        navigator.openWordsTrainingActivity();
                        return true;
                    case R.id.nav_menu_user_words_sources:
                        navigator.openUserWordsSourcesActivity();
                        return true;
                    case R.id.nav_menu_dictionaries:
                        navigator.openDictionariesActivity();
                        return true;
                    case R.id.nav_menu_statistic:
                        navigator.openStatisticActivity();
                        return true;
                    case R.id.nav_menu_settings:
                        navigator.openSettingsActivity();
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawer.openDrawer(GravityCompat.START);
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (presenter != null) {
            presenter.onAttachView(getThisMvpView());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (presenter != null) {
            presenter.onDetachView();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LexiconatorApplication.getInstance().clearActivityComponent();
    }

    @Override
    public Navigator getNavigator() {
        return navigator;
    }

    @Override
    public V showContentScreen(){
        contentScreen.setVisibility(View.VISIBLE);
        return getThisMvpView();
    }

    @Override
    public V hideContentScreen() {
        contentScreen.setVisibility(View.INVISIBLE);
        return getThisMvpView();
    }

    @Override
    public V showLoadingScreen() {
        loadingScreen.setVisibility(View.VISIBLE);
        return getThisMvpView();
    }

    @Override
    public V hideLoadingScreen() {
        loadingScreen.setVisibility(View.INVISIBLE);
        return getThisMvpView();
    }

    @Override
    public V showMessageScreen(CharSequence message) {
        screenMessageTextView.setText(message);
        screenMessageTextView.setMovementMethod(LinkMovementMethod.getInstance());
        messageScreen.setVisibility(View.VISIBLE);
        return getThisMvpView();
    }

    @Override
    public V hideMessageScreen() {
        messageScreen.setVisibility(View.INVISIBLE);
        return getThisMvpView();
    }

    protected abstract int getLayout();

    protected abstract V getThisMvpView();

    protected Toolbar getToolbar() {
        return toolbar;
    }

    protected P getPresenter() {
        return presenter;
    }

    @Inject
    protected void setPresenter(P presenter) {
        this.presenter = presenter;
    }

    protected ActivityComponent getActivityComponent() {
        return LexiconatorApplication.getInstance().getActivityComponent();
    }
}

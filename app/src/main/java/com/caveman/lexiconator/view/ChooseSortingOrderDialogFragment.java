package com.caveman.lexiconator.view;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.caveman.lexiconator.R;
import com.caveman.lexiconator.model.SortColumn;
import com.caveman.lexiconator.model.SortDirection;
import com.caveman.lexiconator.model.SortingOrder;
import com.caveman.lexiconator.presenter.ChooseSortingOrderPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ChooseSortingOrderDialogFragment
        extends BaseFragment<ChooseSortingOrderView, ChooseSortingOrderPresenter>
        implements ChooseSortingOrderView {

    @BindView(R.id.sort_group_column) RadioGroup columnGroup;
    @BindView(R.id.sort_alphabet) RadioButton wordRadioButton;
    @BindView(R.id.sort_date) RadioButton addTimeRadioButton;
    @BindView(R.id.sort_priority) RadioButton priorityRadioButton;
    @BindView(R.id.sort_group_direction) RadioGroup directionGroup;
    @BindView(R.id.sort_asc) RadioButton sortColumnRadioButton;
    @BindView(R.id.sort_desc) RadioButton sortDirectionRadioButton;
    private Unbinder unbinder;

    interface ChooseSortingOrderListener {
        void onChooseSortingOrder(SortingOrder sortingOrder);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentComponent().inject(this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_sorting_order, null);
        unbinder = ButterKnife.bind(this, view);

        getPresenter().onAttachView(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_title_sorting)
                .setView(view)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        getPresenter().onChooseSortingOrder(getSortingOrder());
                    }
                })
                .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getPresenter().onDetachView();
        unbinder.unbind();
    }

    @Override
    public void updateView(SortingOrder currentSortingOrder) {
        initCurrentSorting(currentSortingOrder);
    }

    @Override
    public void notifyListener(SortingOrder sortingOrder) {
        Fragment fragment = getParentFragment();
        if(fragment instanceof ChooseSortingOrderListener) {
            ((ChooseSortingOrderListener) fragment).onChooseSortingOrder(sortingOrder);
        }
    }

    private void initCurrentSorting(SortingOrder currentSortingOrder) {
        switch (currentSortingOrder.getSortColumn()) {
            case WORD:
                wordRadioButton.toggle();
                break;
            case ADD_TIMESTAMP:
                addTimeRadioButton.toggle();
                break;
            case PRIORITY:
                priorityRadioButton.toggle();
                break;
            default:
                wordRadioButton.toggle();
                break;
        }

        switch (currentSortingOrder.getSortDirection()) {
            case ASCENDANT:
                sortColumnRadioButton.toggle();
                break;
            case DESCENDANT:
                sortDirectionRadioButton.toggle();
                break;
            default:
                sortColumnRadioButton.toggle();
                break;
        }
    }

    private SortingOrder getSortingOrder() {
        SortColumn sortColumn;
        switch (columnGroup.getCheckedRadioButtonId()) {
            case R.id.sort_alphabet:
                sortColumn = SortColumn.WORD;
                break;
            case R.id.sort_date:
                sortColumn = SortColumn.ADD_TIMESTAMP;
                break;
            case R.id.sort_priority:
                sortColumn = SortColumn.PRIORITY;
                break;
            default:
                sortColumn = SortColumn.WORD;
                break;
        }

        SortDirection sortDirection;
        switch (directionGroup.getCheckedRadioButtonId()) {
            case R.id.sort_asc:
                sortDirection = SortDirection.ASCENDANT;
                break;
            case R.id.sort_desc:
                sortDirection = SortDirection.DESCENDANT;
                break;
            default:
                sortDirection = SortDirection.ASCENDANT;
                break;
        }

        return new SortingOrder(sortColumn, sortDirection);
    }
}

package com.caveman.lexiconator.view;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.caveman.lexiconator.presenter.MvpChooseWordsSourcePresenter;

public abstract class BaseChooseUserWordsSourceActivity <
        V extends MvpChooseWordsSourceView,
        P extends MvpChooseWordsSourcePresenter<V, UserWordsSourceMetadata>>
        extends BaseChooseWordsSourceActivity<V, P, UserWordsSourceMetadata>
        implements ChooseUserWordsSourceDialogFragment.ChooseUserWordsSourceListener {

    @Override
    public void showChooseWordsSourceDialog() {
        ChooseUserWordsSourceDialogFragment chooseUserWordsSourceDialogFragment =
                new ChooseUserWordsSourceDialogFragment();
        chooseUserWordsSourceDialogFragment.show(getSupportFragmentManager(), "ChooseUserWordsSourceDialog");
    }

    @Override
    public void onChooseUserWordsSource(long itemId, UserWordsSourceMetadata userWordsSourceMetadata) {
        getPresenter().onChooseWordsSource(userWordsSourceMetadata);
    }

    @Override
    public void onRedirectToUserWordsSources() {
        getNavigator().openUserWordsSourcesActivity();
    };
}

package com.caveman.lexiconator.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.caveman.lexiconator.R;
import com.caveman.lexiconator.model.UserWordsMinMaxPriorityData;
import com.caveman.lexiconator.model.room.UserWordMetadata;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class UserWordsRecyclerViewAdapter extends RecyclerView.Adapter<UserWordsRecyclerViewAdapter.WordViewHolder>{

    private List<UserWordMetadata> userWordMetadataList;
    private UserWordsMinMaxPriorityData userWordsMinMaxPriorityData;
    private ItemClickViewListener<UserWordMetadata> itemClickViewListener;
    private Set<WordViewHolder> viewHolders;

    public UserWordsRecyclerViewAdapter(ItemClickViewListener<UserWordMetadata> itemClickViewListener) {
        this.itemClickViewListener = itemClickViewListener;
        this.viewHolders = new HashSet<>();
    }

    @NonNull
    @Override
    public WordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View wordItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_word, parent, false);
        WordViewHolder viewHolder = new WordViewHolder(wordItemView, itemClickViewListener);
        viewHolders.add(viewHolder);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull WordViewHolder holder, int position) {
        UserWordMetadata userWordMetadata = userWordMetadataList.get(position);
        holder.bind(userWordMetadata, userWordsMinMaxPriorityData);
    }

    @Override
    public int getItemCount() {
        return userWordMetadataList != null ? userWordMetadataList.size() : 0;
    }

    public void setWords(List<UserWordMetadata> userWordMetadataList, UserWordsMinMaxPriorityData userWordsMinMaxPriorityData) {
        this.userWordMetadataList = userWordMetadataList;
        this.userWordsMinMaxPriorityData = userWordsMinMaxPriorityData;
        setUserWordsMinMaxPriority(userWordsMinMaxPriorityData);
        notifyDataSetChanged();
    }

    public void setUserWordsMinMaxPriority(UserWordsMinMaxPriorityData userWordsMinMaxPriorityData) {
        for (WordViewHolder viewHolder : viewHolders) {
            viewHolder.updateWordsPriority(userWordsMinMaxPriorityData);
        }
    }

    public void clearWords() {
        userWordMetadataList = null;
        notifyDataSetChanged();
    }

    public static class WordViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.word) TextView wordView;
        @BindView(R.id.add_timestamp) TextView addTimestampView;
        @BindView(R.id.priority) ProgressBar priority;
        private ItemClickViewListener<UserWordMetadata> itemClickListener;
        private UserWordMetadata userWordMetadata;
        private UserWordsMinMaxPriorityData userWordsMinMaxPriorityData;

        public WordViewHolder(View wordItemView, ItemClickViewListener<UserWordMetadata> itemClickListener) {
            super(wordItemView);
            ButterKnife.bind(this, itemView);
            this.itemClickListener = itemClickListener;
        }

        @OnLongClick()
        public boolean onLongClick(View view) {
            itemClickListener.onItemLongClick(view, userWordMetadata);
            return true;
        }

        @OnClick()
        public void onClick (View view) {
            itemClickListener.onItemClick(view, userWordMetadata);
        }

        public void bind(UserWordMetadata userWordMetadata, UserWordsMinMaxPriorityData userWordsMinMaxPriorityData) {
            wordView.setText(userWordMetadata.getWord());
            DateFormat dateFormat = SimpleDateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
            addTimestampView.setText(dateFormat.format(new Date(userWordMetadata.getAddTimestamp() * 1000L)));
            this.userWordMetadata = userWordMetadata;
            this.userWordsMinMaxPriorityData = userWordsMinMaxPriorityData;
            updateWordsPriority(userWordsMinMaxPriorityData);
        }

        public void updateWordsPriority(UserWordsMinMaxPriorityData userWordsMinMaxPriorityData) {
            int progressValueOffset = 1 - userWordsMinMaxPriorityData.getMinPriority();
            priority.setMax(userWordsMinMaxPriorityData.getMaxPriority() + progressValueOffset);
            priority.setProgress((userWordMetadata.getPriority() + progressValueOffset));
        }
    }
}

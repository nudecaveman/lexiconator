package com.caveman.lexiconator.view;

import com.caveman.lexiconator.model.UserWordsMinMaxPriorityData;
import com.caveman.lexiconator.model.room.UserWordMetadata;

import java.util.List;

public interface UserWordsSourceContentView extends WordsSourceContentView {

    void showCannotOpenWordCardToast();

    void showWords(List<UserWordMetadata> userWordMetadataList, UserWordsMinMaxPriorityData userWordsMinMaxPriorityData);

    void showAddWordDialog();

    void showEditWordDialog(String word, long wordId);
}

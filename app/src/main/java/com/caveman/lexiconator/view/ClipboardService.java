package com.caveman.lexiconator.view;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.TaskStackBuilder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.caveman.lexiconator.BuildConfig;
import com.caveman.lexiconator.LexiconatorApplication;
import com.caveman.lexiconator.R;
import com.caveman.lexiconator.presenter.ClipboardPresenter;

import javax.inject.Inject;

import static android.content.ClipDescription.MIMETYPE_TEXT_HTML;
import static android.content.ClipDescription.MIMETYPE_TEXT_PLAIN;
import static android.view.WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
import static android.view.WindowManager.LayoutParams.TYPE_PHONE;

public class ClipboardService extends Service implements ClipboardView {

    public static final int NOTIFICATION_ID = 1000;
    public static final String ACTION_STOP_SERVICE = BuildConfig.APPLICATION_ID + ".ACTION_STOP_SERVICE";
    private WindowManager windowManager;
    private View overlayView;
    private ImageButton overlayButton;
    private View wordDataView;
    private ImageButton closeButton;
    private TextView wordMeaningTextView;
    private ClipboardManager clipboard;
    @Inject ClipboardPresenter clipboardPresenter;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LexiconatorApplication.getInstance().getServiceComponent().inject(this);
        //Todo api 26+ https://github.com/mindbirth/snap-services найти способ обойти ограничение на запруск служб в бекграунде

        LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);

        overlayView = inflater.inflate(R.layout.clipboard_layout, null);

        overlayButton = overlayView.findViewById(R.id.overlay_button);
        overlayButton.setVisibility(View.GONE);
        overlayButton.setOnClickListener(getOverlayButtonOnClickListener());

        wordDataView = overlayView.findViewById(R.id.overlay_word_data_view);
        wordDataView.setVisibility(View.GONE);

        closeButton = overlayView.findViewById(R.id.overlay_close_button);
        closeButton.setOnClickListener(v -> wordDataView.setVisibility(View.GONE));

        wordMeaningTextView = overlayView.findViewById(R.id.overlay_word_meaning);

        clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.addPrimaryClipChangedListener(getClipBoardPrimaryClipChangedListener());

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        windowManager.addView(overlayView, getWindowLayoutParams());

        clipboardPresenter.onAttachView(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        String action = intent.getAction();
        if (action != null && action.equals(ACTION_STOP_SERVICE)) {
            clipboardPresenter.onCloseButtonClick();
        }
        else {
            clipboardPresenter.onShowClipboardNotification();
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        clipboardPresenter.onDetachView();
        windowManager.removeView(overlayView);
        LexiconatorApplication.getInstance().clearServiceComponent();
    }

    @Override
    public void showClipboardNotification(
            boolean isClipBoardWordsAddIsActive, String userWordsAddWordsSourceName,
            boolean isClipBoardTranslateIsActive, String translateSourceLanguage, String translateTargetLanguage) {

        //Todo для оперативного отслеживания изменений в настройках службы и их отображения в уведомлении нужно сделать что-то вроде event bus'a

        startForeground(NOTIFICATION_ID, getForegroundNotification(
                isClipBoardWordsAddIsActive, userWordsAddWordsSourceName,
                isClipBoardTranslateIsActive, translateSourceLanguage, translateTargetLanguage)
        );
    }

    @Override
    public void showAddedWordsCountToast(String userWordsSourceName, int addedWordsCount) {
        String toastMassage = getString(R.string.label_words_added_in_list) +
                              " " + userWordsSourceName + ": " + addedWordsCount;
        Toast toast = Toast.makeText(getApplicationContext(), toastMassage, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void showWordDefinition(String definition) {
        wordDataView.setVisibility(View.VISIBLE);
        wordMeaningTextView.setText(definition);
    }

    @Override
    public void close() {
        stopSelf();
    }

    private Notification getForegroundNotification(
            boolean isClipBoardWordsAddIsActive, String userWordsAddWordsSourceName,
            boolean isClipBoardTranslateIsActive, String translateSourceLanguage, String translateTargetLanguage){

        Intent openPreferencesIntent = new Intent(getApplicationContext(), SettingsActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        stackBuilder.addNextIntentWithParentStack(openPreferencesIntent);
        PendingIntent openSettingsPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent closeIntent = new Intent(this, ClipboardService.class);
        closeIntent.setAction(ACTION_STOP_SERVICE);
        PendingIntent closePendingIntent = PendingIntent.getService(this, 0, closeIntent, 0);

        return new Notification.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.pref_clipboard_group_service_title))
                .setContentIntent(openSettingsPendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .addAction(
                        R.drawable.ic_close,
                        getString(R.string.clipboard_service_notification_disable_button_text),
                        closePendingIntent)
                .build();
    }

    private View.OnClickListener getOverlayButtonOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clipboard.hasPrimaryClip()) {
                    if (clipboard.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_PLAIN) ||
                        clipboard.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_HTML)) {
                        ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
                        CharSequence pasteData = item.getText();
                        if (pasteData != null) {
                            overlayButton.setVisibility(View.GONE);
                            clipboardPresenter.onTextCapture(pasteData.toString());
                        }
                    }
                }
            }
        };
    }

    private ClipboardManager.OnPrimaryClipChangedListener getClipBoardPrimaryClipChangedListener() {
        final Handler handler = new Handler();
        final Runnable visibilityTime = () -> {
            //тоже срабатывает два раза. Почему и как фиксить?
            if (overlayButton.isShown()) {
                overlayButton.setVisibility(View.GONE);
                Animation buttonPopdownAnimation = AnimationUtils.loadAnimation(
                        ClipboardService.this, R.anim.overlay_button_popdown);
                overlayButton.startAnimation(buttonPopdownAnimation);
            }
        };

        return new ClipboardManager.OnPrimaryClipChangedListener() {
            @Override
            public void onPrimaryClipChanged() {
                //срабатывает два раза в Genymotion. Почему и как фиксить?
                if (clipboard.hasPrimaryClip() && (
                        clipboard.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_PLAIN) ||
                        clipboard.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_HTML)
                )) {
                    overlayButton.setVisibility(View.VISIBLE);
                    handler.postDelayed(visibilityTime, 5000);
                    Animation buttonPopupAnimation = AnimationUtils.loadAnimation(
                            ClipboardService.this, R.anim.overlay_button_popup);
                    overlayButton.startAnimation(buttonPopupAnimation);
                }
            }
        };
    }

    private WindowManager.LayoutParams getWindowLayoutParams() {
        int windowType = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? TYPE_APPLICATION_OVERLAY : TYPE_PHONE;
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                windowType,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.END;
        params.x = 0;
        params.y = 100;
        return params;
    }
}

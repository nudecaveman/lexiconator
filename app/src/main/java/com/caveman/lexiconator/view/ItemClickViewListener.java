package com.caveman.lexiconator.view;

import android.view.View;

public interface ItemClickViewListener<T> {

    void onItemClick(View view, T value);

    void onItemLongClick(View view, T value);
}

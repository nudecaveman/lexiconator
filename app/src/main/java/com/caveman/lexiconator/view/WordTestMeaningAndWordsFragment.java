package com.caveman.lexiconator.view;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.caveman.lexiconator.R;
import com.caveman.lexiconator.model.WordData;
import com.caveman.lexiconator.model.WordTestData;
import com.caveman.lexiconator.presenter.WordTestPresenter;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.Unbinder;

import static android.graphics.Color.GREEN;
import static android.graphics.Color.RED;

public class WordTestMeaningAndWordsFragment extends BaseFragment<WordTestView, WordTestPresenter> implements WordTestView {

    public static final String ARG_WORD = "ARG_WORD";
    public static final String ARG_USER_WORDS_SOURCE_ID = "ARG_WORDS_SOURCE_ID";

    @BindView(R.id.card_test) CardView cardView;
    @BindView(R.id.word_image) ImageView wordImage;
    @BindView(R.id.question) TextView questionView;
    @BindView(R.id.answer_1) RadioButton answerView_1;
    @BindView(R.id.answer_2) RadioButton answerView_2;
    @BindView(R.id.answer_3) RadioButton answerView_3;
    @BindView(R.id.answer_4) RadioButton answerView_4;
    private Unbinder unbinder;
    private RadioButton[] answersViews;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_word_test_meaning_and_words, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        answersViews = new RadioButton[]{answerView_1, answerView_2, answerView_3, answerView_4};
        cardView.setVisibility(View.INVISIBLE
        );
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getPresenter().onAttachView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().onDetachView();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getActivity() != null) {
            if (isVisibleToUser) {
                startFadeInAnimation();
            } else {
                cardView.setVisibility(View.INVISIBLE);
            }
        }
    }

    @OnCheckedChanged({R.id.answer_1, R.id.answer_2, R.id.answer_3, R.id.answer_4})
    public void onRadioButtonCheckChanged(CompoundButton answerView, boolean isChecked) {
        if (isChecked) {
            getPresenter().onAnswerSelected(answerView.getId());
        }
    }

    @Override
    public void updateView(WordTestData wordTestData, boolean isHtml) {
        CharSequence meaning = wordTestData.getQuestionData().getMeaning();
        if (isHtml) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                meaning = Html.fromHtml(wordTestData.getQuestionData().getMeaning(), Html.FROM_HTML_MODE_COMPACT);
            } else {
                meaning = Html.fromHtml(wordTestData.getQuestionData().getMeaning());
            }
        }

        questionView.setText(meaning, TextView.BufferType.SPANNABLE);

        Map<WordData, Integer> answerIdBindings = new HashMap<>();
        for (int index = 0; index < 4; index++) {
            WordData wordData = wordTestData.getAnswersVariants().get(index);

            RadioButton answerView = answersViews[index];

            answerView.setText(wordData.getWord());
            answerIdBindings.put(wordData, answerView.getId());
        }

        getPresenter().onAnswersIdBindingsInit(answerIdBindings);

        if(getUserVisibleHint()) {
            startFadeInAnimation();
        }
    }

    @Override
    public void showAnswersWithColors(int currentAnswerId, int correctAnswerId) {
        for(RadioButton answerRadioButton : answersViews) {
            if (answerRadioButton.getId() == currentAnswerId) {
                answerRadioButton.setBackgroundColor(RED);
            }
            if (answerRadioButton.getId() == correctAnswerId){
                answerRadioButton.setBackgroundColor(GREEN);
            }
        }
    }

    @Override
    public void notifyTestPassListener() {
        Activity activity = getActivity();
        if (activity instanceof WordsTrainingView) {
            ((WordsTrainingView) activity).onTestPass();
        }
    }

    @Override
    public void disableVariants() {
        for (RadioButton radioButton : answersViews) {
            radioButton.setClickable(false);
        }
    }

    @Override
    public long getWordSourceIdArg() {
        long dictionarySourceIdArg = -1;
        Bundle bundle = getArguments();
        if(bundle != null) {
            dictionarySourceIdArg = bundle.getLong(ARG_USER_WORDS_SOURCE_ID);
        }
        return dictionarySourceIdArg;
    }

    @Override
    public String getWordArg() {
        String wordArg = null;
        Bundle bundle = getArguments();
        if(bundle != null) {
            wordArg = bundle.getString(ARG_WORD);
        }
        return wordArg;
    }

    private void startFadeInAnimation() {
        Animation fadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        fadeIn.setDuration(500);
        fadeIn.setFillAfter(true);
        cardView.startAnimation(fadeIn);
    }

    public static WordTestMeaningAndWordsFragment createNew(String word, long userWordsSourceId) {
        Bundle args = new Bundle();
        args.putString(ARG_WORD, word);
        args.putLong(ARG_USER_WORDS_SOURCE_ID, userWordsSourceId);
        WordTestMeaningAndWordsFragment wordTestFragment = new WordTestMeaningAndWordsFragment();
        wordTestFragment.setArguments(args);
        return wordTestFragment;
    }

}
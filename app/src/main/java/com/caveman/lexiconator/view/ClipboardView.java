package com.caveman.lexiconator.view;

public interface ClipboardView extends MvpView {

    void showClipboardNotification(
            boolean isClipBoardWordsAddIsActive, String userWordsAddWordsSourceName,
            boolean isClipBoardTranslateIsActive, String translateSourceLanguage, String translateTargetLanguage);

    void showAddedWordsCountToast(String userWordsSourceName, int addedWordsCount);

    void showWordDefinition(String definition);

    void close();
}

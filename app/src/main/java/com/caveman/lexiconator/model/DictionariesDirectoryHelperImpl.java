package com.caveman.lexiconator.model;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DictionariesDirectoryHelperImpl implements DictionariesDirectoryHelper {

    private Context context;
    private PreferencesHelper preferencesHelper;
    private File dictionariesDefaultDirectory;
    private File dictionariesCustomDirectory;

    public DictionariesDirectoryHelperImpl(Context context, File dictionariesDefaultDirectory, PreferencesHelper preferencesHelper) {
        this.context = context;
        this.dictionariesDefaultDirectory = dictionariesDefaultDirectory;
        this.preferencesHelper = preferencesHelper;

        String dictionariesCustomDirectoryPath = preferencesHelper.getDictionariesCustomDirectoryPath();
        if (dictionariesCustomDirectoryPath != null) {
            dictionariesCustomDirectory = new File(dictionariesCustomDirectoryPath);
            if (!dictionariesCustomDirectory.exists()) {
                dictionariesCustomDirectory = null;
                preferencesHelper.setDictionariesCustomDirectoryPath(null);
            }
        }
    }

    @Override
    public void createDefaultDictionariesIfFirstRun() {
        // Todo как создать папку для пользователей с N версии?
        //Todo крашится на больших словарях вроде такого http://downloads.sourceforge.net/xdxf/stardict-comn_sdict05_eng_rus_full-2.4.2.tar.bz2

        if (preferencesHelper.isFirstRun()) {
            createDefaultDictionariesInDirectory(dictionariesDefaultDirectory);
        }
    }

    @Override
    public File getDictionariesDefaultDirectory() {
        return dictionariesDefaultDirectory;
    }

    @Override
    public void setDictionariesDefaultDirectory(File dictionariesDefaultDirectory) {
        this.dictionariesDefaultDirectory = dictionariesDefaultDirectory;
    }


    @Override
    public File getDictionariesCustomDirectory() {
        return dictionariesCustomDirectory;
    }

    @Override
    public void setDictionariesCustomDirectory(File dictionariesCustomDirectory) {
        this.dictionariesCustomDirectory = dictionariesCustomDirectory;
    }

    private void createDefaultDictionariesInDirectory(File directory) {
        try {
            String assetsPath = "default_dictionaries";
            for (String fileName : context.getAssets().list(assetsPath)) {
                File targetFile = new File(directory, fileName);

                if (!targetFile.exists()) {
                    InputStream inputStream = context.getAssets().open(assetsPath + "/" + fileName);
                    byte[] buffer = new byte[inputStream.available()];
                    inputStream.read(buffer);

                    OutputStream outStream = new FileOutputStream(targetFile);
                    outStream.write(buffer);
                    outStream.flush();
                    outStream.close();

                    if (!targetFile.exists()) {
                        throw new RuntimeException("Couldn't create file: " + targetFile);
                    }
                }
            }
            preferencesHelper.setIsFirstRun(false);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

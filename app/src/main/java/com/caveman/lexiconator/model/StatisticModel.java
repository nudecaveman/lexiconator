package com.caveman.lexiconator.model;

import java.util.Date;
import java.util.Map;

import io.reactivex.Single;

public interface StatisticModel extends CurrentUserWordsSourceFacade {

    Single<Map<Date, Integer>> getAddedWordsInDayStatistic();

    Single<Integer> getWordsCountStatistic();

    Date getStartDate();

    Date getEndDate();

    void setStartDate(Date startDate);

    void setEndDate(Date endDate);
}

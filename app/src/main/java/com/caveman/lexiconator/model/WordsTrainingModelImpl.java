package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordMetadata;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.reactivex.Single;

public class WordsTrainingModelImpl extends BaseCurrentUserWordsSourceFacade implements WordsTrainingModel {

    private CurrentDictionariesHelper currentDictionariesHelper;
    private PreferencesHelper preferencesHelper;

    public WordsTrainingModelImpl(
            UserWordsSourcesHelper userWordsSourcesHelper,
            CurrentUserWordsSourcesRepository currentUserWordsSourcesRepository,
            CurrentDictionariesHelper currentDictionariesHelper, PreferencesHelper preferencesHelper) {
        super(userWordsSourcesHelper, currentUserWordsSourcesRepository);
        this.userWordsSourcesHelper = userWordsSourcesHelper;
        this.currentDictionariesHelper = currentDictionariesHelper;
        this.preferencesHelper = preferencesHelper;
    }

    @Override
    public Single<List<UserWordMetadata>> getUserWordsList() {
        return Single.fromCallable(() -> {

            List<UserWordMetadata> userWordMetaDataList = new ArrayList<>();
            UserWordsSource userWordsSource = getCurrentUserWordsSource();
            if(userWordsSource != null) {


                int maxCount = preferencesHelper.getWordDelayValue();

                /**
                 * It is extremely important to maintain the original words order of this list.
                 * These words are arranged in decreasing order of priority,
                 * which is necessary for the priority display of the newly added or poorly understood word.
                 */
                userWordMetaDataList = getCurrentUserWordsSource()
                        .getWordsMetadataList(new SortingOrder(SortColumn.PRIORITY, SortDirection.DESCENDANT));

                List<String> words = new ArrayList<>();

                Iterator<UserWordMetadata> wordsRowsIterator = userWordMetaDataList.iterator();
                while (wordsRowsIterator.hasNext() && words.size() != maxCount) {
                    UserWordMetadata userWordData = wordsRowsIterator.next();
                    words.add(userWordData.getWord());
                }

                List<String> existedWords = currentDictionariesHelper.getExistedFromWords(words, findCurrentUserWordsSourceMetadata());

                /**
                 * Remove all words that do not exist in the dictionaries,
                 * while maintaining the priority order of existing.
                 */
                words.retainAll(existedWords);

                Iterator<UserWordMetadata> wordsMetadataIterator = userWordMetaDataList.iterator();
                while (wordsMetadataIterator.hasNext() && words.size() != maxCount) {
                    UserWordMetadata userWordMetadata = wordsMetadataIterator.next();
                    if (!existedWords.contains(userWordMetadata.getWord())) {
                        wordsMetadataIterator.remove();
                    }
                }
            }
            return userWordMetaDataList;
        });
    }

    @Override
    public Single<Boolean> isHasCurrentDictionarySources() {
        return Single.fromCallable( () -> currentDictionariesHelper
                .isHaveCurrentEnabledDictionarySourcesFor(findCurrentUserWordsSourceMetadata()));
    }
}

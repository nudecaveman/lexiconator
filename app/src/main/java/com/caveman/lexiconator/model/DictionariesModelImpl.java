package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.StardictModel.StarDictDictionarySourceCreator;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Single;

public class DictionariesModelImpl implements DictionariesModel {

    private DictionariesDirectoryHelper dictionariesDirectoryHelper;
    private DictionarySourceCreator[] dictionarySourceCreators = {new StarDictDictionarySourceCreator()};
    private UserWordsSourcesHelper userWordsSourcesHelper;
    private CurrentDictionariesHelper currentDictionariesHelper;

    public DictionariesModelImpl(
            DictionariesDirectoryHelper dictionariesDirectoryHelper,
            UserWordsSourcesHelper userWordsSourcesHelper,
            CurrentDictionariesHelper currentDictionariesHelper
    ) {
        this.dictionariesDirectoryHelper = dictionariesDirectoryHelper;
        this.userWordsSourcesHelper = userWordsSourcesHelper;
        this.currentDictionariesHelper = currentDictionariesHelper;
    }

    @Override
    public Single<List<DictionarySourceMetadata>> scanDictionariesDirectories() {
        return Single.fromCallable( () -> {
            List<DictionarySourceMetadata> dictionarySourceMetadataList = scanDirectory(
                    dictionariesDirectoryHelper.getDictionariesDefaultDirectory()
            );
            File dictionariesCustomDirectory = dictionariesDirectoryHelper.getDictionariesCustomDirectory();
            if (dictionariesCustomDirectory != null) {
                dictionarySourceMetadataList.addAll(scanDirectory(dictionariesCustomDirectory));
            }
            return currentDictionariesHelper.saveDictionariesAndReturnActual(dictionarySourceMetadataList);
        });
    }

    @Override
    public Single<Map<Long, String>> getUserWordsSourceIdsAndBriefCodesMap() {
        /**
         * This map needs for display UserWordsSource to which a DictionarySource is bind
         */
        return Single.fromCallable( () -> {
            Map<Long, String> dictionariesIdsAndBriefCodesMap = new HashMap<>();
            for (UserWordsSourceMetadata userWordsSourceMetadata : userWordsSourcesHelper.getAllUserWordsSourcesMetadata()) {
                dictionariesIdsAndBriefCodesMap.put(userWordsSourceMetadata.getId(), userWordsSourceMetadata.getBriefCode());
            }
            return dictionariesIdsAndBriefCodesMap;
        });
    }

    private List<DictionarySourceMetadata> scanDirectory(File directory) {

        List<DictionarySourceMetadata> dictionarySourcesMetadataList = new ArrayList<>();

        /**
         * Creates the map with initialization groups.
         * Each key of this map is instance of DictionarySourceCreator for corresponding dictionary type.
         * Value for this key contain another map, each key of which is name of dictionary without extensions
         * and value for this key is list of files, which necessary for creating instance of DictionarySource of some type.
         */
        Map<DictionarySourceCreator, Map<String, List<File>>> dictionariesSourceCreatorsGroups = new HashMap<>();

        for (DictionarySourceCreator dictionarySourceCreator : dictionarySourceCreators) {
            dictionariesSourceCreatorsGroups.put(dictionarySourceCreator, new HashMap<>());
        }

        //Recursively scans dictionaries folder and group dictionaries files by types and names.
        for (File file : directory.listFiles()) {
            if (!file.isDirectory()) {
                groupIfDictionaryFile(dictionariesSourceCreatorsGroups, file);
            }
            else {
                dictionarySourcesMetadataList.addAll(scanDirectory(file));
            }
        }

        //Creates DictionarySource from grouped files with help of DictionarySourceCreator of corresponding type.
        for (Map.Entry<DictionarySourceCreator, Map<String, List<File>>> dictionarySourceCreatorEntry : dictionariesSourceCreatorsGroups.entrySet()) {
            DictionarySourceCreator dictionarySourceCreator = dictionarySourceCreatorEntry.getKey();
            Map<String, List<File>> dictionariesFilesGroups = dictionarySourceCreatorEntry.getValue();
            //Creates DictionarySource for each group of files
            for (List<File> files : dictionariesFilesGroups.values()) {
                DictionarySourceMetadata dictionarySourceMetadata = dictionarySourceCreator.createDictionaryRes(files);
                dictionarySourcesMetadataList.add(dictionarySourceMetadata);
            }
        }
        return dictionarySourcesMetadataList;
    }

    /**
     * Checks what DictionarySourceCreator the file belongs to, and groups this file by dictionary name
     */
    private void groupIfDictionaryFile(Map<DictionarySourceCreator, Map<String, List<File>>> dictionariesResCreatorsGroups, File file) {
        for (Map.Entry<DictionarySourceCreator, Map<String, List<File>>> dictionarySourceCreatorEntry : dictionariesResCreatorsGroups.entrySet()) {
            DictionarySourceCreator dictionarySourceCreator = dictionarySourceCreatorEntry.getKey();
            if (dictionarySourceCreator.isFileForThisCreator(file)) {
                Map<String, List<File>> dictionariesFilesGroups = dictionariesResCreatorsGroups.get(dictionarySourceCreator);
                String fileNameWithoutExtension = dictionarySourceCreator.getFileNameWithoutExtension(file);
                if (dictionariesFilesGroups.containsKey(fileNameWithoutExtension)) {
                    List<File> files = dictionariesFilesGroups.get(fileNameWithoutExtension);
                    files.add(file);
                }
                else {
                    List<File> files = new ArrayList<>();
                    files.add(file);
                    dictionariesFilesGroups.put(fileNameWithoutExtension, files);
                }
            }
        }
    }

    @Override
    public Completable deleteDictionary(DictionarySourceMetadata dictionarySourceMetadata) {
        return Completable.fromAction( () -> {
            currentDictionariesHelper.removeCurrentDictionary(dictionarySourceMetadata);

            for (File file : dictionarySourceMetadata.getDictionaryFiles()) {
                file.delete();
            }
        });
    }

    @Override
    public Completable bindDictionaryToUserWordsSource(long dictionarySourceId, UserWordsSourceMetadata userWordsSourceMetadata) {
        return Completable.fromAction( () -> {
            DictionarySourceMetadata dictionarySourceMetadata = currentDictionariesHelper
                    .getDictionarySourceMetadataById(dictionarySourceId);
            if (dictionarySourceMetadata != null) {
                if (userWordsSourceMetadata != null) {
                    dictionarySourceMetadata.setForUserWordsSourceId(userWordsSourceMetadata.getId());
                }
                else {
                    dictionarySourceMetadata.setForUserWordsSourceId(-1);
                }
                currentDictionariesHelper.updateCurrentDictionary(dictionarySourceMetadata);
            }
        });
    }

    @Override
    public Completable setDictionaryIsEnable(DictionarySourceMetadata dictionarySourceMetadata, boolean isEnabled) {
        return Completable.fromAction( () -> {
            dictionarySourceMetadata.setIsEnabled(isEnabled);
            currentDictionariesHelper.updateCurrentDictionary(dictionarySourceMetadata);
        });
    }
}

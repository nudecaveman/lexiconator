package com.caveman.lexiconator.model;

import com.caveman.lexiconator.Pair;

import io.reactivex.Maybe;
import io.reactivex.Single;

public class ClipboardModelImpl implements ClipboardModel {

    private UserWordsSourcesHelper userWordsSourcesHelper;
    private WordDataHelper wordDataHelper;

    public ClipboardModelImpl(UserWordsSourcesHelper userWordsSourcesHelper, WordDataHelper wordDataHelper) {
        this.userWordsSourcesHelper = userWordsSourcesHelper;
        this.wordDataHelper = wordDataHelper;
    }

    @Override
    public Maybe<Pair<String, Integer>> addWordsFromTextForUserWordsSource(String text, long userWordsSourceId) {
        return Maybe.defer( () -> {
            String userWordsSourceName = "";
            int addedWordsCount = 0;
            if (userWordsSourcesHelper.checkIfUserWordsSourceExists(userWordsSourceId)) {
                UserWordsSource userWordsSource = userWordsSourcesHelper.getUserWordsSourceById(userWordsSourceId);
                if (userWordsSource != null) {
                    userWordsSourceName = userWordsSource.getWordSourceMetadata().getName();
                    String[] words = text.split(" ");
                    for (String word : words) {
                        addedWordsCount += userWordsSource.addWord(word);
                    }
                }
                return Maybe.just(new Pair<>(userWordsSourceName, addedWordsCount));
            }
            else {
                return Maybe.empty();
            }
        });
    }

    @Override
    public Single<String> getTranslationFor(String text, String sourceLanguage, String targetLanguage) {
        return wordDataHelper.getWordData(text, sourceLanguage, targetLanguage)
                .map(WordData::getMeaning);
    }
}

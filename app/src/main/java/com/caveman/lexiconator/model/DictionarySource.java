package com.caveman.lexiconator.model;

import java.util.List;

public interface DictionarySource extends WordsSource<DictionarySourceMetadata> {

    WordData findWord(String word);

    List<DictionaryWordMetadata> getWordsMetadataList(SortDirection sortDirection);

    List<String> getRandomWords(int count, List<String> exceptWords);

    List<String> getExistedFromWords(List<String> words);

    MeaningProcessor getWordMeaningParser();
}

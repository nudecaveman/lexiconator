package com.caveman.lexiconator.model;

public class MeaningConfiguration {
    private boolean restrictMeaningsCount;
    private int maxMeaningsCount;
    private boolean meaningsOnNewLines;
    private boolean processTags;

    public MeaningConfiguration(boolean restrictMeaningsCount, int maxMeaningsCount, boolean meaningsOnNewLines, boolean processTags) {
        this.restrictMeaningsCount = restrictMeaningsCount;
        this.maxMeaningsCount = maxMeaningsCount;
        this.meaningsOnNewLines = meaningsOnNewLines;
        this.processTags = processTags;
    }

    public boolean isRestrictMeaningsCount() {
        return restrictMeaningsCount;
    }

    public void setRestrictMeaningsCount(boolean restrictMeaningsCount) {
        this.restrictMeaningsCount = restrictMeaningsCount;
    }

    public int getMaxMeaningsCount() {
        return maxMeaningsCount;
    }

    public void setMaxMeaningsCount(int maxMeaningsCount) {
        this.maxMeaningsCount = maxMeaningsCount;
    }

    public boolean isMeaningsOnNewLines() {
        return meaningsOnNewLines;
    }

    public void setMeaningsOnNewLines(boolean meaningsOnNewLines) {
        this.meaningsOnNewLines = meaningsOnNewLines;
    }

    public boolean isProcessTags() {
        return processTags;
    }

    public void setProcessTags(boolean processTags) {
        this.processTags = processTags;
    }
}

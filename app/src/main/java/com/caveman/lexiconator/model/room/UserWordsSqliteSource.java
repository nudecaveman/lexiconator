package com.caveman.lexiconator.model.room;

import android.arch.persistence.db.SimpleSQLiteQuery;

import com.caveman.lexiconator.model.SortingOrder;
import com.caveman.lexiconator.model.UserWordsMinMaxPriorityData;
import com.caveman.lexiconator.model.UserWordsSource;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import static com.caveman.lexiconator.model.WordsDbContract.WordsMetadataTable.COLUMN_NAME_USER_WORDS_SOURCE_ID;
import static com.caveman.lexiconator.model.WordsDbContract.WordsMetadataTable.TABLE_NAME;

public class UserWordsSqliteSource implements UserWordsSource {

    private UserWordsSourceMetadata userWordsSourceMetadata;
    private UserWordsSqliteSourcesDatabase db;

    public UserWordsSqliteSource(UserWordsSourceMetadata userWordsSourceMetadata, UserWordsSqliteSourcesDatabase db) {
        this.userWordsSourceMetadata = userWordsSourceMetadata;
        this.db = db;
    }

    @Override
    public UserWordMetadata getUserWordMetadataForWord(String word) {
        return db.userWordsDao().getWordMetadataForWord(userWordsSourceMetadata.getId(), word);
    }

    @Override
    public int addWord(String word) {
        int wordsAdded = 0;
        String wordForAdd = StringUtils.strip(StringUtils.trim(word)).toLowerCase();
        if (!db.userWordsDao().checkIfWordExist(wordForAdd, userWordsSourceMetadata.getId())) {
            wordsAdded++;
            long timestamp = System.currentTimeMillis() / 1000L;
            UserWordMetadata userWordMetadata = new UserWordMetadata(
                    wordForAdd,
                    timestamp,
                    0,
                    userWordsSourceMetadata.getId()
            );
            db.userWordsDao().addWord(userWordMetadata);
        }
        return wordsAdded;
    }

    @Override
    public void editWord(String word, long wordId) {
        String wordForUpdate = StringUtils.strip(StringUtils.trim(word)).toLowerCase();
        if (db.userWordsDao().checkIfWordExist(wordId)) {
            UserWordMetadata userWordMetadata = db.userWordsDao().getWordMetadataForWordId(wordId);
            userWordMetadata.setWord(wordForUpdate);
            db.userWordsDao().updateWord(userWordMetadata);
        }
    }

    @Override
    public void deleteWord(UserWordMetadata word) {
        db.userWordsDao().deleteWord(word);
    }

    @Override
    public void changeWordPriorityBy(UserWordMetadata word, int value) {
        word.setPriority(word.getPriority() + value);
        db.userWordsDao().updateWord(word);
    }

    @Override
    public UserWordsMinMaxPriorityData getWordsMinMaxPriority() {
        int minPriority = 0;
        int maxPriority = 0;
        if (userWordsSourceMetadata != null) {
            minPriority = db.userWordsDao().getWordsMinPriorityForSourceId(userWordsSourceMetadata.getId());
            maxPriority = db.userWordsDao().getWordsMaxPriorityForSourceId(userWordsSourceMetadata.getId());
        }
        return new UserWordsMinMaxPriorityData(minPriority, maxPriority);
    }

    @Override
    public UserWordsSourceMetadata getWordSourceMetadata() {
        return userWordsSourceMetadata;
    }

    @Override
    public List<UserWordMetadata> getWordsMetadataList(SortingOrder sortingOrder) {
        String query = String.format("SELECT * FROM %s WHERE %s = %s ORDER BY %s %s",
                                     TABLE_NAME,
                                     COLUMN_NAME_USER_WORDS_SOURCE_ID,
                                     userWordsSourceMetadata.getId(),
                                     sortingOrder.getSortColumn().getSortColumnString(),
                                     sortingOrder.getSortDirection().getSortDirectionString());
        return userWordsSourceMetadata != null ?
                db.userWordsDao().getWordsForSourceId(new SimpleSQLiteQuery(query)) :
                new ArrayList<>();
    }

    @Override
    public int getWordsCount() {
        return userWordsSourceMetadata != null ?
                db.userWordsDao().getWordsCountForSourceId(userWordsSourceMetadata.getId()) :
                0;
    }
}

package com.caveman.lexiconator.model;

import io.reactivex.Completable;
import io.reactivex.Maybe;

public interface WordsBrowserModel {

    Maybe<WordsSourceMetadata> getCurrentWordSourceMetadata();

    Completable setCurrentWordsSourceMetadata(WordsSourceMetadata wordsSourceMetadata);
}

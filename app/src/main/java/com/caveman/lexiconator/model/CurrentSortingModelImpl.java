package com.caveman.lexiconator.model;

import io.reactivex.Completable;
import io.reactivex.Single;

public class CurrentSortingModelImpl implements CurrentSortingModel {

    private CurrentSortingRepository currentSortingRepository;

    public CurrentSortingModelImpl(CurrentSortingRepository currentSortingRepository) {
        this.currentSortingRepository = currentSortingRepository;
    }

    @Override
    public Single<SortingOrder> getCurrentSortingOrder() {
        return Single.fromCallable( () -> currentSortingRepository.getCurrentUserWordsSourceSortingOrder());
    }

    @Override
    public Completable setCurrentSortingOrder(SortingOrder sortingOrder) {
        return Completable.fromAction( () -> currentSortingRepository.setCurrentUserWordsSourceSortingOrder(sortingOrder));
    }

    @Override
    public Single<SortDirection> getCurrentDictionarySourceSortDirection() {
        return Single.fromCallable(() -> currentSortingRepository.getCurrentDictionarySourceSortDirection());
    }

    @Override
    public Completable setCurrentDictionarySourceSortDirection(SortDirection sortDirection) {
        return Completable.fromAction(() -> currentSortingRepository.setCurrentDictionarySourceSortDirection(sortDirection));
    }
}

package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordMetadata;

import java.util.List;

public class WordTestData {

    private UserWordMetadata userWordMetadata;
    private WordData questionData;
    private List<WordData> answerVariants;

    public WordTestData (UserWordMetadata userWordMetadata, WordData questionData, List<WordData> answerVariants) {
        this.userWordMetadata = userWordMetadata;
        this.questionData = questionData;
        this.answerVariants = answerVariants;
    }

    public UserWordMetadata getUserWordMetadata() {
        return userWordMetadata;
    }

    public WordData getQuestionData() {
        return questionData;
    }

    public List<WordData> getAnswersVariants() {
        return answerVariants;
    }
}

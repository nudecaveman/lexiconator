package com.caveman.lexiconator.model;

public class WordData {
    private String word;
    private String meaning;

    private MeaningProcessor meaningProcessor;

    public WordData(String word, String meaning, MeaningProcessor meaningProcessor) {
        this.word = word;
        this.meaning = meaning;
        this.meaningProcessor = meaningProcessor;
    }

    public String getWord() {
        return word;
    }

    public String getMeaning() {
        return meaning;
    }

    public String getPronunation() {
        return meaning;
    }

    public Object getAudio() {
        return null;
    }

    public Object getImage() {
        return null;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    public void setPronunation(String pronunation) {
    }

    public void setAudio(Object audio) {
    }

    public void setImage(Object image) {
    }

    public MeaningProcessor getMeaningProcessor() {
        return meaningProcessor;
    }

    public void setMeaningProcessor(MeaningProcessor meaningProcessor) {
        this.meaningProcessor = meaningProcessor;
    }
}

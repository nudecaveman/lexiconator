package com.caveman.lexiconator.model.room;

import android.content.Context;

import com.caveman.lexiconator.model.WordsStatisticHelper;

import org.apache.commons.lang3.time.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public class WordsSqliteStatisticHelper implements WordsStatisticHelper {

    private UserWordsSqliteSourcesDatabase db;

    public WordsSqliteStatisticHelper(Context context) {
        db = UserWordsSqliteSourcesDatabase.getInstance(context);
    }

    @Override
    public Map<Date, Integer> getAddedWordsInDayStatistic(Date startDate, Date endDate, long userWordsSourceId) {
        Map<Date, Integer> addedWordsInDayStatistic = new TreeMap<>();

        Calendar startDateVar = Calendar.getInstance();
        startDateVar.setTimeInMillis(DateUtils.truncate(startDate, Calendar.DATE).getTime());

        Calendar endDateVar = Calendar.getInstance();
        endDateVar.setTimeInMillis(DateUtils.truncate(endDate, Calendar.DATE).getTime());
        endDateVar.add(Calendar.DATE, 1);

        for (; startDateVar.before(endDateVar); startDateVar.add(Calendar.DAY_OF_MONTH, 1)) {
            int count = getWordsAddByDay(startDateVar.getTime(), userWordsSourceId);
            addedWordsInDayStatistic.put(startDateVar.getTime(), count);
        }
        return addedWordsInDayStatistic;
    }

    @Override
    public int getWordsCountStatistic(Date startDate, Date endDate, long userWordsSourceId) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String startDateString = simpleDateFormat.format(startDate.getTime());
        String endDateString = simpleDateFormat.format(endDate.getTime());
        return db.userWordsStatisticDao().getAddedWordsCountForPeriod(startDateString, endDateString, userWordsSourceId);
    }

    private int getWordsAddByDay(Date dayDate, long userWordsSourceId) {
        //Todo проверить на баг. что если, локаль в бд и взятая по дефолту не совпадут?
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String dateString = simpleDateFormat.format(dayDate.getTime());
        return db.userWordsStatisticDao().getWordAddedInDay(dateString, userWordsSourceId);
    }
}

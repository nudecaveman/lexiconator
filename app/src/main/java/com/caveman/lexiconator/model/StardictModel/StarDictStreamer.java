package com.caveman.lexiconator.model.StardictModel;

import org.dict.zip.DictZipInputStream;
import org.dict.zip.RandomAccessInputStream;

import java.io.File;
import java.io.IOException;

public class StarDictStreamer {

    private RandomAccessInputStream dictExtensionStream;
    private DictZipInputStream dictDzExtensionStream;

    public StarDictStreamer(File file) {
        try {
            if (file.getName().endsWith(".dict")) {
                dictExtensionStream = new RandomAccessInputStream(file.getAbsolutePath(), "r");
            } else if (file.getName().endsWith(".dict.dz")) {
                dictDzExtensionStream = new DictZipInputStream(file.getAbsolutePath());
            } else {
                throw new IllegalStateException("The file does not have an 'dict' or 'dict.dz' extension");
            }
        }
        catch (IOException e) {
            throw new IllegalStateException("Can't read dictionary files ", e);
        }
    }

    public String seek(int startPos, int length) throws IOException {
        byte[] buf = new byte[length];
        if (dictExtensionStream != null) {
            dictExtensionStream.seek(startPos);
            dictExtensionStream.read(buf);
        }
        else {
            dictDzExtensionStream.seek(startPos);
            dictDzExtensionStream.read(buf);
        }
        return new String(buf, "utf-8");
    }
}

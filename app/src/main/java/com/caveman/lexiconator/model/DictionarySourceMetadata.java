package com.caveman.lexiconator.model;

import java.io.File;
import java.util.List;

public interface DictionarySourceMetadata extends WordsSourceMetadata {

    long getForUserWordsSourceId();

    void setForUserWordsSourceId(long forUserWordsSourceId);

    boolean isEnabled();

    void setIsEnabled(boolean isEnabled);

    boolean isAllDictionaryFilesExists();

    List<File> getDictionaryFiles();

    long getDictionaryFilesSize();

    DictionarySource createDictionarySource();
}

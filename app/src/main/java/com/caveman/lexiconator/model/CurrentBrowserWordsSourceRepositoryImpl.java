package com.caveman.lexiconator.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.caveman.lexiconator.GsonInterfaceAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class CurrentBrowserWordsSourceRepositoryImpl implements CurrentBrowserWordsSourceRepository {

    public static final String PREF_KEY_BROWSER_WORDS_SOURCE = "KEY_BROWSER_WORDS_SOURCE";
    private SharedPreferences sharedPreferences;
    private Type collectionType;
    private Gson gson;

    public CurrentBrowserWordsSourceRepositoryImpl(Context context) {
        sharedPreferences = context.getSharedPreferences("current_browser_words_source.pref", Context.MODE_PRIVATE);
        collectionType = new TypeToken<WordsSourceMetadata>(){}.getType();
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(WordsSourceMetadata.class, new GsonInterfaceAdapter<>());
        gson = builder.create();
    }

    @Override
    public WordsSourceMetadata getCurrentWordsSourceMetadata() {
        WordsSourceMetadata wordsSourceMetadata = null;
        String currentWordsSourceKeysString = sharedPreferences.getString(PREF_KEY_BROWSER_WORDS_SOURCE, null);
        if (currentWordsSourceKeysString != null) {
            wordsSourceMetadata = gson.fromJson(currentWordsSourceKeysString, collectionType);
        }
        return wordsSourceMetadata;
    }

    @Override
    public void setCurrentWordsSource(WordsSourceMetadata wordsSourceMetadata) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String serializableCurrentWordsSourceMetadata = gson.toJson(wordsSourceMetadata, collectionType);
        editor.putString(PREF_KEY_BROWSER_WORDS_SOURCE, serializableCurrentWordsSourceMetadata);
        editor.commit();
    }
}

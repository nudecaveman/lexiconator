package com.caveman.lexiconator.model.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

@Dao
public interface UserWordsStatisticDao {

    @Query("SELECT COUNT(*) FROM user_words_metadata WHERE date(add_timestamp, 'unixepoch') >= :startDateString AND date(add_timestamp, 'unixepoch') <= :endDateString AND user_words_source_id = :userWordsSourceId")
    int getAddedWordsCountForPeriod(String startDateString, String endDateString, long userWordsSourceId);

    @Query("SELECT COUNT(*) FROM user_words_metadata WHERE date(add_timestamp, 'unixepoch') = :dayDateString AND user_words_source_id = :userWordsSourceId")
    int getWordAddedInDay(String dayDateString, long userWordsSourceId);
}

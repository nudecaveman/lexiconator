package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.util.List;

public interface UserWordsSourcesHelper {

    boolean checkIfUserWordsSourceExists(long userWordsSourceId);

    UserWordsSourceMetadata getFirstUserWordsSourceMetadataFromExisted();

    UserWordsSource getUserWordsSource(UserWordsSourceMetadata userWordsSourceMetadata);

    UserWordsSource getUserWordsSourceById(long userWordsSourceId);

    List<UserWordsSourceMetadata> getAllUserWordsSourcesMetadata();

    UserWordsSourceMetadata getUserWordsSourceMetadataById(long userWordsSourceId);

    void addNewUserWordsSourceMetadata(String userWordsSourceName, String userWordsSourceBriefCode);

    void updateUserWordsSourceMetadata(UserWordsSourceMetadata userWordsSourceMetadata);

    void deleteUserWordsSourceMetadata(UserWordsSourceMetadata userWordsSourceMetadata);
}

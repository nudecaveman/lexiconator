package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.util.List;

public interface CurrentDictionariesHelper extends CurrentDictionariesRepository {

    List<WordData> findWord(String word, UserWordsSourceMetadata userWordsSourceMetadata);

    List<WordData> findWord(long dictionarySourceId, String word);

    List<String> getExistedFromWords(List<String> words, UserWordsSourceMetadata userWordsSourceMetadata);

    List<String> getRandomWords(int count, List<String> exceptWords, UserWordsSourceMetadata userWordsSourceMetadata);
}

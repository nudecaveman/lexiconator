package com.caveman.lexiconator.model.room;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import com.caveman.lexiconator.model.WordsSourceMetadata;

import io.reactivex.annotations.NonNull;

@Entity(tableName = "user_words_sources_metadata", indices = {@Index(value = {"_id", "name", "brief_code"},
        unique = true)})
public class UserWordsSourceMetadata implements WordsSourceMetadata {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "_id")
    private long id;

    @NonNull
    @ColumnInfo(name = "name")
    private String name;

    @NonNull
    @ColumnInfo(name = "brief_code")
    private String briefCode;

    @Ignore
    private int wordsCount;


    public UserWordsSourceMetadata(String name, String briefCode){
        this.name = name;
        this.briefCode = briefCode;
        this.wordsCount = 0;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getBriefCode() {
        return briefCode;
    }

    @Override
    public void setBriefCode(String briefCode) {
        this.briefCode = briefCode;
    }

    @Override
    public int getWordsCount() {
        return wordsCount;
    }

    @Override
    public void setWordsCount(int wordsCount) {
        this.wordsCount = wordsCount;
    }
}

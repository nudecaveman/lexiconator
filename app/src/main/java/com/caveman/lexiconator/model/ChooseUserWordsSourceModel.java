package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.util.List;

import io.reactivex.Single;

public interface ChooseUserWordsSourceModel {

    Single<List<UserWordsSourceMetadata>> getUserWordsSourceMetadataList();
}

package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.util.List;

public class WordsSourcesData {

    private List<UserWordsSourceMetadata> userWordsSourceMetadataList;
    private List<DictionarySourceMetadata> dictionarySourceMetadataList;

    public WordsSourcesData(
            List<UserWordsSourceMetadata> userWordsSourceMetadataList,
            List<DictionarySourceMetadata> dictionarySourceMetadataList) {
        this.userWordsSourceMetadataList = userWordsSourceMetadataList;
        this.dictionarySourceMetadataList = dictionarySourceMetadataList;
    }

    public List<UserWordsSourceMetadata> getUserWordsSourceMetadataList() {
        return userWordsSourceMetadataList;
    }

    public List<DictionarySourceMetadata> getDictionarySourceMetadataList() {
        return dictionarySourceMetadataList;
    }
}

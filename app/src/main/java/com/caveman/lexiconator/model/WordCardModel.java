package com.caveman.lexiconator.model;

import java.util.List;

import io.reactivex.Single;

public interface WordCardModel {

    Single<List<WordData>> findWordForUserWordsSource(long userWordsSourceId, String word, MeaningConfiguration meaningConfiguration);

    Single<List<WordData>> findWordFromDictionary(long dictionarySourceId, String word, MeaningConfiguration meaningConfiguration);
}

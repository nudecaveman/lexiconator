package com.caveman.lexiconator.model.room;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import com.caveman.lexiconator.model.WordMetadata;

import io.reactivex.annotations.NonNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;
import static com.caveman.lexiconator.model.WordsDbContract.WordsMetadataTable.COLUMN_NAME_ADD_TIMESTAMP;
import static com.caveman.lexiconator.model.WordsDbContract.WordsMetadataTable.COLUMN_NAME_ID;
import static com.caveman.lexiconator.model.WordsDbContract.WordsMetadataTable.COLUMN_NAME_PRIORITY;
import static com.caveman.lexiconator.model.WordsDbContract.WordsMetadataTable.COLUMN_NAME_USER_WORDS_SOURCE_ID;
import static com.caveman.lexiconator.model.WordsDbContract.WordsMetadataTable.COLUMN_NAME_WORD;
import static com.caveman.lexiconator.model.WordsDbContract.WordsMetadataTable.TABLE_NAME;

@Entity(tableName = TABLE_NAME,
        foreignKeys = @ForeignKey(
                entity = UserWordsSourceMetadata.class,
                parentColumns = COLUMN_NAME_ID,
                childColumns = COLUMN_NAME_USER_WORDS_SOURCE_ID,
                onDelete = CASCADE,
                onUpdate = CASCADE
        ),
        indices = {@Index(COLUMN_NAME_USER_WORDS_SOURCE_ID)}
)
public class UserWordMetadata implements WordMetadata {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = COLUMN_NAME_ID)
    private long id;

    @NonNull
    @ColumnInfo(name = COLUMN_NAME_WORD)
    private String word;

    @ColumnInfo(name = COLUMN_NAME_ADD_TIMESTAMP)
    private long addTimestamp;

    @ColumnInfo(name = COLUMN_NAME_PRIORITY)
    private int priority;

    @ColumnInfo(name = COLUMN_NAME_USER_WORDS_SOURCE_ID)
    private long userWordsSourceId;


    public UserWordMetadata(String word, long addTimestamp, int priority, long userWordsSourceId) {
        this.word = word;
        this.addTimestamp = addTimestamp;
        this.priority = priority;
        this.userWordsSourceId = userWordsSourceId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getWord() {
        return word;
    }

    @Override
    public void setWord(String word) {
        this.word = word;
    }

    public long getAddTimestamp() {
        return addTimestamp;
    }

    public void setAddTimestamp(long addTimestamp) {
        this.addTimestamp = addTimestamp;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public long getUserWordsSourceId() {
        return userWordsSourceId;
    }


    public void setUserWordsSourceId(long userWordsSourceId) {
        this.userWordsSourceId = userWordsSourceId;
    }
}

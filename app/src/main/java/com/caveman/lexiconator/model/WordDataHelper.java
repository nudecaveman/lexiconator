package com.caveman.lexiconator.model;

import io.reactivex.Single;

public interface WordDataHelper {
    Single<WordData> getWordData(String text, String sourceLanguage, String targetLanguage);
}

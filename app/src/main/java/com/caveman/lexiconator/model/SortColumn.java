package com.caveman.lexiconator.model;

import static com.caveman.lexiconator.model.WordsDbContract.WordsMetadataTable.COLUMN_NAME_ADD_TIMESTAMP;
import static com.caveman.lexiconator.model.WordsDbContract.WordsMetadataTable.COLUMN_NAME_PRIORITY;
import static com.caveman.lexiconator.model.WordsDbContract.WordsMetadataTable.COLUMN_NAME_WORD;

public enum SortColumn {
    WORD   (COLUMN_NAME_WORD),
    ADD_TIMESTAMP       (COLUMN_NAME_ADD_TIMESTAMP),
    PRIORITY  (COLUMN_NAME_PRIORITY);

    private final String sortColumnString;

    SortColumn(String sortColumnString) {
        this.sortColumnString = sortColumnString;
    }

    public String getSortColumnString() {
        return sortColumnString;
    }
}

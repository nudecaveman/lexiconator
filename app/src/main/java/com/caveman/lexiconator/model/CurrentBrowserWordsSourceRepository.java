package com.caveman.lexiconator.model;

public interface CurrentBrowserWordsSourceRepository {

    WordsSourceMetadata getCurrentWordsSourceMetadata();

    void setCurrentWordsSource(WordsSourceMetadata wordsSourceMetadata);
}

package com.caveman.lexiconator.model.StardictModel;

import com.caveman.lexiconator.model.DictionarySourceCreator;
import com.caveman.lexiconator.model.DictionarySourceMetadata;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class StarDictDictionarySourceCreator extends DictionarySourceCreator {

    public final static String EXTENSION_IFO = "ifo";
    public final static String EXTENSION_IDX = "idx";
    public final static String EXTENSION_DICT = "dict";
    public final static String EXTENSION_DICT_DZ = "dict.dz";


    public StarDictDictionarySourceCreator() {
        super(Arrays.asList(EXTENSION_IFO, EXTENSION_IDX, EXTENSION_DICT, EXTENSION_DICT_DZ));
    }

    @Override
    public DictionarySourceMetadata createDictionaryRes(List<File> dictionaryFiles) {
        File ifoFile = getFileByExtension(dictionaryFiles, EXTENSION_IFO);
        File idxFile = getFileByExtension(dictionaryFiles, EXTENSION_IDX);
        File dictDzFile = getFileByExtension(dictionaryFiles, EXTENSION_DICT_DZ);
        File dictFile = getFileByExtension(dictionaryFiles, EXTENSION_DICT);

        StarDictInfoReader infoReader = new StarDictInfoReader();
        infoReader.read(ifoFile);
        String name = infoReader.getName();
        int wordCount = infoReader.getWordsCount();
        StarDictDictionarySourceMetadata dictionarySourceMetadata = new StarDictDictionarySourceMetadata(
                ifoFile, idxFile, (dictDzFile != null) ? dictDzFile : dictFile,
                name, wordCount
        );

        return dictionarySourceMetadata;
    }
}

package com.caveman.lexiconator.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.caveman.lexiconator.GsonInterfaceAdapter;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CurrentDictionariesRepositoryImpl implements CurrentDictionariesRepository {

    public static final String PREF_KEY_CURRENT_DICTIONARIES = "PREF_KEY_CURRENT_DICTIONARIES";
    public static final String PREF_KEY_CURRENT_DICTIONARIES_MAX_ID = "PREF_KEY_CURRENT_DICTIONARIES_MAX_ID";
    private SharedPreferences sharedPreferences;
    private Map<DictionarySourceMetadata, DictionarySource> currentDictionarySourcesMap;
    private Type collectionType;
    private Gson gson;

    public CurrentDictionariesRepositoryImpl(Context context) {
        this.sharedPreferences = context.getSharedPreferences("current_dictionaries.pref", Context.MODE_PRIVATE);

        this.collectionType = new TypeToken<Set<DictionarySourceMetadata>>(){}.getType();
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(DictionarySourceMetadata.class, new GsonInterfaceAdapter<>());
        this.gson = builder.create();

        this.currentDictionarySourcesMap = getCurrentDictionarySourcesMap();
        clearFromNotExisted(currentDictionarySourcesMap);
        saveCurrentDictionarySourcesMap();
    }

    @Override
    public boolean checkIfDictionaryIsAvailable(long dictionarySourceId) {
        boolean result = false;
        for (DictionarySourceMetadata dictionarySourceMetadata : currentDictionarySourcesMap.keySet()) {
            if ((dictionarySourceMetadata.getId() == dictionarySourceId) && dictionarySourceMetadata.isEnabled()) {
                result = true;
            }
        }
        return result;
    }

    @Override
    public DictionarySourceMetadata getFirstDictionarySourceMetadataFromAvailable() {
        List<DictionarySourceMetadata> enabledDictionarySourceMetadataList = getEnabledDictionariesMetadataList();
        return enabledDictionarySourceMetadataList.size() > 0 ? enabledDictionarySourceMetadataList.get(0) : null;
    }

    @Override
    public DictionarySourceMetadata getDictionarySourceMetadataById(long dictionarySourceId) {
        DictionarySourceMetadata result = null;
        for (DictionarySourceMetadata dictionarySourceMetadata : currentDictionarySourcesMap.keySet()) {
            if ((dictionarySourceMetadata.getId() == dictionarySourceId) && dictionarySourceMetadata.isEnabled()) {
                result = dictionarySourceMetadata;
                break;
            }
        }
        return result;
    }

    @Override
    public List<DictionarySourceMetadata> getEnabledDictionariesMetadataList() {
        ArrayList<DictionarySourceMetadata> enabledDictionarySourceMetadataList = new ArrayList<>();
        for (DictionarySourceMetadata dictionarySourceMetadata : currentDictionarySourcesMap.keySet()) {
            if (dictionarySourceMetadata.isEnabled()) {
                enabledDictionarySourceMetadataList.add(dictionarySourceMetadata);
            }
        }
        return enabledDictionarySourceMetadataList;
    }

    @Override
    public List<DictionarySource> getEnabledDictionarySourcesFor(UserWordsSourceMetadata userWordsSourceMetadata) {
        List<DictionarySource> enabledDictionaries = new ArrayList<>();
        for (DictionarySourceMetadata dictionarySourceMetadata : currentDictionarySourcesMap.keySet()) {
            if (dictionarySourceMetadata.getForUserWordsSourceId() == userWordsSourceMetadata.getId() &&
                dictionarySourceMetadata.isEnabled()) {
                enabledDictionaries.add(getCurrentDictionarySourcesMap().get(dictionarySourceMetadata));
            }
        }
        return enabledDictionaries;
    }

    @Override
    public boolean isHaveCurrentEnabledDictionarySourcesFor(UserWordsSourceMetadata userWordsSourceMetadata) {
        boolean isHave = false;
        for (DictionarySourceMetadata dictionarySourceMetadata : getEnabledDictionariesMetadataList()) {
            if (dictionarySourceMetadata.getForUserWordsSourceId() == userWordsSourceMetadata.getId()) {
                isHave = true;
                break;
            }
        }
        return isHave;
    }

    @Override
    public DictionarySource getCurrentDictionarySourceById(long dictionaryWordsSourceId) {
        DictionarySource dictionarySourceResult = null;
        for (DictionarySourceMetadata dictionarySourceMetadata : currentDictionarySourcesMap.keySet()) {
            if (dictionarySourceMetadata.getId() == dictionaryWordsSourceId) {
                dictionarySourceResult = currentDictionarySourcesMap.get(dictionarySourceMetadata);
            }
        }
        return dictionarySourceResult;
    }

    @Override
    public List<DictionarySourceMetadata> saveDictionariesAndReturnActual(List<DictionarySourceMetadata> dictionarySourceMetadataList) {
        List<DictionarySourceMetadata> result = new ArrayList<>();
        for (DictionarySourceMetadata dictionarySourceArgMetadata : dictionarySourceMetadataList) {

            boolean exist = false;

            for (DictionarySourceMetadata dictionarySourceMetadata : currentDictionarySourcesMap.keySet()) {
                if (dictionarySourceMetadata.equals(dictionarySourceArgMetadata)) {
                    exist = true;
                    result.add(dictionarySourceMetadata);
                    break;
                }
            }

            if (!exist) {
                long newMaxId = getMaxId() + 1;
                dictionarySourceArgMetadata.setId(newMaxId);
                saveMaxId(newMaxId);
                currentDictionarySourcesMap.put(dictionarySourceArgMetadata, dictionarySourceArgMetadata.createDictionarySource());
                result.add(dictionarySourceArgMetadata);
            }
        }
        saveCurrentDictionarySourcesMap();
        return result;
    }

    @Override
    public void updateCurrentDictionary(DictionarySourceMetadata dictionarySourceMetadata) {
        for (DictionarySourceMetadata keyDictionarySourceMetadata : currentDictionarySourcesMap.keySet()) {
            if (keyDictionarySourceMetadata.equals(dictionarySourceMetadata)) {
                copyDictionarySourceMetadataFromTo(dictionarySourceMetadata, keyDictionarySourceMetadata);
                saveCurrentDictionarySourcesMap();
            }
        }
    }

    @Override
    public void removeCurrentDictionary(DictionarySourceMetadata dictionarySourceMetadata) {
        if (currentDictionarySourcesMap.keySet().contains(dictionarySourceMetadata)) {
            currentDictionarySourcesMap.remove(dictionarySourceMetadata);
            saveCurrentDictionarySourcesMap();
        }
    }

    @Override
    public void removeCurrentDictionary(DictionarySource dictionary) {
        removeCurrentDictionary(dictionary.getWordSourceMetadata());
    }

    private Map<DictionarySourceMetadata, DictionarySource> getCurrentDictionarySourcesMap() {
        if (currentDictionarySourcesMap == null) {
            currentDictionarySourcesMap = new HashMap<>();
            String currentDictionariesKeysString = sharedPreferences.getString(PREF_KEY_CURRENT_DICTIONARIES, null);
            if (currentDictionariesKeysString != null) {
                Set<DictionarySourceMetadata> currentDictionariesKeys = gson.fromJson(currentDictionariesKeysString, collectionType);
                for (DictionarySourceMetadata dictionarySourceMetadata : currentDictionariesKeys) {
                    currentDictionarySourcesMap.put(dictionarySourceMetadata, dictionarySourceMetadata.createDictionarySource());
                }
            }
        }
        return currentDictionarySourcesMap;
    }

    private void saveCurrentDictionarySourcesMap() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String serializableCurrentDictionaries = gson.toJson(currentDictionarySourcesMap.keySet(), collectionType);
        editor.putString(PREF_KEY_CURRENT_DICTIONARIES, serializableCurrentDictionaries);
        editor.commit();
    }

    private long getMaxId() {
        return sharedPreferences.getLong(PREF_KEY_CURRENT_DICTIONARIES_MAX_ID, 0);
    }

    private void saveMaxId(long maxId) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(PREF_KEY_CURRENT_DICTIONARIES_MAX_ID, maxId);
        editor.commit();
    }

    private void clearFromNotExisted(Map<DictionarySourceMetadata, DictionarySource> dictionariesMap) {
        Iterator<DictionarySourceMetadata> dictionarySourceMetadataIterator = dictionariesMap.keySet().iterator();
        while (dictionarySourceMetadataIterator.hasNext()) {
            DictionarySourceMetadata dictionarySourceMetadata = dictionarySourceMetadataIterator.next();
            if (!dictionarySourceMetadata.isAllDictionaryFilesExists()) {
                dictionarySourceMetadataIterator.remove();
            }
        }
    }

    private void copyDictionarySourceMetadataFromTo(
            DictionarySourceMetadata sourceDictionarySourceMetadata,
            DictionarySourceMetadata targetDictionarySourceMetadata
    ) {
        targetDictionarySourceMetadata.setId(sourceDictionarySourceMetadata.getId());
        targetDictionarySourceMetadata.setForUserWordsSourceId(sourceDictionarySourceMetadata.getForUserWordsSourceId());
        targetDictionarySourceMetadata.setIsEnabled(sourceDictionarySourceMetadata.isEnabled());
    }

}

package com.caveman.lexiconator.model.StardictModel;

import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StarDictInfoReader {

    private String name;
    private int wordsCount;

    public StarDictInfoReader() {
    }

    public boolean read(File ifoFile) {
        try {
            Scanner scanner = new Scanner(ifoFile);
            while (scanner.hasNextLine()) {
                String string = scanner.nextLine();
                if (string.startsWith("bookname")) {
                    name = getValue(string);
                }
                else if (string.startsWith("wordcount")) {
                    wordsCount = Integer.parseInt(getValue(string));
                }
            }
            scanner.close();
            return true;
        } catch (FileNotFoundException e) {
            return false;
        }
    }

    private String getValue(String string) {
        return (string.split("=", 2)[1]).trim();
    }

    public String getName() {
        return name;
    }

    public int getWordsCount() {
        return wordsCount;
    }
}

package com.caveman.lexiconator.model;

import com.caveman.lexiconator.Pair;
import com.caveman.lexiconator.model.room.UserWordMetadata;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

import static java.lang.Integer.MAX_VALUE;

public class RandomWordsModelImpl extends BaseCurrentUserWordsSourceFacade implements RandomWordsModel {

    private CurrentDictionariesHelper currentDictionariesHelper;

    public RandomWordsModelImpl(
            UserWordsSourcesHelper userWordsSourcesHelper,
            CurrentUserWordsSourcesRepository currentUserWordsSourcesRepository,
            CurrentDictionariesHelper currentDictionariesHelper
    ) {
        super(userWordsSourcesHelper, currentUserWordsSourcesRepository);
        this.userWordsSourcesHelper = userWordsSourcesHelper;
        this.currentDictionariesHelper = currentDictionariesHelper;
    }

    @Override
    public Single<Boolean> isHaveCurrentDictionarySources() {
        return Single.fromCallable( () -> currentDictionariesHelper
                .isHaveCurrentEnabledDictionarySourcesFor(findCurrentUserWordsSourceMetadata()));
    }

    @Override
    public Completable addWord(String word) {
        return Completable.fromAction(() -> getCurrentUserWordsSource().addWord(word));
    }

    @Override
    public Single<Pair<Long, List<String>>> getRandomWords() {
        return Single.fromCallable(() -> {
            List<String> result = new ArrayList<>();
            if (hasCurrentUserWordsSource()) {
                List<String> exceptWords = new ArrayList<>();

                for (UserWordMetadata userWordMetadata : getCurrentUserWordsSource().getWordsMetadataList(new SortingOrder(SortColumn.WORD, SortDirection.DESCENDANT))) {
                    exceptWords.add(userWordMetadata.getWord());
                }
                result = currentDictionariesHelper.getRandomWords(MAX_VALUE, exceptWords, findCurrentUserWordsSourceMetadata());
            }
            return new Pair<>(findCurrentUserWordsSourceMetadata().getId(), result);
        });
    }
}

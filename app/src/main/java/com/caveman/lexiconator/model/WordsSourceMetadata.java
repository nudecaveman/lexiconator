package com.caveman.lexiconator.model;

public interface WordsSourceMetadata {

    long getId();

    void setId(long id);

    String getName();

    void setName(String name);

    String getBriefCode();

    void setBriefCode(String briefCode);

    int getWordsCount();

    void setWordsCount(int wordsCount);
}

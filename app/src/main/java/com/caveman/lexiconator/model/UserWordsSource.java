package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordMetadata;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.util.List;

public interface UserWordsSource extends WordsSource<UserWordsSourceMetadata> {

    UserWordMetadata getUserWordMetadataForWord(String word);

    List<UserWordMetadata> getWordsMetadataList(SortingOrder sortingOrder);

    int addWord(String word);

    void editWord(String word, long wordId);

    void deleteWord(UserWordMetadata word);

    void changeWordPriorityBy(UserWordMetadata word, int value);

    UserWordsMinMaxPriorityData getWordsMinMaxPriority();
}

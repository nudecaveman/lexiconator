package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;

public abstract class BaseCurrentUserWordsSourceFacade implements CurrentUserWordsSourceFacade {

    protected UserWordsSourcesHelper userWordsSourcesHelper;
    private CurrentUserWordsSourcesRepository currentUserWordsSourcesRepository;

    public BaseCurrentUserWordsSourceFacade(
            UserWordsSourcesHelper userWordsSourcesHelper,
            CurrentUserWordsSourcesRepository currentUserWordsSourcesRepository) {
        this.currentUserWordsSourcesRepository = currentUserWordsSourcesRepository;
        this.userWordsSourcesHelper = userWordsSourcesHelper;
    }

    protected UserWordsSourceMetadata findCurrentUserWordsSourceMetadata() {
        long userWordsSourceId = currentUserWordsSourcesRepository.getUserWordsSourceIdForeKey(getKey());
        boolean exist = userWordsSourcesHelper.checkIfUserWordsSourceExists(userWordsSourceId);
        return exist ? userWordsSourcesHelper.getUserWordsSourceMetadataById(userWordsSourceId) :
                userWordsSourcesHelper.getFirstUserWordsSourceMetadataFromExisted();
    }

    protected UserWordsSource getCurrentUserWordsSource() {
        UserWordsSource userWordsSource = null;
        UserWordsSourceMetadata userWordsSourceMetadata = findCurrentUserWordsSourceMetadata();
        if (userWordsSourceMetadata != null) {
            userWordsSource = userWordsSourcesHelper.getUserWordsSource(userWordsSourceMetadata);
        }
        return userWordsSource;
    }

    protected boolean hasCurrentUserWordsSource() {
        UserWordsSourceMetadata userWordsSourceMetadata = findCurrentUserWordsSourceMetadata();
            return (userWordsSourceMetadata != null);
    }

    @Override
    public Maybe<UserWordsSourceMetadata> getCurrentUserWordsSourceMetadata() {
        return Maybe.defer(() -> {
            UserWordsSourceMetadata userWordsSourceMetadata = findCurrentUserWordsSourceMetadata();
            if (userWordsSourceMetadata != null) {
                return Maybe.just(userWordsSourceMetadata);
            }
            else {
                return Maybe.empty();
            }
        });
    }

    @Override
    public Completable setCurrentUserWordsSource(UserWordsSourceMetadata userWordsSourceMetadata) {
        return Completable.fromAction(
                () -> currentUserWordsSourcesRepository.setUserWordsSourceIdForKey(getKey(), userWordsSourceMetadata.getId()));
    }

    @Override
    public Single<Boolean> isHaveUserWordsSources() {
        return Single.fromCallable( () -> {
            UserWordsSourceMetadata userWordsSourceMetadata = findCurrentUserWordsSourceMetadata();
            return (userWordsSourceMetadata != null);
        });
    }

    private String getKey() {
        return getClass().getSimpleName();
    }
}

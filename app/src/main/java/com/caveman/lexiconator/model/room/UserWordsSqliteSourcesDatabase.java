package com.caveman.lexiconator.model.room;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import static com.caveman.lexiconator.model.WordsDbContract.DATABASE_FILE_NAME;

@Database(entities = {UserWordsSourceMetadata.class, UserWordMetadata.class}, version = 1)
public abstract class UserWordsSqliteSourcesDatabase  extends RoomDatabase {

    private static UserWordsSqliteSourcesDatabase instance;

    public static synchronized UserWordsSqliteSourcesDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), UserWordsSqliteSourcesDatabase.class, DATABASE_FILE_NAME)
                    .addCallback(new Callback() {
                        @Override
                        public void onCreate(@NonNull SupportSQLiteDatabase db) {
                            super.onCreate(db);

                            //Todo найти лучший способ презаполнения базы данных
                            /**
                             * База не успевается заполниться на старте до того как возьмется первое значение,
                             * приходится использовать этот костыль, чтобы презаписанное значнение на старте
                             * приложения бралось корректно, пока не найдется более адекватное решение
                             * Query может измениться, как и схема базы, а я и не замечу
                             */
                            db.execSQL("INSERT OR IGNORE INTO user_words_sources_metadata (name, brief_code) VALUES ('English', 'eng')");

                            //UserWordsSourceMetadata userWordsSourceMetadata = new UserWordsSourceMetadata("English", "eng");
                            //instance.userWordsSourcesDao().addNewUserWordsSourceMetadata(userWordsSourceMetadata);

                            /*Completable.fromAction( () -> {

                                UserWordsSourceMetadata userWordsSourceMetadata = new UserWordsSourceMetadata("English", "eng");
                                getInstance(context).userWordsSourcesDao().addNewUserWordsSourceMetadata(userWordsSourceMetadata);
                            })
                                    .subscribeOn(Schedulers.newThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe();*/

                        }
                    })
                    .build();
        }
        return instance;
    }

    public abstract UserWordsSourcesDao userWordsSourcesDao();
    public abstract UserWordsDao userWordsDao();
    public abstract UserWordsStatisticDao userWordsStatisticDao();
}

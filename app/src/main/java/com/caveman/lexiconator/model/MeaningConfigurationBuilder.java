package com.caveman.lexiconator.model;

public class MeaningConfigurationBuilder {
    private boolean restrictMeaningsCount;
    private int maxMeaningsCount;
    private boolean meaningsOnNewLines;
    private boolean processTags;

    public MeaningConfigurationBuilder() {
    }

    public MeaningConfigurationBuilder restrictMeaningsCount(boolean restrictMeaningsCount) {
        this.restrictMeaningsCount = restrictMeaningsCount;
        return this;
    }

    public MeaningConfigurationBuilder maxMeaningsCount(int maxMeaningsCount) {
        this.maxMeaningsCount = maxMeaningsCount;
        return this;
    }

    public MeaningConfigurationBuilder meaningsOnNewLines(boolean meaningsOnNewLines) {
        this.meaningsOnNewLines = meaningsOnNewLines;
        return this;
    }

    public MeaningConfigurationBuilder processTags(boolean processTags) {
        this.processTags = processTags;
        return this;
    }

    public MeaningConfiguration build() {
        return new MeaningConfiguration(restrictMeaningsCount, maxMeaningsCount, meaningsOnNewLines, processTags);
    }
}

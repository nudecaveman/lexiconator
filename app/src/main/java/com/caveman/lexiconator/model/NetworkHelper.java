package com.caveman.lexiconator.model;

public interface NetworkHelper {

    boolean isWifiConnected();

    boolean isConnected();
}

package com.caveman.lexiconator.model.StardictModel;

import com.caveman.lexiconator.model.DictionarySourceMetadata;

import java.io.File;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class StarDictDictionarySourceMetadata implements DictionarySourceMetadata, Serializable {

    private File ifoFile;
    private File idxFile;
    private File dictFile;

    private long _id;
    private String name;
    private String briefCode;
    private int wordsCount;
    private long forUserWordsSourceId;
    private boolean isEnabled;


    public StarDictDictionarySourceMetadata(File ifoFile, File idxFile, File dictFile, String name, int wordCount) {
        this._id = -1;
        this.ifoFile = ifoFile;
        this.idxFile = idxFile;
        this.dictFile = dictFile;
        this.name = name;
        this.briefCode = name.substring(0, 3);
        this.wordsCount = wordCount;
        this.forUserWordsSourceId = -1;
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        this._id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getBriefCode() {
        return briefCode;
    }

    @Override
    public void setBriefCode(String briefCode) {
        this.briefCode = briefCode;
    }

    @Override
    public int getWordsCount() {
        return wordsCount;
    }

    @Override
    public void setWordsCount(int wordsCount) {
        this.wordsCount = wordsCount;
    }

    @Override
    public long getForUserWordsSourceId() {
        return forUserWordsSourceId;
    }

    @Override
    public void setForUserWordsSourceId(long forUserWordsSourceId) {
        this.forUserWordsSourceId = forUserWordsSourceId;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    @Override
    public void setIsEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    @Override
    public boolean isAllDictionaryFilesExists() {
        return ifoFile.exists() && idxFile.exists() && dictFile.exists();
    }

    @Override
    public List<File> getDictionaryFiles() {
        return Arrays.asList(ifoFile, idxFile, dictFile);
    }

    @Override
    public long getDictionaryFilesSize() {
        long filesSize = 0;
        for (File file : getDictionaryFiles()) {
            filesSize += file.length();
        }
        return filesSize;
    }

    @Override
    public StarDictDictionarySource createDictionarySource() {
        return new StarDictDictionarySource(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof StarDictDictionarySourceMetadata))
            return false;
        if (obj == this)
            return true;
        StarDictDictionarySourceMetadata dictMetaDataObject = (StarDictDictionarySourceMetadata) obj;
        return (this.getName().equals(dictMetaDataObject.getName()) &&
                this.getWordsCount() == dictMetaDataObject.getWordsCount() &&
                this.ifoFile.equals(dictMetaDataObject.ifoFile) &&
                this.idxFile.equals(dictMetaDataObject.idxFile) &&
                this.dictFile.equals(dictMetaDataObject.dictFile)
        );
    }

    @Override
    public int hashCode() {
        int prime = 31;
        int result = 1;
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + getWordsCount();
        result = prime * result + ifoFile.hashCode();
        result = prime * result + idxFile.hashCode();
        result = prime * result + dictFile.hashCode();
        return result;
    }

    public File getIfoFile() {
        return ifoFile;
    }

    public File getIdxFile() {
        return idxFile;
    }

    public File getDictFile() {
        return dictFile;
    }
}

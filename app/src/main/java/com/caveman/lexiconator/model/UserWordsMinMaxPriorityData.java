package com.caveman.lexiconator.model;

public class UserWordsMinMaxPriorityData {

    private int minPriority;
    private int maxPriority;

    public UserWordsMinMaxPriorityData(int minPriority, int maxPriority) {
        this.minPriority = minPriority;
        this.maxPriority = maxPriority;
    }

    public int getMinPriority() {
        return minPriority;
    }

    public int getMaxPriority() {
        return maxPriority;
    }
}

package com.caveman.lexiconator.model;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface DictionarySourceContentModel {

    Single<List<DictionaryWordMetadata>> getWordsMetadataList();

    Single<Integer> getWordsCounter();

    Completable setCurrentDictionaryWordSource(long dictionaryWordsSourceId);

    Completable setCurrentSortDirection(SortDirection sortDirection);
}

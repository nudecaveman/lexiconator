package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.util.List;

import io.reactivex.Single;

public class ChooseWordsSourceModelImpl implements ChooseWordsSourceModel {
    private UserWordsSourcesHelper userWordsSourcesHelper;
    private CurrentDictionariesHelper currentDictionariesHelper;

    public ChooseWordsSourceModelImpl(
            UserWordsSourcesHelper userWordsSourcesHelper, CurrentDictionariesHelper currentDictionariesHelper) {
        this.userWordsSourcesHelper = userWordsSourcesHelper;
        this.currentDictionariesHelper = currentDictionariesHelper;
    }


    @Override
    public Single<WordsSourcesData> getWordsSourcesData() {
        return Single.fromCallable( () -> {
            List<UserWordsSourceMetadata> userWordsSourceMetadataList = userWordsSourcesHelper.getAllUserWordsSourcesMetadata();
            List<DictionarySourceMetadata> dictionarySourceMetadataList = currentDictionariesHelper.getEnabledDictionariesMetadataList();
            return new WordsSourcesData(userWordsSourceMetadataList, dictionarySourceMetadataList);
        });
    }
}

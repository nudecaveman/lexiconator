package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordMetadata;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class UserWordsSourceContentModelImpl implements UserWordsSourceContentModel {

    private UserWordsSourcesHelper userWordsSourcesHelper;
    private CurrentSortingRepository currentSortingRepository;
    private CurrentDictionariesHelper currentDictionariesHelper;
    private UserWordsSourceMetadata currentUserWordsSourceMetadata;

    public UserWordsSourceContentModelImpl(
            UserWordsSourcesHelper userWordsSourcesHelper,
            CurrentDictionariesHelper currentDictionariesHelper,
            CurrentSortingRepository currentSortingRepository
    ) {
        this.userWordsSourcesHelper = userWordsSourcesHelper;
        this.currentDictionariesHelper = currentDictionariesHelper;
        this.currentSortingRepository = currentSortingRepository;
    }

    @Override
    public Completable addWord(String word) {
        return Completable.fromAction( () -> getCurrentUserWordsSource().addWord(word));
    }

    @Override
    public Completable editWord(String word, long wordId) {
        return Completable.fromAction( () -> getCurrentUserWordsSource().editWord(word, wordId));
    }

    @Override
    public Completable deleteWord(UserWordMetadata word) {
        return Completable.fromAction( () -> getCurrentUserWordsSource().deleteWord(word));
    }

    @Override
    public Completable setCurrentSortingOrder(SortingOrder sortingOrder) {
        return Completable.fromAction(() -> currentSortingRepository.setCurrentUserWordsSourceSortingOrder(sortingOrder));
    }

    @Override
    public Completable setCurrentWordSourceId(long currentWordSourceId) {
        return Completable.fromAction( () -> {
            this.currentUserWordsSourceMetadata = userWordsSourcesHelper.getUserWordsSourceMetadataById(currentWordSourceId);
        });
    }

    @Override
    public Single<Boolean> isHaveDictionarySources() {
        return Single.fromCallable( () -> currentDictionariesHelper
                .isHaveCurrentEnabledDictionarySourcesFor(currentUserWordsSourceMetadata));
    }

    @Override
    public Single<List<UserWordMetadata>> getWordsMetadataList() {
        return Single.fromCallable(() -> getCurrentUserWordsSource()
                .getWordsMetadataList(currentSortingRepository.getCurrentUserWordsSourceSortingOrder()));
    }

    @Override
    public Single<UserWordsMinMaxPriorityData> getWordsMinMaxPriorityData() {
        return Single.fromCallable( () ->  getCurrentUserWordsSource().getWordsMinMaxPriority());
    }

    @Override
    public Single<Integer> getWordsCounter() {
        return Single.fromCallable( () -> getCurrentUserWordsSource().getWordsCount());
    }

    private UserWordsSource getCurrentUserWordsSource() {
        return userWordsSourcesHelper.getUserWordsSource(currentUserWordsSourceMetadata);
    }
}

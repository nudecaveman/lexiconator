package com.caveman.lexiconator.model;

public interface PreferencesHelper {

    boolean isFirstRun();

    void setIsFirstRun(boolean isFirstRun);

    String getDictionariesCustomDirectoryPath();

    void setDictionariesCustomDirectoryPath(String dictionariesCustomDirectoryPath);

    int getWordDelayValue();

    void setWordDelayValue(int wordDelay);

    boolean isMakeInternetOperationsIfWiFiOnly();

    void setMakeInternetOperationsIfWiFiOnly(boolean makeInternetOperationsIfWiFiOnly);

    boolean clipboardQuickWordsAddIsActive();

    void setClipboardQuickWordsAddIsActive(boolean clipboardQuickWordsAddIsActive);

    long getClipboardQuickWordsAddUserWordsSourceId();

    void setClipboardQuickWordsAddUserWordsSourceId(long userWordsSourceId);

    boolean clipboardTranslationIsActive();

    void setClipboardTranslationIsActive(boolean clipboardTranslationIsActive);

    String getClipboardTranslationSourceLang();

    void setClipboardTranslationSourceLang(String clipboardTranslationSourceLang);

    String getClipboardTranslationTargetLang();

    void setClipboardTranslationTargetLang(String clipboardTranslationTargetLang);

    boolean isRestrictMeaningsCount();

    void setRestrictMeaningsCount(boolean isRestrictMeaningsCount);

    int getMaxMeaningsCount();

    void setMaxMeaningsCount(int maxMeaningsCount);

    boolean isMeaningsOnNewLines();

    void setMeaningsOnNewLines(boolean isMeaningsOnNewLines);

    boolean isProcessTags();

    void setProcessTags(boolean isProcessTags);
}

package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordMetadata;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface WordTestModel {

    Single<WordTestData> getWordTestData(String word, long userWordsSourceId, MeaningConfiguration meaningConfiguration);

    Completable countWordTestSuccess(UserWordMetadata userWordMetadata);

    Completable countWordTestFail(UserWordMetadata userWordMetadata);
}

package com.caveman.lexiconator.model;

import java.io.File;

public interface DictionariesDirectoryHelper {

    void createDefaultDictionariesIfFirstRun();

    File getDictionariesDefaultDirectory();

    void setDictionariesDefaultDirectory(File dictionariesDefaultDirectory);

    File getDictionariesCustomDirectory();

    void setDictionariesCustomDirectory(File dictionariesCustomDirectory);
}

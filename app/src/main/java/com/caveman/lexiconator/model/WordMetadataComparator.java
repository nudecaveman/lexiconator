package com.caveman.lexiconator.model;

import org.apache.commons.lang3.StringUtils;

import java.util.Comparator;

public class WordMetadataComparator implements Comparator<WordMetadata> {

    @Override
    public int compare(WordMetadata wordMetadata1, WordMetadata wordMetadata2) {
        String word1 = StringUtils.trim(StringUtils.strip(wordMetadata1.getWord().toLowerCase()));
        String word2 = StringUtils.trim(StringUtils.strip(wordMetadata2.getWord().toLowerCase()));
        return word1.compareTo(word2);
    }
}

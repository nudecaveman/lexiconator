package com.caveman.lexiconator.model;

import java.io.Serializable;

public class SortingOrder implements Serializable {

    private SortColumn sortColumn;
    private SortDirection sortDirection;

    public SortingOrder(SortColumn sortColumn, SortDirection sortDirection) {
        this.sortColumn = sortColumn;
        this.sortDirection = sortDirection;
    }

    public SortColumn getSortColumn() {
        return sortColumn;
    }

    public SortDirection getSortDirection() {
        return sortDirection;
    }

    public void setSortColumn(SortColumn sortColumn) {
        this.sortColumn = sortColumn;
    }

    public void setSortDirection(SortDirection sortDirection) {
        this.sortDirection = sortDirection;
    }
}

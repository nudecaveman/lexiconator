package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordMetadata;

import java.util.List;

import io.reactivex.Single;

public interface WordsTrainingModel extends CurrentUserWordsSourceFacade {

    Single<List<UserWordMetadata>> getUserWordsList();

    Single<Boolean> isHasCurrentDictionarySources();
}

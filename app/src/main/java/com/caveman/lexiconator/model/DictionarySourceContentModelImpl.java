package com.caveman.lexiconator.model;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class DictionarySourceContentModelImpl implements DictionarySourceContentModel {

    private CurrentDictionariesHelper currentDictionariesHelper;
    private CurrentSortingRepository currentSortingRepository;
    private DictionarySource currentDictionarySource;

    public DictionarySourceContentModelImpl(CurrentDictionariesHelper currentDictionariesHelper, CurrentSortingRepository currentSortingRepository) {
        this.currentDictionariesHelper = currentDictionariesHelper;
        this.currentSortingRepository = currentSortingRepository;
    }

    @Override
    public Single<List<DictionaryWordMetadata>> getWordsMetadataList() {
        return Single.fromCallable(() -> currentDictionarySource.getWordsMetadataList(currentSortingRepository.getCurrentDictionarySourceSortDirection()));
    }

    @Override
    public Single<Integer> getWordsCounter() {
        return Single.fromCallable(() -> currentDictionarySource.getWordsCount());
    }

    @Override
    public Completable setCurrentDictionaryWordSource(long dictionaryWordsSourceId) {
        return Completable.fromAction( () -> currentDictionarySource = currentDictionariesHelper.getCurrentDictionarySourceById(dictionaryWordsSourceId));
    }

    @Override
    public Completable setCurrentSortDirection(SortDirection sortDirection) {
        return Completable.fromAction( () -> {
            currentSortingRepository.setCurrentDictionarySourceSortDirection(sortDirection);
        });
    }
}

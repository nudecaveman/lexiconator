package com.caveman.lexiconator.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.caveman.lexiconator.R;

public class AndroidPreferencesHelper implements PreferencesHelper {

    public static final String KEY_PREF_IS_FIRST_RUN = "KEY_PREF_IS FIRST_RUN";
    private SharedPreferences preferences;
    private String dictionariesCustomDirectoryPathKey;

    private String restrictMeaningsCountKey;
    private String maxMeaningsCountKey;
    private String meaningsOnNewLinesKey;
    private String processTagsKey;

    private String wordDelayKey;
    private String makeInternetOperationsIfWifiOnlyKey;
    private String clipboardQuickWordsAddIsActiveKey;
    private String clipboardQuickWordsAddUserWordsSourceIdKey;
    private String clipboardTranslationIsActiveKey;
    private String clipboardTranslationSourceLangKey;
    private String clipboardTranslationTargetLangKey;

    public AndroidPreferencesHelper(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);

        dictionariesCustomDirectoryPathKey = context.getString(R.string.KEY_PREF_DICTIONARIES_CUSTOM_DIRECTORY);

        restrictMeaningsCountKey = context.getString(R.string.KEY_PREF_CARD_DISPLAY_OPTIONS_RESTRICT_MEANINGS_COUNT);
        maxMeaningsCountKey = context.getString(R.string.KEY_PREF_CARD_DISPLAY_OPTIONS_MEANINGS_MAX_COUNT);
        meaningsOnNewLinesKey = context.getString(R.string.KEY_PREF_CARD_DISPLAY_OPTIONS_MEANINGS_ON_NEW_LINES);
        processTagsKey = context.getString(R.string.KEY_PREF_CARD_DISPLAY_OPTIONS_PROCESS_TAGS);

        wordDelayKey = context.getString(R.string.KEY_PREF_WORDS_TRAINING_DELAY);
        makeInternetOperationsIfWifiOnlyKey = context.getString(R.string.KEY_PREF_MAKE_INTERNET_OPERATIONS_IF_WIFI_ONLY);
        clipboardQuickWordsAddIsActiveKey = context.getString(R.string.KEY_PREF_CLIPBOARD_SERVICE_QUICK_WORDS_ADD_IS_ACTIVE);
        clipboardQuickWordsAddUserWordsSourceIdKey = context.getString(R.string.KEY_PREF_CLIPBOARD_SERVICE_QUICK_WORDS_ADD_USER_WORDS_SOURCE_ID);
        clipboardTranslationIsActiveKey = context.getString(R.string.KEY_PREF_CLIPBOARD_SERVICE_TRANSLATE_IS_ACTIVE);
        clipboardTranslationSourceLangKey = context.getString(R.string.KEY_PREF_CLIPBOARD_TRANSLATION_LANGUAGE_SOURCE);
        clipboardTranslationTargetLangKey = context.getString(R.string.KEY_PREF_CLIPBOARD_TRANSLATION_LANGUAGE_TARGET);
    }

    @Override
    public boolean isFirstRun() {
        return preferences.getBoolean(KEY_PREF_IS_FIRST_RUN, true);
    }

    @Override
    public void setIsFirstRun(boolean isFirstRun) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_PREF_IS_FIRST_RUN, isFirstRun);
        editor.apply();
    }

    @Override
    public String getDictionariesCustomDirectoryPath() {
        return preferences.getString(dictionariesCustomDirectoryPathKey, null);
    }

    @Override
    public void setDictionariesCustomDirectoryPath(String dictionariesCustomDirectoryPath) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(dictionariesCustomDirectoryPathKey, dictionariesCustomDirectoryPath);
        editor.apply();
    }

    @Override
    public int getWordDelayValue() {
        return Integer.valueOf(preferences.getString(wordDelayKey, "20"));
    }

    @Override
    public void setWordDelayValue(int wordDelay) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(wordDelayKey, wordDelay);
        editor.apply();
    }

    @Override
    public boolean isMakeInternetOperationsIfWiFiOnly() {
        return preferences.getBoolean(makeInternetOperationsIfWifiOnlyKey, false);
    }

    @Override
    public void setMakeInternetOperationsIfWiFiOnly(boolean makeInternetOperationsIfWiFiOnly) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(makeInternetOperationsIfWifiOnlyKey, makeInternetOperationsIfWiFiOnly);
        editor.apply();
    }

    @Override
    public boolean clipboardQuickWordsAddIsActive() {
        return preferences.getBoolean(clipboardQuickWordsAddIsActiveKey, false);
    }

    @Override
    public void setClipboardQuickWordsAddIsActive(boolean clipboardQuickWordsAddIsActive) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(clipboardQuickWordsAddIsActiveKey, clipboardQuickWordsAddIsActive);
        editor.apply();
    }

    @Override
    public long getClipboardQuickWordsAddUserWordsSourceId() {
        return preferences.getLong(clipboardQuickWordsAddUserWordsSourceIdKey, -1);
    }

    @Override
    public void setClipboardQuickWordsAddUserWordsSourceId(long userWordsSourceId) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(clipboardQuickWordsAddUserWordsSourceIdKey, userWordsSourceId);
        editor.apply();
    }

    @Override
    public boolean clipboardTranslationIsActive() {
        return preferences.getBoolean(clipboardTranslationIsActiveKey, false);
    }

    @Override
    public void setClipboardTranslationIsActive(boolean clipboardTranslationIsActive) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(clipboardTranslationIsActiveKey, clipboardTranslationIsActive);
        editor.apply();
    }

    @Override
    public String getClipboardTranslationSourceLang() {
        return preferences.getString(clipboardTranslationSourceLangKey, "auto");
    }

    @Override
    public void setClipboardTranslationSourceLang(String clipboardTranslationSourceLang) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(clipboardTranslationSourceLangKey, clipboardTranslationSourceLang);
        editor.apply();
    }

    @Override
    public String getClipboardTranslationTargetLang() {
        return preferences.getString(clipboardTranslationTargetLangKey, "ru");
    }

    @Override
    public void setClipboardTranslationTargetLang(String clipboardTranslationTargetLang) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(clipboardTranslationTargetLangKey, clipboardTranslationTargetLang);
        editor.apply();
    }

    @Override
    public boolean isRestrictMeaningsCount() {
        return preferences.getBoolean(restrictMeaningsCountKey, true);
    }

    @Override
    public void setRestrictMeaningsCount(boolean isRestrictMeaningsCount) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(restrictMeaningsCountKey, isRestrictMeaningsCount);
        editor.apply();
    }

    @Override
    public int getMaxMeaningsCount() {
        return Integer.valueOf(preferences.getString(maxMeaningsCountKey, "3"));
    }

    @Override
    public void setMaxMeaningsCount(int maxMeaningsCount) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(maxMeaningsCountKey, maxMeaningsCount);
        editor.apply();
    }

    @Override
    public boolean isMeaningsOnNewLines() {
        return preferences.getBoolean(meaningsOnNewLinesKey, true);
    }

    @Override
    public void setMeaningsOnNewLines(boolean isMeaningsOnNewLines) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(meaningsOnNewLinesKey, isMeaningsOnNewLines);
        editor.apply();
    }


    @Override
    public boolean isProcessTags() {
        return preferences.getBoolean(processTagsKey, true);
    }

    @Override
    public void setProcessTags(boolean isProcessTags) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(processTagsKey, isProcessTags);
        editor.apply();
    }
}

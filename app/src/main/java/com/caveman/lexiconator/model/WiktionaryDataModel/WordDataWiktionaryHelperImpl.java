package com.caveman.lexiconator.model.WiktionaryDataModel;

import com.caveman.lexiconator.model.ServiceGenerator;
import com.caveman.lexiconator.model.WiktionaryDataModel.POJO.WordWikiData;
import com.caveman.lexiconator.model.WordData;
import com.caveman.lexiconator.model.WordDataHelper;

import org.jsoup.Jsoup;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.reactivex.Single;

public class WordDataWiktionaryHelperImpl implements WordDataHelper {

    private static WordDataHelper instance;
    private Locale wordLanguage;
    //private Locale meaningLanguage;
    private WiktionaryService service;


    private WordDataWiktionaryHelperImpl(Locale wordLanguage){
        service = ServiceGenerator.createService(
                WiktionaryService.class,
                getWordLanguageUrl()
        );
    }

    public static WordDataHelper getInstance(Locale wordLanguage) {
        if (instance == null) {
            instance = new WordDataWiktionaryHelperImpl(wordLanguage);
        }
        return instance;
    }

    private String getWordLanguageUrl() {
        //TODO адаптировать к кодам-исключениям для википедии.
        return String.format("https://%s.wiktionary.org/", wordLanguage.getLanguage());
    }

    @Override
    public Single<WordData> getWordData(String word, String sourceLanguage, String targetLanguage) {
        Single<Map<String, List<WordWikiData>>> rawWordData = service.getWordRawData(word);
        return rawWordData.flatMap(
                x -> {
                    String key = "en";
                    WordData wordData = new WordData("None", "None", null);
                    if (x.containsKey(key)) {
                        List<WordWikiData> wordWikiDataList = x.get(key);
                        WordWikiData wordWikiData = wordWikiDataList.get(0);

                        String definition = Jsoup.parse(wordWikiData.getDefinitions().get(0).getDefinition()).text();
                        wordData = new WordData(word, definition, null);
                    }
                    return Single.just(wordData);
                });
    }
}


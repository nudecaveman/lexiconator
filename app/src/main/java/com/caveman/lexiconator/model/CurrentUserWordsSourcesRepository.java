package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

public interface CurrentUserWordsSourcesRepository {

    void setUserWordsSourceIdForKey(String key, long userWordsSourceId);

    long getUserWordsSourceIdForeKey(String key);
}

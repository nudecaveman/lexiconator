package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordMetadata;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface UserWordsSourceContentModel {

    Single<Boolean> isHaveDictionarySources();

    Single<List<UserWordMetadata>> getWordsMetadataList();

    Single<UserWordsMinMaxPriorityData> getWordsMinMaxPriorityData();

    Single<Integer> getWordsCounter();

    Completable addWord(String word);

    Completable editWord(String word, long wordId);

    Completable deleteWord(UserWordMetadata word);

    Completable setCurrentSortingOrder(SortingOrder sortingOrder);

    Completable setCurrentWordSourceId(long currentWordSourceId);
}

package com.caveman.lexiconator.model.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface UserWordsSourcesDao {

    @Query("SELECT EXISTS(SELECT * FROM user_words_sources_metadata WHERE _id = :userWordsSourceId)")
    boolean checkIfUserWordsSourceExists(long userWordsSourceId);

    @Query("SELECT * FROM user_words_sources_metadata")
    List<UserWordsSourceMetadata> getAllUserWordsSourcesMetadata();

    @Query("SELECT * FROM user_words_sources_metadata WHERE _id = :userWordsSourceId")
    UserWordsSourceMetadata getUserWordsSourceMetadataById(long userWordsSourceId);

    @Insert
    long addNewUserWordsSourceMetadata(UserWordsSourceMetadata userWordsSource);

    @Update
    int updateUserWordsSourceMetadata(UserWordsSourceMetadata userWordsSource);

    @Delete
    int deleteUserWordsSourceMetadata(UserWordsSourceMetadata userWordsSource);
}

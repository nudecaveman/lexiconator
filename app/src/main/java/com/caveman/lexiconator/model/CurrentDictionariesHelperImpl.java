package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CurrentDictionariesHelperImpl implements CurrentDictionariesHelper {

    private CurrentDictionariesRepository currentDictionariesRepository;

    public CurrentDictionariesHelperImpl(CurrentDictionariesRepository currentDictionariesRepository) {
        this.currentDictionariesRepository = currentDictionariesRepository;
    }

    @Override
    public DictionarySource getCurrentDictionarySourceById(long dictionaryWordsSourceId) {
        return currentDictionariesRepository.getCurrentDictionarySourceById(dictionaryWordsSourceId);
    }

    @Override
    public List<DictionarySourceMetadata> getEnabledDictionariesMetadataList() {
        return currentDictionariesRepository.getEnabledDictionariesMetadataList();
    }

    @Override
    public List<DictionarySource> getEnabledDictionarySourcesFor(UserWordsSourceMetadata userWordsSourceMetadata) {
        return currentDictionariesRepository.getEnabledDictionarySourcesFor(userWordsSourceMetadata);
    }

    @Override
    public void removeCurrentDictionary(DictionarySource dictionary) {
        currentDictionariesRepository.removeCurrentDictionary(dictionary);
    }

    @Override
    public void removeCurrentDictionary(DictionarySourceMetadata dictionarySourceMetadata) {
        currentDictionariesRepository.removeCurrentDictionary(dictionarySourceMetadata);
    }

    @Override
    public boolean isHaveCurrentEnabledDictionarySourcesFor(UserWordsSourceMetadata userWordsSourceMetadata) {
        return currentDictionariesRepository.isHaveCurrentEnabledDictionarySourcesFor(userWordsSourceMetadata);
    }

    @Override
    public List<DictionarySourceMetadata> saveDictionariesAndReturnActual(List<DictionarySourceMetadata> dictionarySourceMetadataList) {
        return currentDictionariesRepository.saveDictionariesAndReturnActual(dictionarySourceMetadataList);
    }

    @Override
    public void updateCurrentDictionary(DictionarySourceMetadata dictionarySourceMetadata) {
        currentDictionariesRepository.updateCurrentDictionary(dictionarySourceMetadata);
    }

    @Override
    public boolean checkIfDictionaryIsAvailable(long dictionarySourceId) {
        return currentDictionariesRepository.checkIfDictionaryIsAvailable(dictionarySourceId);
    }

    @Override
    public DictionarySourceMetadata getFirstDictionarySourceMetadataFromAvailable() {
        return currentDictionariesRepository.getFirstDictionarySourceMetadataFromAvailable();
    }

    @Override
    public DictionarySourceMetadata getDictionarySourceMetadataById(long dictionaryWordsSourceId) {
        return currentDictionariesRepository.getDictionarySourceMetadataById(dictionaryWordsSourceId);
    }

    @Override
    public List<WordData> findWord(String word, UserWordsSourceMetadata userWordsSourceMetadata) {
        List<WordData> wordDataList = new ArrayList<>();
        List<DictionarySource> dictionaries = currentDictionariesRepository.getEnabledDictionarySourcesFor(userWordsSourceMetadata);
        for (DictionarySource dictionary : dictionaries) {
            WordData wordData = dictionary.findWord(word);
            if (wordData != null) {
                wordDataList.add(wordData);
            }
        }
        if (wordDataList.size() == 0) {
            wordDataList.add(new WordData("Not Found", " ", new MeaningProcessorImpl()));
        }
        return wordDataList;
    }

    @Override
    public List<WordData> findWord(long dictionarySourceId, String word) {
        List<WordData> wordDataList = new ArrayList<>();
        DictionarySource dictionarySource = currentDictionariesRepository.getCurrentDictionarySourceById(dictionarySourceId);
        WordData wordData = dictionarySource.findWord(word);
        wordDataList.add(wordData);
        return wordDataList;
    }

    @Override
    public List<String> getExistedFromWords(List<String> words, UserWordsSourceMetadata userWordsSourceMetadata) {
        Set<String> existedWordsResult = new HashSet<>();
        List<DictionarySource> dicts = currentDictionariesRepository.getEnabledDictionarySourcesFor(userWordsSourceMetadata);
        for (DictionarySource dictionary : dicts) {
            List<String> existedWords = dictionary.getExistedFromWords(words);
            existedWordsResult.addAll(existedWords);
        }
        return new ArrayList<>(existedWordsResult);
    }

    @Override
    public List<String> getRandomWords(int count, List<String> exceptWords, UserWordsSourceMetadata userWordsSourceMetadata) {
        Set<String> randomWordsSet = new HashSet<>();
        List<DictionarySource> dicts = currentDictionariesRepository.getEnabledDictionarySourcesFor(userWordsSourceMetadata);

        for (DictionarySource dictionary : dicts) {
            List<String> randomWords = dictionary.getRandomWords((count), exceptWords);
            randomWordsSet.addAll(randomWords);
        }
        List<String> result = new ArrayList<>(randomWordsSet);
        Collections.shuffle(result);
        if (result.size() > count) {
            result = result.subList(0, count);
        }
        return result;
    }
}

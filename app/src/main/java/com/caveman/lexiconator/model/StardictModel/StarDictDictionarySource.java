package com.caveman.lexiconator.model.StardictModel;

import com.caveman.lexiconator.model.DictionarySource;
import com.caveman.lexiconator.model.DictionaryWordMetadata;
import com.caveman.lexiconator.model.MeaningProcessor;
import com.caveman.lexiconator.model.MeaningProcessorImpl;
import com.caveman.lexiconator.model.SortDirection;
import com.caveman.lexiconator.model.WordData;
import com.caveman.lexiconator.model.WordMetadataComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.caveman.lexiconator.model.SortDirection.ASCENDANT;
import static com.caveman.lexiconator.model.SortDirection.DESCENDANT;

public class StarDictDictionarySource implements DictionarySource {
    private StarDictDictionarySourceMetadata dictionarySourceMetadata;
    private StarDictParser starDictParser;

    public StarDictDictionarySource(StarDictDictionarySourceMetadata dictionarySourceMetadata) {
        this.dictionarySourceMetadata = dictionarySourceMetadata;
    }

    @Override
    public StarDictDictionarySourceMetadata getWordSourceMetadata() {
        return dictionarySourceMetadata;
    }

    @Override
    public List<DictionaryWordMetadata> getWordsMetadataList(SortDirection sortDirection) {
        List<String> words = new ArrayList<>(getStarDictParser().getWords());

        List<DictionaryWordMetadata> wordMetadataList = new ArrayList<>();
        for (String word : words) {
            wordMetadataList.add(new DictionaryWordMetadata(word, dictionarySourceMetadata.getId()));
        }

        WordMetadataComparator comparator = new WordMetadataComparator();
        if (sortDirection == ASCENDANT) {
            Collections.sort(wordMetadataList, comparator);
        }
        else if (sortDirection == DESCENDANT) {
            Collections.sort(wordMetadataList, Collections.reverseOrder(comparator));
        }

        return wordMetadataList;
    }

    @Override
    public int getWordsCount() {
        return dictionarySourceMetadata.getWordsCount();
    }

    @Override
    public WordData findWord(String word) {
        List<Map.Entry<String,WordPosition>> variants = getStarDictParser().findWord(word);

        WordData wordData = null;
        if (variants.size() != 0) {
            Map.Entry<String,WordPosition> variant = variants.get(0);
            String explanation = getStarDictParser().getWordExplanation(variant.getValue());
            wordData = new WordData(variant.getKey(), explanation, getWordMeaningParser());

        }
        return wordData;
    }


    @Override
    public List<String> getExistedFromWords(List<String> words) {
        return getStarDictParser().getExistedFromWords(words);
    }

    @Override
    public MeaningProcessor getWordMeaningParser() {
        return new MeaningProcessorImpl();
    }

    @Override
    public List<String> getRandomWords(int count, List<String> exceptWords) {
        return getStarDictParser().getRandomWords(count, exceptWords);
    }

    private StarDictParser getStarDictParser() {
        if (starDictParser == null) {
            starDictParser = new StarDictParser(dictionarySourceMetadata.getIdxFile(), dictionarySourceMetadata.getDictFile());
        }
        return starDictParser;
    }
}

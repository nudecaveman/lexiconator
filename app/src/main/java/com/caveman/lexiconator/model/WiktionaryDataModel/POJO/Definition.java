package com.caveman.lexiconator.model.WiktionaryDataModel.POJO;

public class Definition {

    private String definition;

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }
}

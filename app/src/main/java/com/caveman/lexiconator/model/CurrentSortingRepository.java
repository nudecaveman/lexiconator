package com.caveman.lexiconator.model;

public interface CurrentSortingRepository {

    SortingOrder getCurrentUserWordsSourceSortingOrder();

    void setCurrentUserWordsSourceSortingOrder(SortingOrder sortingOrder);

    SortDirection getCurrentDictionarySourceSortDirection();

    void setCurrentDictionarySourceSortDirection(SortDirection sortDirection);
}

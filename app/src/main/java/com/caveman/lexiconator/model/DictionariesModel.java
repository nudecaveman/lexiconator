package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface DictionariesModel {

    Single<List<DictionarySourceMetadata>> scanDictionariesDirectories();

    Single<Map<Long, String>> getUserWordsSourceIdsAndBriefCodesMap();

    Completable bindDictionaryToUserWordsSource(long dictionarySourceId, UserWordsSourceMetadata userWordsSourceMetadata);

    Completable setDictionaryIsEnable(DictionarySourceMetadata dictionarySourceMetadata, boolean isEnabled);

    Completable deleteDictionary(DictionarySourceMetadata dictionarySourceMetadata);
}

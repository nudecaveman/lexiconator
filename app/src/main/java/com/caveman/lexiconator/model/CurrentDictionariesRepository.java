package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.util.List;

public interface CurrentDictionariesRepository {

    boolean checkIfDictionaryIsAvailable(long dictionarySourceId);

    DictionarySourceMetadata getFirstDictionarySourceMetadataFromAvailable();

    DictionarySourceMetadata getDictionarySourceMetadataById(long dictionaryWordsSourceId);

    DictionarySource getCurrentDictionarySourceById(long dictionaryWordsSourceId);

    List<DictionarySourceMetadata> getEnabledDictionariesMetadataList();

    List<DictionarySource> getEnabledDictionarySourcesFor(UserWordsSourceMetadata userWordsSourceMetadata);

    boolean isHaveCurrentEnabledDictionarySourcesFor(UserWordsSourceMetadata userWordsSourceMetadata);

    List<DictionarySourceMetadata> saveDictionariesAndReturnActual(List<DictionarySourceMetadata> dictionarySourceMetadataList);

    void updateCurrentDictionary(DictionarySourceMetadata dictionarySourceMetadata);

    void removeCurrentDictionary(DictionarySourceMetadata dictionarySourceMetadata);

    void removeCurrentDictionary(DictionarySource dictionary);
}

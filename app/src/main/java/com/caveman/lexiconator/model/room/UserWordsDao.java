package com.caveman.lexiconator.model.room;

import android.arch.persistence.db.SupportSQLiteQuery;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.RawQuery;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface UserWordsDao {

    //@Query("INSERT OR IGNORE INTO words_table (word) VALUES (:wordArg)")
    // INSERT not supported by Room yet
    //void addWord(String wordArg);

    @Query("SELECT EXISTS(SELECT 1 FROM user_words_metadata WHERE word = :word AND user_words_source_id = :userWordsSourceId)")
    boolean checkIfWordExist(String word, long userWordsSourceId);

    @Query("SELECT EXISTS(SELECT 1 FROM user_words_metadata WHERE _id = :wordsId)")
    boolean checkIfWordExist(long wordsId);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long addWord(UserWordMetadata userWordMetadata);

    @Update
    int updateWord(UserWordMetadata userWordMetadata);

    @Delete
    int deleteWord(UserWordMetadata userWordMetadata);

    @Query("SELECT * FROM user_words_metadata WHERE user_words_source_id = :userWordsSourceId AND word = :word")
    UserWordMetadata getWordMetadataForWord(long userWordsSourceId, String word);

    @Query("SELECT * FROM user_words_metadata WHERE _id = :userWordId")
    UserWordMetadata getWordMetadataForWordId(long userWordId);

    //"SELECT * FROM user_words_metadata WHERE user_words_source_id = :userWordsSourceId ORDER BY :sortColumn ASC"
    @RawQuery
    List<UserWordMetadata> getWordsForSourceId(SupportSQLiteQuery query);

    @Query("SELECT MIN(priority) FROM user_words_metadata WHERE user_words_source_id = :userWordsSourceId")
    int getWordsMinPriorityForSourceId(long userWordsSourceId);

    @Query("SELECT MAX(priority) FROM user_words_metadata WHERE user_words_source_id = :userWordsSourceId")
    int getWordsMaxPriorityForSourceId(long userWordsSourceId);

    @Query("SELECT COUNT(*) FROM user_words_metadata WHERE user_words_source_id = :userWordsSourceId")
    int getWordsCountForSourceId(long userWordsSourceId);
}

package com.caveman.lexiconator.model;

public interface WordMetadata {

    String getWord();

    void setWord(String word);
}

package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordMetadata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class WordTestModelImpl implements WordTestModel {

    private UserWordsSourcesHelper userWordsSourcesHelper;
    private CurrentDictionariesHelper currentDictionariesHelper;

    public WordTestModelImpl(UserWordsSourcesHelper userWordsSourcesHelper,
            CurrentDictionariesHelper currentDictionariesHelper) {
        this.userWordsSourcesHelper = userWordsSourcesHelper;
        this.currentDictionariesHelper = currentDictionariesHelper;
    }

    @Override
    public Single<WordTestData> getWordTestData(String word, long userWordsSourceId, MeaningConfiguration meaningConfiguration) {
        return Single.fromCallable( () -> {

            UserWordsSource userWordsSource = userWordsSourcesHelper.getUserWordsSourceById(userWordsSourceId);
            UserWordMetadata userWordMetadata = userWordsSource.getUserWordMetadataForWord(word);
            List<WordData> wordDataList = currentDictionariesHelper.findWord(word, userWordsSource.getWordSourceMetadata());

            WordData wordData = wordDataList.get(0);
            String processedForTestMeaning = wordData.getMeaningProcessor()
                    .processForWordTest(wordData.getWord(), wordData.getMeaning(), meaningConfiguration);
            wordData.setMeaning(processedForTestMeaning);

            List<WordData> wordVariants = new ArrayList<>();
            wordVariants.add(wordData);

            List<String> randomWords = currentDictionariesHelper
                    .getRandomWords(3, Arrays.asList(wordData.getWord()), userWordsSource.getWordSourceMetadata());
            for (String randomWord : randomWords) {
                List<WordData> randomWordDataList = currentDictionariesHelper
                        .findWord(randomWord, userWordsSource.getWordSourceMetadata());
                WordData randomWordData = randomWordDataList.get(0);
                wordVariants.add(randomWordData);
            }
            Collections.shuffle(wordVariants);

            return new WordTestData(userWordMetadata, wordData, wordVariants);
        });
    }

    @Override
    public Completable countWordTestSuccess(UserWordMetadata userWordMetadata) {
        return Completable.fromAction( () -> {
            getUserWordsSource(userWordMetadata).changeWordPriorityBy(userWordMetadata, -1);
        });
    }

    @Override
    public Completable countWordTestFail(UserWordMetadata userWordMetadata) {
        return Completable.fromAction( () -> {
            getUserWordsSource(userWordMetadata).changeWordPriorityBy(userWordMetadata, 2);
        });
    }

    private UserWordsSource getUserWordsSource(UserWordMetadata userWordMetadata) {
        return userWordsSourcesHelper.getUserWordsSourceById(userWordMetadata.getUserWordsSourceId());
    }
}

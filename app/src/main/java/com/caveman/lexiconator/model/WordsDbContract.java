package com.caveman.lexiconator.model;

public abstract class WordsDbContract {

    public static final String DATABASE_FILE_NAME = "words.db";

    public static class UserWordsSourcesMetadataTable {
        public static final String TABLE_NAME = "user_words_sources_metadata";
        public static final String COLUMN_NAME_ID = "_id";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_BRIEF_CODE = "brief_code";
    }

    public static class WordsMetadataTable {
        public static final String TABLE_NAME = "user_words_metadata";
        public static final String COLUMN_NAME_ID = "_id";
        public static final String COLUMN_NAME_WORD = "word";
        public static final String COLUMN_NAME_ADD_TIMESTAMP = "add_timestamp";
        public static final String COLUMN_NAME_PRIORITY = "priority";
        public static final String COLUMN_NAME_USER_WORDS_SOURCE_ID = "user_words_source_id";
    }
}

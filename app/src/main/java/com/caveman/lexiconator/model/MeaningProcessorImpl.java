package com.caveman.lexiconator.model;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.parser.Tag;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MeaningProcessorImpl implements MeaningProcessor {

    public String processForWordCard(String word, String meaning, MeaningConfiguration meaningConfiguration) {
        String result = removeRedundantWord(meaning);
        result = distinguishTranscription(result);
        if (meaningConfiguration.isRestrictMeaningsCount()) {
            result = getMeanings(result, meaningConfiguration.getMaxMeaningsCount());
        }
        if (meaningConfiguration.isMeaningsOnNewLines()) {
            result = moveMeaningsOnNewLines(result);
        }
        if (meaningConfiguration.isProcessTags()) {
            result = processTagsAndReturnInHtml(result);
        }
        return result;
    }

    public String processForWordTest(String word, String meaning, MeaningConfiguration meaningConfiguration) {
        String result = removeTranscription(meaning);
        result = processForWordCard(word, result, meaningConfiguration)
                .replaceAll(word, "?");
        result = StringUtils.capitalize(result);
        return result;
    }

    private String removeRedundantWord(String meaning) {
        Document doc = Jsoup.parse(meaning, "", Parser.xmlParser());
        doc.outputSettings(new Document.OutputSettings().prettyPrint(false));
        for (Element e : doc.select("k")) {
            e.remove();
        }
        return doc.html();
    }

    private String removeTranscription(String meaning) {
        Document doc = Jsoup.parse(meaning, "", Parser.xmlParser());
        doc.outputSettings(new Document.OutputSettings().prettyPrint(false));

        for (Element e : doc.select("tr")) {
            e.remove();
        }
        String result = doc.html();

        result = result.replaceAll("(?<=\\s)((<[^>]*>)*[\\[][^\\d\\s.,_]+?[\\]](<\\/[^>]*>)*)(?=\\s)", "\n");
        return result;
    }

    private String distinguishTranscription(String meaning) {
        //Todo переписать в более краткое выражение, которое делает сразу все
        String result = meaning.replaceAll(" (?<=\\s)((<[^>]*>)*[\\[][^\\d\\s.,_]+?[\\]](<\\/[^>]*>)*)(?=\\s)", "\n$1");
        return result.replaceAll("(?<=\\s)((<[^>]*>)*[\\[][^\\d\\s.,_]+?[\\]](<\\/[^>]*>)*)(?=\\s) ", "$1\n");
    }


    private String getMeanings(String meaning, int maxMeaningsCount) {
        String result = meaning;
        Pattern containsNumItemsPattern = Pattern.compile("((<\\d+>|\\[\\d+\\]|\\d+)(?=[\\s.:)]))+");
        Matcher matcher = containsNumItemsPattern.matcher(meaning);
        if (matcher.find()) {
            String patternString = "((^[\\s\\S]+?)%s|(^[\\s\\S]+))";
            Pattern getNumItemsPattern = Pattern.compile(String.format(patternString, maxMeaningsCount + 1), Pattern.MULTILINE);
            Matcher getNumItemsMatcher = getNumItemsPattern.matcher(meaning);
            if (getNumItemsMatcher.find()) {
                result = getNumItemsMatcher.group(1);
            }
        }
        return result;
    }

    private String moveMeaningsOnNewLines(String meaning) {
        return meaning.replaceAll(
                "(?<!\\n)((?<!\\S)(<\\d+>|\\[\\d+\\]|\\d+)(?=[\\s.:)]))+",
                "\n$1"
        );
    }

    private String processTagsAndReturnInHtml(String meaning) {
        String meaningWithReplacedNewLines = meaning.replace("\n", "<br>");
        Document doc = Jsoup.parse(meaningWithReplacedNewLines, "", Parser.xmlParser());
        doc.outputSettings(new Document.OutputSettings().prettyPrint(false));

        for (Element e : doc.select("tr")) {
            Element newElement = new Element(Tag.valueOf("i"), "")
                    .attr("color", "#BEBEBE")
                    .text("[" + e.text() + "]");;
            e.replaceWith(newElement);
        }
        for (Element e : doc.select("c")) {
            Element newElement = new Element(Tag.valueOf("font"), "")
                    .attr("color", "#BEBEBE")
                    .text(e.text());
            e.replaceWith(newElement);
        }

        String result = doc.html();

        return result;
    }
}

package com.caveman.lexiconator.model.StardictModel;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.dict.zip.DictZipInputStream;
import org.dict.zip.RandomAccessInputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

/**
 * StarDict dictionary file parser
 * usage as follows:<br/>
 * <code>StarDictParser rdw=new StarDictParser();</code><br/>
 * <code>rdw.loadIndexFile(f);</code><br/>
 * <code>rdw.loadContentFile(fcnt);</code><br/>
 * @author beethoven99@126.com
 */
public class StarDictParser {

	private byte buf[] = new byte[1024];

	private int smark;

	//缓冲区的处理标记
	private int mark;

	//装载读到的单词和位置、长度
	private Map<String, WordPosition> words=new HashMap<>();

	//随机读取字典内容
	private StarDictStreamer starDictStreamer;

	public StarDictParser(File idxFile, File dictionaryFile){
	    loadIndexFile(idxFile.getAbsolutePath());
	    starDictStreamer = new StarDictStreamer(dictionaryFile);
    }

    public Set<String> getWords() {
	    return words.keySet();
    }

    public boolean isContainWord(String word) {
	    boolean result = false;
        String word1 = StringUtils.trim(StringUtils.strip(word.toLowerCase()));

	    for (String wordKey : words.keySet()) {
            String word2 = StringUtils.trim(StringUtils.strip(wordKey.toLowerCase()));
	        if (word1.equals(word2)) {
	            result = true;
	            break;
            }
        }
	    return result;
    }

	public List<Entry<String, WordPosition>> findWord(String term){
		List<Entry<String, WordPosition>> res = new ArrayList<Map.Entry<String,WordPosition>>();

		//LevenshteinDistance levenshteinDistance = new LevenshteinDistance();
		//int minLevenshteinDictance = 100;

		for(Map.Entry<String, WordPosition> en : words.entrySet()){
		    if (en.getKey().toLowerCase().equals(term.toLowerCase())) {
                //System.out.println("FIND !" + term);
		        res.clear();
		        res.add(en);
		        return res;
            }
			//else if (en.getKey()!= null) {
			    //if (en.getKey().toLowerCase().indexOf(term.toLowerCase()) == 0) {
                    //System.out.println("FIND " + en.getKey());
                    //res.add(en);
                //}
            /*else {
		        int distance = levenshteinDistance.apply(term, en.getKey());
		        if (distance < minLevenshteinDictance)  {
		            minLevenshteinDictance = distance;
		            res.clear();
		            res.add(en);
		        }
		        else if (distance == minLevenshteinDictance){
                    res.add(en);
                }
		    }*/
			//}
		}

		//Todo Ничто не мешает выдавать результат, приближенный по Левеншейну, но как тогда тренировать неправильно записанные слова? Лучше показывать диалог, что "слово не найдено, но может вы имели в виду эти?"
		//Collections.sort(res, WordComparator);
		return res;
	}


	public String getWordExplanation(WordPosition wordPosition) {
        try {
            return starDictStreamer.seek(wordPosition.getStartPos(), wordPosition.getLength());
        } catch (IOException e) {
            throw new IllegalStateException("Can't seek word position: ", e);
        }
    }

	public void loadIfoFile(File file) {

    }

	public void loadIndexFile(String f){
		FileInputStream fis=null;
		try {
			fis=new FileInputStream(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		try {
			//第一次读
			int res=fis.read(buf);
			while(res>0){
				mark=0;
				smark=0;
				parseByteArray(buf,1024);
				if(mark==res){
					//上一个刚好完整，可能性几乎不存在，不过还要考虑
					res=fis.read(buf);
				}else{
					//有上一轮未处理完的，应该从mark+1开始
					//System.out.println("写 buf from: "+(buf.length-smark)+", len:"+(smark));
					res=fis.read(buf,buf.length-smark, smark);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * parse a block of bytes
	 * @param buf 使用全局的，之后可以去掉它
	 * @param len cbuf最多只有这么长
	 * @throws UnsupportedEncodingException
	 */
	private void parseByteArray(byte buf[],int len) throws UnsupportedEncodingException{
		for(;mark<len;){
			if(buf[mark]!=0){
				//在字符串中
				if(mark==len-1){
					//字符串被截断了
					//System.out.println("字符串被截断：mark:"+mark+", smark: "+smark+", left:"+(len-mark));
					System.arraycopy(buf, smark, buf, 0, len-smark);
					break;
				}else{
					mark++;
				}
			}else{
				//等于0，说明一个单词完结
				String tword=null;
				if(mark!=0){
					byte[] bs=ArrayUtils.subarray(buf, smark, mark);
					tword=new String(bs,"utf-8");
				}

				if(len-mark>8){
					//如果后面至少还有8个
					smark=mark+9;
					byte[] bstartpos = ArrayUtils.subarray(buf, mark+1,mark+5);
					byte[] blen=ArrayUtils.subarray(buf, mark+5, mark+9);
					int startpos=ByteArrayHelper.toIntAsBig(bstartpos);
					int strlen=ByteArrayHelper.toIntAsBig(blen);
					//System.out.println(" start: "+startpos+" , len:"+strlen);
					//现在是一个完整的单词了
					if(tword!=null && tword.trim().length()>0 && strlen<10000){
						words.put(tword, new WordPosition(startpos,strlen));
					}
					mark+=8;
				}else{
					//后面少于8个
					//System.out.println("后面少于8个:mark="+mark+", smark: "+smark+", left:"+(len-mark));
					//加完之后已跳过0，指向两个8字节数字
					System.arraycopy(buf, smark, buf, 0, len-smark);
					break;
				}
			}
		}
	}

	public List<String> getRandomWords(int count, List<String> exceptWords) {
	    List<String> wordsResult = new ArrayList<>();
	    List<String> wordsArray = new ArrayList<>(words.keySet());

	    Random random = new Random();

	    while ((wordsArray.size() != 0) && (wordsResult.size() != count)) {
            int randomIndex = random.nextInt(wordsArray.size());
            String word = wordsArray.get(randomIndex);
            if (word != null && !wordContainsInCollection(word, exceptWords) && word.split("\\s+").length == 1) {
                wordsResult.add(word);
            }
            wordsArray.remove(randomIndex);
        }
		return wordsResult;
	}

	public List<String> getExistedFromWords(List<String> wordsArg) {
	    List<String> existedWords = new ArrayList<>();
        List<String> wordsArray = new ArrayList<>(words.keySet());

        for (String word : wordsArray) {
            if (wordContainsInCollection(word, wordsArg)) {
                existedWords.add(word);
            }
        }

        return existedWords;
    }

	private boolean wordContainsInCollection(String word, List<String> collection) {
	    for (String wordVar : collection) {
	        if (wordVar.equalsIgnoreCase(word)) {
	            return true;
            }
        }
        return false;
    }

	public void setWords(Map<String, WordPosition> words) {
		this.words = words;
	}

	/**
	 * customer comparator
	 */
	private  static Comparator<Map.Entry<String,WordPosition>> WordComparator=new Comparator<Map.Entry<String,WordPosition>>(){
		public int compare(Map.Entry<String,WordPosition> ea,Map.Entry<String,WordPosition> eb){
            String word1 = StringUtils.trim(StringUtils.strip(ea.getKey().toLowerCase()));
            String word2 = StringUtils.trim(StringUtils.strip(eb.getKey().toLowerCase()));
			//return word1.compareToIgnoreCase(word2);
            return Integer.compare(word1.length(), word2.length());
		}
	};

	public static void main(String[] args) {

        File dictionaryDirectory = new File("C:\\Users\\CaveMan\\Desktop\\Android\\Lexiconator\\app\\src\\main\\assets\\default_dictionaries");

        File ifoFile = new File(dictionaryDirectory, "merrianwebster.ifo");
        File idxFile = new File(dictionaryDirectory, "merrianwebster.idx");
		File contentFile = new File(dictionaryDirectory, "merrianwebster.dict");


        StarDictParser starDictParser = new StarDictParser(idxFile, contentFile);
        List<Entry<String, WordPosition>> finded = starDictParser.findWord("cat's");

        for (Entry<String, WordPosition> entry : finded) {
                System.out.println(entry.getKey());
                //System.out.println(starDictParser.getWordExplanation(entry.getValue()));

                System.out.println("1 ------------------ " + finded.size());
        }
        LevenshteinDistance levenshteinDistance = new LevenshteinDistance();
        /*System.out.println(levenshteinDistance.apply("cat's", "cactus"));
        System.out.println(levenshteinDistance.apply("cats", "cactus"));
        System.out.println(levenshteinDistance.apply("cat's", "cactu"));
        System.out.println(levenshteinDistance.apply("cat's", "cat"));
        System.out.println(levenshteinDistance.apply("cats", "cat"));
        System.out.println(levenshteinDistance.apply("cat", "cactus"));

        System.out.println("cactus".compareToIgnoreCase("cat"));*/
	}
	

	public void testByConsole(){
		java.io.BufferedReader br=new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
		String s=null;
		
		try {
			s=br.readLine();
			while(s.length()>0){
				System.out.println(s);
				List<Map.Entry<String,WordPosition>> res=this.findWord(s);
				int i=0;
				for(Map.Entry<String,WordPosition> en:res){
					System.out.println(i+++" : "+en.getKey());
				}
				s=br.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 测试，显示这些单词
	 */
	public void showWords(){
		int i=0;
		for(Map.Entry<String, WordPosition> en:words.entrySet()){
			System.out.println(en.getKey()+" :"+en.getValue().getStartPos()+" - "+en.getValue().getLength());
			if(i++%25==0){
				System.out.println(this.getWordExplanation(en.getValue()));
			}
		}
	}


    protected static String getUncompressedFileName(final String name) {
        String result = name;
        if (name.endsWith(".dz") || name.endsWith(".gz")) {
            result = name.substring(0, name.length() - 3);
        }
        return result;
    }


    public void doUnzip(final File compressedFile) throws IOException {
        final int BUF_LEN = 58315;
        final long start = 0L;

        File outputFile = new File(compressedFile.getParent(), getUncompressedFileName(compressedFile.getName()));

        try (DictZipInputStream din = new DictZipInputStream(new RandomAccessInputStream(new RandomAccessFile(compressedFile, "r")));) {

            OutputStream outStream = new FileOutputStream(outputFile);
            byte[] buf = new byte[BUF_LEN];
            din.seek(start);
            int len;
            while ((len = din.read(buf, 0, BUF_LEN)) > 0) {
                outStream.write(buf, 0, len);
            }

            outStream.flush();
            outStream.close();
        }
    }
}

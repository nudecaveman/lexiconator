package com.caveman.lexiconator.model;

public enum SortDirection {
    ASCENDANT  ("ASC"),
    DESCENDANT ("DESC");

    private final String sortDirectionString;

    SortDirection(String sortDirectionString) {
        this.sortDirectionString = sortDirectionString;
    }

    public String getSortDirectionString() {
        return sortDirectionString;
    }
}

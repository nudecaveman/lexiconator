package com.caveman.lexiconator.model;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import io.reactivex.Single;

public class StatisticModelImpl extends BaseCurrentUserWordsSourceFacade implements StatisticModel {
    private WordsStatisticHelper wordsStatisticHelper;
    private Date startDate;
    private Date endDate;

    public StatisticModelImpl(WordsStatisticHelper wordsStatisticHelper,
            UserWordsSourcesHelper userWordsSourcesHelper, CurrentUserWordsSourcesRepository currentUserWordsSourcesRepository) {
        super(userWordsSourcesHelper, currentUserWordsSourcesRepository);
        this.wordsStatisticHelper = wordsStatisticHelper;

        Calendar calendar = Calendar.getInstance();

        this.endDate = calendar.getTime();

        calendar.add(Calendar.DATE, -(calendar.getActualMaximum(Calendar.DATE)));
        this.startDate = calendar.getTime();
    }

    @Override
    public Single<Map<Date, Integer>> getAddedWordsInDayStatistic() {
        return Single.fromCallable( () -> wordsStatisticHelper
                .getAddedWordsInDayStatistic(startDate, endDate, findCurrentUserWordsSourceMetadata().getId()));
    }

    @Override
    public Single<Integer> getWordsCountStatistic() {
        return Single.fromCallable( () -> wordsStatisticHelper
                .getWordsCountStatistic(startDate, endDate, findCurrentUserWordsSourceMetadata().getId()));
    }

    @Override
    public Date getStartDate() {
        return this.startDate;
    }

    @Override
    public Date getEndDate() {
        return this.endDate;
    }

    @Override
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Override
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}

package com.caveman.lexiconator.model;

import java.util.Date;
import java.util.Map;

public interface WordsStatisticHelper {

    Map<Date, Integer> getAddedWordsInDayStatistic(Date startDate, Date endDate, long userWordsSourceId);

    int getWordsCountStatistic(Date startDate, Date endDate, long userWordsSourceId);
}

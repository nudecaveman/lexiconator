package com.caveman.lexiconator.model.room;

import android.content.Context;

import com.caveman.lexiconator.model.UserWordsSource;
import com.caveman.lexiconator.model.UserWordsSourcesHelper;

import java.util.HashMap;
import java.util.List;

public class UserWordsSourcesSqliteHelper implements UserWordsSourcesHelper {

    private UserWordsSqliteSourcesDatabase db;
    private HashMap<UserWordsSourceMetadata, UserWordsSource> userWordsSourcesHashMap;

    public UserWordsSourcesSqliteHelper(Context context) {
        db = UserWordsSqliteSourcesDatabase.getInstance(context);
        this.userWordsSourcesHashMap = new HashMap<>();
    }

    @Override
    public boolean checkIfUserWordsSourceExists(long userWordsSourceId) {
        return db.userWordsSourcesDao().checkIfUserWordsSourceExists(userWordsSourceId);
    }

    @Override
    public UserWordsSourceMetadata getFirstUserWordsSourceMetadataFromExisted() {
        UserWordsSourceMetadata userWordsSourceMetadata = null;
        List<UserWordsSourceMetadata> userWordsSourceMetadataList = getAllUserWordsSourcesMetadata();
        if (userWordsSourceMetadataList.size() > 0) {
            userWordsSourceMetadata = userWordsSourceMetadataList.get(0);
        }
        return userWordsSourceMetadata;
    }

    @Override
    public UserWordsSource getUserWordsSource(UserWordsSourceMetadata userWordsSourceMetadata) {
        if (!userWordsSourcesHashMap.containsKey(userWordsSourceMetadata)) {
            UserWordsSource userWordsSource = new UserWordsSqliteSource(userWordsSourceMetadata, db);
            userWordsSourcesHashMap.put(userWordsSourceMetadata, userWordsSource);
            return userWordsSource;
        }
        return userWordsSourcesHashMap.get(userWordsSourceMetadata);
    }

    @Override
    public UserWordsSource getUserWordsSourceById(long userWordsSourceId) {
        UserWordsSourceMetadata userWordsSourceMetadata = getUserWordsSourceMetadataById(userWordsSourceId);
        return getUserWordsSource(userWordsSourceMetadata);
    }

    @Override
    public UserWordsSourceMetadata getUserWordsSourceMetadataById(long userWordsSourceId) {
        UserWordsSourceMetadata userWordSourceMetadata = db.userWordsSourcesDao().getUserWordsSourceMetadataById(userWordsSourceId);
        int userSourceWordsCount = db.userWordsDao().getWordsCountForSourceId(userWordsSourceId);
        userWordSourceMetadata.setWordsCount(userSourceWordsCount);
        return userWordSourceMetadata;
    }

    @Override
    public List<UserWordsSourceMetadata> getAllUserWordsSourcesMetadata() {
        List<UserWordsSourceMetadata> userWordsSourceMetadataList = db.userWordsSourcesDao().getAllUserWordsSourcesMetadata();
        for (UserWordsSourceMetadata userWordsSourceMetadata : userWordsSourceMetadataList) {
            int userSourceWordsCount = db.userWordsDao().getWordsCountForSourceId(userWordsSourceMetadata.getId());
            userWordsSourceMetadata.setWordsCount(userSourceWordsCount);
        }
        return userWordsSourceMetadataList;
    }

    @Override
    public void addNewUserWordsSourceMetadata(String userWordsSourceName, String userWordsSourceBriefCode) {
        UserWordsSourceMetadata userWordsSourceMetadata = new UserWordsSourceMetadata(userWordsSourceName, userWordsSourceBriefCode);
        db.userWordsSourcesDao().addNewUserWordsSourceMetadata(userWordsSourceMetadata);
    }

    @Override
    public void updateUserWordsSourceMetadata(UserWordsSourceMetadata userWordsSourceMetadata) {
        db.userWordsSourcesDao().updateUserWordsSourceMetadata(userWordsSourceMetadata);
    }

    @Override
    public void deleteUserWordsSourceMetadata(UserWordsSourceMetadata userWordsSourceMetadata) {
        db.userWordsSourcesDao().deleteUserWordsSourceMetadata(userWordsSourceMetadata);
    }
}

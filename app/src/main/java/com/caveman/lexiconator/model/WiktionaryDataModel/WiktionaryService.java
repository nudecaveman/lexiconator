package com.caveman.lexiconator.model.WiktionaryDataModel;

import com.caveman.lexiconator.model.WiktionaryDataModel.POJO.WordWikiData;

import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface WiktionaryService {

    @GET("api/rest_v1/page/definition/{word}?redirect=true")
    Single<Map<String, List<WordWikiData>>> getWordRawData(@Path("word") String word);
}

package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import io.reactivex.Completable;
import io.reactivex.Maybe;

public class WordsBrowserModelImpl implements WordsBrowserModel {
    private UserWordsSourcesHelper userWordsSourcesHelper;
    private CurrentDictionariesHelper currentDictionariesHelper;
    private CurrentBrowserWordsSourceRepository currentBrowserWordsSourceRepository;

    public WordsBrowserModelImpl(UserWordsSourcesHelper userWordsSourcesHelper, CurrentDictionariesHelper currentDictionariesHelper,
            CurrentBrowserWordsSourceRepository currentBrowserWordsSourceRepository) {
        this.userWordsSourcesHelper = userWordsSourcesHelper;
        this.currentDictionariesHelper = currentDictionariesHelper;
        this.currentBrowserWordsSourceRepository = currentBrowserWordsSourceRepository;
    }

    @Override
    public Maybe<WordsSourceMetadata> getCurrentWordSourceMetadata() {
        return Maybe.defer(() -> {
            /**
             * We need to open a browser on a same words source, on which the user has finished.
             * If there is no such one, opens any words source from existed.
             */
            WordsSourceMetadata currentWordsSourceMetadata = currentBrowserWordsSourceRepository.getCurrentWordsSourceMetadata();
            currentWordsSourceMetadata = getActualWordSourceMetadata(currentWordsSourceMetadata);

            if (currentWordsSourceMetadata == null) {
                currentWordsSourceMetadata = getAnyWordsSourceMetadata();
            }
            return currentWordsSourceMetadata != null ? Maybe.just(currentWordsSourceMetadata) : Maybe.empty();
        });
    }

    @Override
    public Completable setCurrentWordsSourceMetadata(WordsSourceMetadata wordsSourceMetadata) {
        return Completable.fromAction(() -> currentBrowserWordsSourceRepository.setCurrentWordsSource(wordsSourceMetadata));
    }

    private WordsSourceMetadata getActualWordSourceMetadata(WordsSourceMetadata wordsSourceMetadata) {
        WordsSourceMetadata actualWordSourceMetadata = null;
        //We need wordsSourceMetadata with only actual data
        if (wordsSourceMetadata instanceof UserWordsSourceMetadata) {
            if(userWordsSourcesHelper.checkIfUserWordsSourceExists(wordsSourceMetadata.getId())) {
                actualWordSourceMetadata  = userWordsSourcesHelper.getUserWordsSourceMetadataById(wordsSourceMetadata.getId());
            }
        }
        else if (wordsSourceMetadata instanceof DictionarySourceMetadata) {
            if (currentDictionariesHelper.checkIfDictionaryIsAvailable(wordsSourceMetadata.getId())) {
                actualWordSourceMetadata = currentDictionariesHelper.getDictionarySourceMetadataById(wordsSourceMetadata.getId());
            }
        }
        return actualWordSourceMetadata;
    }

    private WordsSourceMetadata getAnyWordsSourceMetadata() {
        WordsSourceMetadata wordsSourceMetadata = userWordsSourcesHelper.getFirstUserWordsSourceMetadataFromExisted();
        if (wordsSourceMetadata == null ) {
            wordsSourceMetadata = currentDictionariesHelper.getFirstDictionarySourceMetadataFromAvailable();
        }
        return wordsSourceMetadata;
    }
}

package com.caveman.lexiconator.model.GoogleTranslateModel;

import com.caveman.lexiconator.model.ServiceGenerator;
import com.caveman.lexiconator.model.WordData;
import com.caveman.lexiconator.model.WordDataHelper;
import com.google.gson.JsonArray;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.LinkedHashMap;

import io.reactivex.Single;

public class WordDataGTHelperImpl implements WordDataHelper {

    private GoogleTranslateService service;


    public WordDataGTHelperImpl(){
        service = ServiceGenerator.createService(
                GoogleTranslateService.class,
                "http://translate.google.cn/"
        );
    }


    @Override
    public Single<WordData> getWordData(String text, String sourceLanguage, String targetLanguage) {
        //http://translate.google.cn//translate_a/single?client=gtx&sl=auto&tl=ru&dt=t&q=cat
        HashMap<String, String> queryMap = new LinkedHashMap<>();
        queryMap.put("client", "gtx");
        queryMap.put("sl", sourceLanguage);
        queryMap.put("tl", targetLanguage);
        queryMap.put("dt", "t");
        queryMap.put("q", text);

        Single<JsonArray> rawWordData = service.getWordRawData(queryMap);
        return rawWordData.flatMap(
                (response) -> {
                    String responseMeaning = response.get(0).getAsJsonArray().get(0).getAsJsonArray().get(0).toString();
                    WordData wordData = new WordData(
                            text,
                            StringUtils.capitalize(responseMeaning.substring( 1, responseMeaning.length() - 1)),
                            null);
                    return Single.just(wordData);
                });
    }

    public static void main(String[] args) {
        WordDataGTHelperImpl wordDataGTHelper = new WordDataGTHelperImpl();
        Single<WordData> rawWordData = wordDataGTHelper.getWordData("cat is a big piece of meat",
                                                                    "auto", "ru");
        rawWordData.subscribe((wordData) -> {
            System.out.println(wordData.getMeaning());
        });
    }
}

package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface UserWordsSourcesModel {

    Completable addNewUserWordsSource(String userWordsSourceName, String userWordsSourceBriefCode);

    Completable deleteUserWordsSource(UserWordsSourceMetadata userWordsSourceMetadata);

    Completable editUserWordsSource(long userWordsSourceId, String wordsSourceName, String wordsSourceBriefCode);

    Single<List<UserWordsSourceMetadata>> getAllUserWordsSourcesMetadata();
}

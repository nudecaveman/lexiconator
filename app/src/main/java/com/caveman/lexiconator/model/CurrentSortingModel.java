package com.caveman.lexiconator.model;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface CurrentSortingModel {

    Single<SortingOrder> getCurrentSortingOrder();

    Completable setCurrentSortingOrder(SortingOrder sortingOrder);

    Single<SortDirection> getCurrentDictionarySourceSortDirection();

    Completable setCurrentDictionarySourceSortDirection(SortDirection sortDirection);
}

package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.util.List;

import io.reactivex.Single;

public class ChooseUserWordsSourceModelImpl implements ChooseUserWordsSourceModel {

    private UserWordsSourcesHelper userWordsSourcesHelper;

    public ChooseUserWordsSourceModelImpl(UserWordsSourcesHelper userWordsSourcesHelper) {
        this.userWordsSourcesHelper = userWordsSourcesHelper;
    }

    @Override
    public Single<List<UserWordsSourceMetadata>> getUserWordsSourceMetadataList() {
        return Single.fromCallable(() -> userWordsSourcesHelper.getAllUserWordsSourcesMetadata());
    }
}

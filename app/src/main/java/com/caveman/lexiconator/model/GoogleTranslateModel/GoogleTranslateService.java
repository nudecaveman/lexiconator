package com.caveman.lexiconator.model.GoogleTranslateModel;

import com.google.gson.JsonArray;

import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface GoogleTranslateService {
    //http://translate.google.cn//translate_a/single?client=gtx&sl=en&tl=ru&dt=bd&q=cat
    //http://translate.google.cn//translate_a/single?client=gtx&sl=en&tl=ru&dt=t&q=cat
    //http://translate.google.cn//translate_a/single?client=gtx&sl=auto&tl=ru&dt=t&q=cat
    @GET("translate_a/single")
    Single<JsonArray> getWordRawData(@QueryMap() Map<String, String> queries);
}

package com.caveman.lexiconator.model;

import android.content.Context;
import android.content.SharedPreferences;

public class CurrentUserWordsSourcesRepositoryImpl implements CurrentUserWordsSourcesRepository {

    private SharedPreferences sharedPreferences;

    public CurrentUserWordsSourcesRepositoryImpl(Context context) {
        sharedPreferences = context.getSharedPreferences("current_user_words_sources.pref", Context.MODE_PRIVATE);
    }

    @Override
    public void setUserWordsSourceIdForKey(String key, long userWordsSourceId) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(key, userWordsSourceId);
        editor.commit();
    }

    @Override
    public long getUserWordsSourceIdForeKey(String key) {
        return sharedPreferences.getLong(key, -1);
    }
}

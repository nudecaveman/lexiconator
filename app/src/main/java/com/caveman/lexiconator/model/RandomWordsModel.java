package com.caveman.lexiconator.model;

import com.caveman.lexiconator.Pair;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface RandomWordsModel extends CurrentUserWordsSourceFacade {

    Single<Boolean> isHaveCurrentDictionarySources();

    Single<Pair<Long, List<String>>> getRandomWords();

    Completable addWord(String word);
}

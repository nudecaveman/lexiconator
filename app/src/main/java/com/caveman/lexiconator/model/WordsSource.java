package com.caveman.lexiconator.model;

import java.util.List;

public interface WordsSource<W extends WordsSourceMetadata> {

    W getWordSourceMetadata();

    int getWordsCount();
}

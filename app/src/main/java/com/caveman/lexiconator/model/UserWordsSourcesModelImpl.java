package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class UserWordsSourcesModelImpl implements UserWordsSourcesModel {

    private UserWordsSourcesHelper userWordsSourcesHelper;

    public UserWordsSourcesModelImpl(UserWordsSourcesHelper userWordsSourcesHelper) {
        this.userWordsSourcesHelper = userWordsSourcesHelper;
    }

    @Override
    public Completable addNewUserWordsSource(String userWordsSourceName, String userWordsSourceBriefCode) {
        return Completable.fromAction( () -> userWordsSourcesHelper.addNewUserWordsSourceMetadata(userWordsSourceName, userWordsSourceBriefCode));
    }

    @Override
    public Completable deleteUserWordsSource(UserWordsSourceMetadata userWordsSourceMetadata) {
        return Completable.fromAction( () -> userWordsSourcesHelper.deleteUserWordsSourceMetadata(userWordsSourceMetadata));
    }

    @Override
    public Completable editUserWordsSource(long userWordsSourceId, String userWordsSourceName, String userWordsSourceBriefCode) {
        return Completable.fromAction( () -> {
            UserWordsSourceMetadata userWordsSourceMetadata = userWordsSourcesHelper.getUserWordsSourceMetadataById(userWordsSourceId);
            userWordsSourceMetadata.setName(userWordsSourceName);
            userWordsSourceMetadata.setBriefCode(userWordsSourceBriefCode);
            userWordsSourcesHelper.updateUserWordsSourceMetadata(userWordsSourceMetadata);
        });
    }

    @Override
    public Single<List<UserWordsSourceMetadata>> getAllUserWordsSourcesMetadata() {
        return Single.fromCallable(() ->  userWordsSourcesHelper.getAllUserWordsSourcesMetadata());
    }
}

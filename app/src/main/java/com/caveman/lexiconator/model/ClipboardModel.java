package com.caveman.lexiconator.model;

import com.caveman.lexiconator.Pair;

import io.reactivex.Maybe;
import io.reactivex.Single;

public interface ClipboardModel {

    Maybe<Pair<String, Integer>> addWordsFromTextForUserWordsSource(String text, long userWordsSourceId);

    Single<String> getTranslationFor(String text, String sourceLanguage, String targetLanguage);
}

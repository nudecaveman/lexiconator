package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import java.util.List;

import io.reactivex.Single;

public class WordCardModelImpl implements WordCardModel {

    private UserWordsSourcesHelper userWordsSourcesHelper;
    private CurrentDictionariesHelper currentDictionariesHelper;

    public WordCardModelImpl(UserWordsSourcesHelper userWordsSourcesHelper, CurrentDictionariesHelper currentDictionariesHelper) {
        this.userWordsSourcesHelper = userWordsSourcesHelper;
        this.currentDictionariesHelper = currentDictionariesHelper;
    }

    @Override
    public Single<List<WordData>> findWordForUserWordsSource(long userWordsSourceId, String word, MeaningConfiguration meaningConfiguration) {
        return Single.fromCallable( () -> {
            UserWordsSourceMetadata userWordsSourceMetadata = userWordsSourcesHelper.getUserWordsSourceMetadataById(userWordsSourceId);
            List<WordData> wordDataList = currentDictionariesHelper.findWord(word, userWordsSourceMetadata);
            processMeaning(wordDataList, meaningConfiguration);
            return wordDataList;
        });
    }

    @Override
    public Single<List<WordData>> findWordFromDictionary(long dictionarySourceId, String word, MeaningConfiguration meaningConfiguration) {
        return Single.fromCallable( () -> {
            List<WordData> wordDataList = currentDictionariesHelper.findWord(dictionarySourceId, word);
            processMeaning(wordDataList, meaningConfiguration);
            return wordDataList;
        });
    }

    private List<WordData> processMeaning(List<WordData> wordDataList, MeaningConfiguration meaningConfiguration) {
        for (WordData wordData : wordDataList) {
            String wordMeaning = wordData.getMeaningProcessor()
                    .processForWordCard(wordData.getWord(), wordData.getMeaning(), meaningConfiguration);
            wordData.setMeaning(wordMeaning);
        }
        return wordDataList;
    }
}

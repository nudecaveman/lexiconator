package com.caveman.lexiconator.model;

public class DictionaryWordMetadata implements WordMetadata {

    private String word;
    private long dictionarySourceId;

    public DictionaryWordMetadata(String word, long dictionarySourceId) {
        this.word = word;
        this.dictionarySourceId = dictionarySourceId;
    }

    @Override
    public String getWord() {
        return word;
    }

    @Override
    public void setWord(String word) {
        this.word = word;
    }

    public long getDictionarySourceId() {
        return dictionarySourceId;
    }


    public void setDictionarySourceId(long dictionarySourceId) {
        this.dictionarySourceId = dictionarySourceId;
    }
}

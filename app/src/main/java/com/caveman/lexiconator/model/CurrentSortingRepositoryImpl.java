package com.caveman.lexiconator.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

public class CurrentSortingRepositoryImpl implements CurrentSortingRepository {

    private static final String PREF_KEY_CURRENT_SORTING_ORDER = "PREF_KEY_CURRENT_SORTING_ORDER";
    private static final String PREF_KEY_CURRENT_SORT_DIRECTION = "PREF_KEY_CURRENT_SORT_DIRECTION";
    private SharedPreferences sharedPreferences;

    public CurrentSortingRepositoryImpl(Context context) {
        sharedPreferences = context.getSharedPreferences("current_sorting.pref", Context.MODE_PRIVATE);
        //TODO dagger заместитть инъекцию контекста и может имени файла
    }

    @Override
    public SortingOrder getCurrentUserWordsSourceSortingOrder() {
        SortingOrder currentSortingOrder;
        String currentSortingOrderString = sharedPreferences.getString(PREF_KEY_CURRENT_SORTING_ORDER, null);
        if (currentSortingOrderString != null) {
            Gson gson = new Gson();
            currentSortingOrder = gson.fromJson(currentSortingOrderString, SortingOrder.class);
        }
        else {
            currentSortingOrder = new SortingOrder(SortColumn.WORD, SortDirection.ASCENDANT);
        }
        return currentSortingOrder;
    }

    @Override
    public void setCurrentUserWordsSourceSortingOrder(SortingOrder sortingOrder) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String serializableCurrentSortingOrder = gson.toJson(sortingOrder);
        editor.putString(PREF_KEY_CURRENT_SORTING_ORDER, serializableCurrentSortingOrder);
        editor.apply();
    }

    @Override
    public SortDirection getCurrentDictionarySourceSortDirection() {
        SortDirection currentSortDirection;
        String currentSortingOrderString = sharedPreferences.getString(PREF_KEY_CURRENT_SORT_DIRECTION, null);
        if (currentSortingOrderString != null) {
            Gson gson = new Gson();
            currentSortDirection = gson.fromJson(currentSortingOrderString, SortDirection.class);
        }
        else {
            currentSortDirection = SortDirection.ASCENDANT;
        }
        return currentSortDirection;
    }

    @Override
    public void setCurrentDictionarySourceSortDirection(SortDirection sortDirection) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String serializableCurrentSortDirection = gson.toJson(sortDirection);
        editor.putString(PREF_KEY_CURRENT_SORT_DIRECTION, serializableCurrentSortDirection);
        editor.apply();
    }
}

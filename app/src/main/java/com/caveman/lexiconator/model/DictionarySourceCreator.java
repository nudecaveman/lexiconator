package com.caveman.lexiconator.model;

import java.io.File;
import java.util.List;

public abstract class DictionarySourceCreator {

    private List<String> extensions;

    public DictionarySourceCreator(List<String> extensions) {
        this.extensions = extensions;
    }

    public boolean isFileForThisCreator(File file) {
        String extension = getFileExtension(file);
        return (extension != null) && extensions.contains(extension);
    }

    protected File getFileByExtension(List<File> files, String extension) {
        for (File file : files) {
            String fileExtension = getFileExtension(file);
            if ((fileExtension != null) && (fileExtension.equals(extension))) {
                return file;
            }
        }
        return null;
    }

    public String getFileNameWithoutExtension(File file) {
        String[] nameParts = file.getName().split("\\.", 2);
        return (nameParts.length >= 2) ? nameParts[0] : file.getName();
    }

    public String getFileExtension(File file) {
        String[] nameParts = file.getName().split("\\.", 2);
        return (nameParts.length >= 2) ? nameParts[1] : null;
    }

    public abstract DictionarySourceMetadata createDictionaryRes(List<File> dictionaryFiles);
}

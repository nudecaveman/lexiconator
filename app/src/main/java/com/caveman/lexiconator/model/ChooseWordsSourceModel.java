package com.caveman.lexiconator.model;

import io.reactivex.Single;

public interface ChooseWordsSourceModel {

    Single<WordsSourcesData> getWordsSourcesData();
}

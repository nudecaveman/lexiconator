package com.caveman.lexiconator.model;

import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;

public interface CurrentUserWordsSourceFacade {

    Maybe<UserWordsSourceMetadata> getCurrentUserWordsSourceMetadata();

    Completable setCurrentUserWordsSource(UserWordsSourceMetadata userWordsSourceMetadata);

    Single<Boolean> isHaveUserWordsSources();
}

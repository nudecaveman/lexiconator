package com.caveman.lexiconator.model;

public interface MeaningProcessor {

    String processForWordCard(String word, String meaning, MeaningConfiguration meaningConfiguration);

    String processForWordTest(String word, String meaning, MeaningConfiguration meaningConfiguration);
}

package com.caveman.lexiconator;

import com.caveman.lexiconator.model.CurrentBrowserWordsSourceRepository;
import com.caveman.lexiconator.model.CurrentDictionariesHelper;
import com.caveman.lexiconator.model.DictionarySourceMetadata;
import com.caveman.lexiconator.model.UserWordsSourcesHelper;
import com.caveman.lexiconator.model.WordsBrowserModelImpl;
import com.caveman.lexiconator.model.WordsSourceMetadata;
import com.caveman.lexiconator.model.room.UserWordsSourceMetadata;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import io.reactivex.Maybe;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class WordsBrowserModelImplTest {
    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock public UserWordsSourceMetadata userWordsSourceMetadata;
    @Mock public DictionarySourceMetadata dictionarySourceMetadata;

    @Mock public UserWordsSourcesHelper userWordsSourcesHelper;
    @Mock public CurrentDictionariesHelper currentDictionariesHelper;
    @Mock public CurrentBrowserWordsSourceRepository currentBrowserWordsSourceRepository;

    @InjectMocks public WordsBrowserModelImpl wordsBrowserModel;

    @Test
    public void getCurrentUserWordSourceMetadata_SavedUserWordsSourceNotExist_And_NoAnyWordSource_ReturnNothing_Test() {
        //We get a UserWordsSource, on which a user previously has finished
        when(currentBrowserWordsSourceRepository.getCurrentWordsSourceMetadata()).thenReturn(userWordsSourceMetadata);
        //But it's not exist anymore
        when(userWordsSourcesHelper.checkIfUserWordsSourceExists(anyInt())).thenReturn(false);
        //And there is no an any other words source
        when(userWordsSourcesHelper.getFirstUserWordsSourceMetadataFromExisted()).thenReturn(null);
        when(currentDictionariesHelper.getFirstDictionarySourceMetadataFromAvailable()).thenReturn(null);

        Maybe<WordsSourceMetadata> wordsSourceMetadataObservable = wordsBrowserModel.getCurrentWordSourceMetadata();

        wordsSourceMetadataObservable
                .test()
                .assertSubscribed()
                .assertValueCount(0)
                .assertComplete()
                .assertNoErrors();

        verify(currentDictionariesHelper, never()).checkIfDictionaryIsAvailable(anyInt());
        verify(userWordsSourcesHelper).getFirstUserWordsSourceMetadataFromExisted();
    }

    @Test
    public void getCurrentUserWordsSourceMetadata_SavedUserWordsSourceSourceExist_ReturnThat_Test() {
        //We get a UserWordsSource, on which a user previously has finished
        when(currentBrowserWordsSourceRepository.getCurrentWordsSourceMetadata()).thenReturn(userWordsSourceMetadata);
        //And it's exist
        when(userWordsSourcesHelper.checkIfUserWordsSourceExists(anyInt())).thenReturn(true);
        when(userWordsSourcesHelper.getUserWordsSourceMetadataById(anyInt())).thenReturn(userWordsSourceMetadata);
        //And there is no an any other words source
        when(userWordsSourcesHelper.getFirstUserWordsSourceMetadataFromExisted()).thenReturn(null);
        when(currentDictionariesHelper.getFirstDictionarySourceMetadataFromAvailable()).thenReturn(null);

        Maybe<WordsSourceMetadata> wordsSourceMetadataObservable = wordsBrowserModel.getCurrentWordSourceMetadata();

        wordsSourceMetadataObservable
                .test()
                .assertSubscribed()
                .assertValueCount(1)
                .assertValue(userWordsSourceMetadata)
                .assertComplete()
                .assertNoErrors();

        verify(currentDictionariesHelper, never()).checkIfDictionaryIsAvailable(anyInt());
        verify(userWordsSourcesHelper, never()).getFirstUserWordsSourceMetadataFromExisted();
    }

    @Test
    public void getCurrentDictionarySourceMetadata_SavedDictionarySourceNotExist_And_NoAnyWordSource_ReturnNothing_Test() {
        //We get a DictionarySource, on which a user previously has finished
        when(currentBrowserWordsSourceRepository.getCurrentWordsSourceMetadata()).thenReturn(dictionarySourceMetadata);
        //But it's not exist anymore
        when(currentDictionariesHelper.checkIfDictionaryIsAvailable(anyInt())).thenReturn(false);
        //And there is no an any other words source
        when(userWordsSourcesHelper.getFirstUserWordsSourceMetadataFromExisted()).thenReturn(null);
        when(currentDictionariesHelper.getFirstDictionarySourceMetadataFromAvailable()).thenReturn(null);

        Maybe<WordsSourceMetadata> wordsSourceMetadataObservable = wordsBrowserModel.getCurrentWordSourceMetadata();

        wordsSourceMetadataObservable
                .test()
                .assertSubscribed()
                .assertValueCount(0)
                .assertComplete()
                .assertNoErrors();

        verify(userWordsSourcesHelper, never()).checkIfUserWordsSourceExists(anyInt());
        verify(currentDictionariesHelper).getFirstDictionarySourceMetadataFromAvailable();
    }

    @Test
    public void getCurrentWordsSourceMetadata_SavedDictionarySourceExist_ReturnThat_Test() {
        //We get a DictionarySource, on which a user previously has finished
        when(currentBrowserWordsSourceRepository.getCurrentWordsSourceMetadata()).thenReturn(dictionarySourceMetadata);
        //And it's exist
        when(currentDictionariesHelper.checkIfDictionaryIsAvailable(anyInt()))
                .thenReturn(true);
        when(currentDictionariesHelper.getDictionarySourceMetadataById(anyInt())).thenReturn(dictionarySourceMetadata);
        //And there is no an any other words source
        when(userWordsSourcesHelper.getFirstUserWordsSourceMetadataFromExisted()).thenReturn(null);
        when(currentDictionariesHelper.getFirstDictionarySourceMetadataFromAvailable()).thenReturn(null);

        Maybe<WordsSourceMetadata> wordsSourceMetadataObservable = wordsBrowserModel.getCurrentWordSourceMetadata();

        wordsSourceMetadataObservable
                .test()
                .assertSubscribed()
                .assertValueCount(1)
                .assertValue(dictionarySourceMetadata)
                .assertComplete()
                .assertNoErrors();

        verify(userWordsSourcesHelper, never()).checkIfUserWordsSourceExists(anyInt());
        verify(currentDictionariesHelper, never()).getFirstDictionarySourceMetadataFromAvailable();
    }

    @Test
    public void getCurrentWordsSourceMetadata_NoSaved_ReturnExistedUserWordsSource_Test() {
        //We get a WordSource, on which a user previously has finished
        when(currentBrowserWordsSourceRepository.getCurrentWordsSourceMetadata()).thenReturn(null);
        //And it's not exist
        //And there is some another words sources
        when(userWordsSourcesHelper.getFirstUserWordsSourceMetadataFromExisted()).thenReturn(userWordsSourceMetadata);
        when(currentDictionariesHelper.getFirstDictionarySourceMetadataFromAvailable()).thenReturn(dictionarySourceMetadata);

        Maybe<WordsSourceMetadata> wordsSourceMetadataObservable = wordsBrowserModel.getCurrentWordSourceMetadata();

        wordsSourceMetadataObservable
                .test()
                .assertSubscribed()
                .assertValueCount(1)
                .assertValue(userWordsSourceMetadata)
                .assertComplete()
                .assertNoErrors();

        verify(userWordsSourcesHelper, never()).checkIfUserWordsSourceExists(anyInt());
        verify(currentDictionariesHelper, never()).checkIfDictionaryIsAvailable(anyInt());
        verify(userWordsSourcesHelper).getFirstUserWordsSourceMetadataFromExisted();
        verify(currentDictionariesHelper, never()).getFirstDictionarySourceMetadataFromAvailable();
    }

    @Test
    public void getCurrentWordsSourceMetadata_NoSaved_ReturnExistedDictionaryWordsSource_Test() {
        //We get a WordSource, on which a user previously has finished
        when(currentBrowserWordsSourceRepository.getCurrentWordsSourceMetadata()).thenReturn(null);
        //And it's not exist
        //And there is some another words sources
        when(userWordsSourcesHelper.getFirstUserWordsSourceMetadataFromExisted()).thenReturn(null);
        when(currentDictionariesHelper.getFirstDictionarySourceMetadataFromAvailable()).thenReturn(dictionarySourceMetadata);

        Maybe<WordsSourceMetadata> wordsSourceMetadataObservable = wordsBrowserModel.getCurrentWordSourceMetadata();

        wordsSourceMetadataObservable
                .test()
                .assertSubscribed()
                .assertValueCount(1)
                .assertValue(dictionarySourceMetadata)
                .assertComplete()
                .assertNoErrors();

        verify(userWordsSourcesHelper, never()).checkIfUserWordsSourceExists(anyInt());
        verify(currentDictionariesHelper, never()).checkIfDictionaryIsAvailable(anyInt());
        verify(userWordsSourcesHelper).getFirstUserWordsSourceMetadataFromExisted();
        verify(currentDictionariesHelper).getFirstDictionarySourceMetadataFromAvailable();
    }

    @Test
    public void getCurrentWordsSourceMetadata_NoSaved_ReturnNothing_Test() {
        //We get a WordSource, on which a user previously has finished
        when(currentBrowserWordsSourceRepository.getCurrentWordsSourceMetadata()).thenReturn(null);
        //And it's not exist
        //And there is no another words sources
        when(userWordsSourcesHelper.getFirstUserWordsSourceMetadataFromExisted()).thenReturn(null);
        when(currentDictionariesHelper.getFirstDictionarySourceMetadataFromAvailable()).thenReturn(null);

        wordsBrowserModel.getCurrentWordSourceMetadata()
                .test()
                .assertSubscribed()
                .assertValueCount(0)
                .assertComplete()
                .assertNoErrors();

        verify(userWordsSourcesHelper, never()).checkIfUserWordsSourceExists(anyInt());
        verify(currentDictionariesHelper, never()).checkIfDictionaryIsAvailable(anyInt());
        verify(userWordsSourcesHelper).getFirstUserWordsSourceMetadataFromExisted();
        verify(currentDictionariesHelper).getFirstDictionarySourceMetadataFromAvailable();
    }

    @Test
    public void setCurrentWordsSourceMetadataTest() {
        wordsBrowserModel.setCurrentWordsSourceMetadata(userWordsSourceMetadata)
                .test()
                .assertSubscribed()
                .assertComplete()
                .assertNoErrors();
        verify(currentBrowserWordsSourceRepository).setCurrentWordsSource(userWordsSourceMetadata);
    }
}
